/** ------------------------------------------------------------
 *  monitor.cxx - main file of opmon_monitor application
 *
 *  Monitors transition states of the run control controllers
 *  in a given partition.
 *  Makes use of OH to publish the data online on the IS server.
 *  Saves the collected data to a root file.
 *
 *  author: Herbert Kaiser, Sep 2006
 *  ------------------------------------------------------------
 */

#include <atomic>
#include <unistd.h>
#include <memory>
#include <vector>
#include <signal.h>
#include <dirent.h>
#include <sstream>
#include <mutex>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include <ipc/signal.h>
#include <is/info.h>
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <ipc/core.h>
#include <rc/RCStateInfo.h>
#include <map>
#include <TFile.h>
#include <TClass.h>
#include <TString.h>
#include <TKey.h>
#include <TThread.h>
#include <TSystem.h>
#include <oh/OHUtil.h>
#include <is/infoT.h>
#include <cmdl/cmdargs.h>
#include <oh/OHRootProvider.h>
#include <oh/OHRawProvider.h>
#include <config/Configuration.h>
#include <config/SubscriptionCriteria.h>
#include <config/Change.h>
#include "opmon/RCTransLog.h"
#include "opmon/RCTimeSequence.h"
#include "opmon/RCStringTree.h"

// name of RunCtrl IS server
#define IS_RUNCTRL "RunCtrl"
// number of letters+1 of this name
#define IS_RUNCTRL_N 8
// name of root controller
#define ROOT_CTRL "RootController"
// starting state for RootController
#define NONE_STATE "NONE"
// seperator for full names in the ROOT files
#define TREE_SEPERATOR '|'


using namespace std;

// global pointer to the receiver
ISInfoReceiver * isRec;
// map to save the data of the different controllers
map<string,RCTransLog*>  rcData;
// global pointer to RCStringTree
RCStringTree* rcst;
// RootController timing sequence
RCTimeSequence* rootTiming;
// global pointer to RawProvider
OHRawProvider<OHBins>* gRawProv;
// global pointer to RootProvider
OHRootProvider* gRootProv;
// global pointer to partition
IPCPartition* ipcPartition;
string partName;
string dataName;
string fileName;
string pathName;
// maximal level to which should be published
int PUBLISH_LEVEL;
bool PUBLISH_TAVG;
int LOG_LEVEL;
bool WITH_SA;
// wait period for IS subscription
int WAIT_IS;
bool RESUBSCRIBE;
bool NO_OH;
bool VERBOSE;
bool SAVE_SEPERATE_CYCLE;
bool FILENAME_BYUSER;
int CYCLE_NUMBER;
bool TO_BE_SAVED;
bool stopAll;

std::atomic<bool> RESTART_MYSELF(ATOMIC_VAR_INIT(false));
char** ARGV = nullptr;

std::mutex s_mutex;
std::mutex f_mutex;

const char* const monthName[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

// global function to convert CmdArgStr...
inline std::string str(const char * word)
{
    return word ? word : "";
}

inline std::string replaceChar(const std::string& s,char c1, char c2) {
    std::string sc(s);
    for (unsigned i=0;i<s.length();i++)
	if (sc[i]==c1) sc[i]=c2;
    return sc;
}

void saveToRoot(bool withDeletion=false) {
    std::lock_guard<std::mutex> lk(f_mutex);

    if (!TO_BE_SAVED) {
	return;
    }

    string deleteOld="";
    if (!FILENAME_BYUSER) {
	deleteOld=fileName;
	fileName="";
    }

    if (fileName=="") {
	ostringstream ostr;
	OWLTime curTime;
	OWLDate curDate(curTime);
	ostr << pathName << partName << '_' << setfill('0')
	     << setw(4) << curDate.year()
	     << setw(3) << monthName[curDate.month() - 1] 
	     << setw(2) << curDate.day() << setw(1) << '_'
	     << setw(2) << curTime.hour() << setw(1) << '-' 
	     << setw(2) << curTime.min() << setw(1) << '-' 
	     << setw(2) << curTime.sec();
	if (SAVE_SEPERATE_CYCLE)
	    ostr << setw(4) << "cycle" << setw(2) << CYCLE_NUMBER;
	ostr << setw(5) << ".root";
	fileName=ostr.str();
    }

    TFile f(fileName.c_str(),"RECREATE");
    if (f.IsZombie()) {
	cerr << "ERROR: log file '"<< fileName << "' could not be created!" << endl;
	return;
    }

    TString part(partName.c_str());
    f.WriteObject(&part,"partition");
    TString time(OWLTime().c_str());
    f.WriteObject(&time,"timestamp");

    f.WriteObject(rootTiming,"RootTiming");

    vector<string> list;
    rcst->getAllBranchOrdered(list);
    map<string,RCTransLog*>::const_iterator ctrlIt;
    for (unsigned i=0;i<list.size();i++) {
	ctrlIt=rcData.find(list[i]);
	if (ctrlIt==rcData.end()) {
	    if (VERBOSE) cout<<"WARNING during saving: controller "<<list[i]<<" had no transitions yet!" << endl;
	    continue;
	}
	f.WriteObject(ctrlIt->second,replaceChar(ctrlIt->second->getFullName(),'/',TREE_SEPERATOR).c_str());
    }

    f.Close();

    cout << "Saved data to: " << fileName << endl;

    if (withDeletion) {
	map<string,RCTransLog*>::const_iterator delIt;
	for (delIt=rcData.begin();delIt!=rcData.end();delIt++)
	    delIt->second->deleteData();
	delete rootTiming;
	rootTiming=new RCTimeSequence(ROOT_CTRL);
    }

    if ( (!SAVE_SEPERATE_CYCLE) && ( (deleteOld!="") || (fileName!=deleteOld) ) ) {
	remove(deleteOld.c_str());
    }
}

void loadFromRoot(const string& fName) {
    TFile f(fName.c_str(),"READ");
    if (f.IsZombie()) {
	cout << "File "<< fName << " doesn't exist yet or is not a valid ROOT file. It will be (re)created." << endl;
	return;
    }
    TString* tstr;
    f.GetObject("partition",tstr);
    if (!tstr) {
	cout << "File "<< fName << " is not a valid ROOT file for trans_log. It will be recreated." << endl;
	return;
    }
    cout << "Retrieving data from: " << fName << endl;
    cout << "Partition: " << (const char*)((*tstr));
    f.GetObject("timestamp",tstr);
    if (!tstr) {
	cout << "File "<< fName << " is not a valid ROOT file for trans_log. It will be recreated." << endl;
	return;
    }
    cout  << "   Last Updated: " << (const char*)((*tstr)) << endl;

    RCTransLog* rctl=NULL;

    TKey *key;
    TIter next(f.GetListOfKeys());next();next();
    while ((key = (TKey *) next())) {
	rctl= static_cast<RCTransLog*>(key->ReadObjectAny(TClass::GetClass("RCTransLog")));
	rctl->setFullName(replaceChar(key->GetName(),TREE_SEPERATOR,'/'));
	rcData[rctl->getName()]=rctl;
    }

    f.Close();
}

// include the CommandListener class
#include "TransLogCommands.h"

bool subscribeNew();

// callback function that's called by the receiver
void callback(ISCallbackInfo* isc){
    // build the object with the RunCtrl controller info
    RCStateInfo RCSI;
    isc->value(RCSI);

    // get name of controller
    string name=isc->name();
    name.erase(0,IS_RUNCTRL_N); //delete RunCtrl. at the beginning

    // leave if controller is not in the segment control tree
    if (!rcst->exists(name)) { 
	return;
    }

    std::unique_lock<std::mutex> lk(s_mutex);

    RCTransLog* rct;
    map<string,RCTransLog*>::const_iterator findIt=rcData.find(name);

    if (findIt==rcData.end()) {
        // add controller to the map, if it's not there yet
        //std::cout << "Adding " << name << " to transition log table."<< std::endl;
        rct=new RCTransLog(name,rcst->getFullName(name));
        rcData[name]=rct;
    } else {
        rct=findIt->second;
    }

    const string transName=rct->recordTrans(RCSI.state,RCSI.transitionTime);

    lk.unlock();

    if (transName!="") TO_BE_SAVED=true;

    if (name==ROOT_CTRL) {

	if (isc->reason() == ISInfo::Deleted ) {
	    if (VERBOSE) cout << "RootController information was deleted." << endl;
	    if (RESUBSCRIBE) {
		cout << "IS server seems to go down, will try to resubscribe on a new server." << endl;
		daq::ipc::signal::raise();
	    }
	    if (WAIT_IS>0)
		daq::ipc::signal::raise();
	}

	if (transName!="") {
	    boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
	    std::string s = boost::posix_time::to_simple_string(now);
	    rootTiming->recordValue(transName,s);

	    if (!NO_OH) {
		if (PUBLISH_LEVEL==0) {
		    rcData[ROOT_CTRL]->publish(gRawProv,transName);
		    if (PUBLISH_TAVG) TransLogCommandListener::showAvgForAllTransitions(ROOT_CTRL);
		} else {
		    vector<string> list;
		    rcst->getAllBranchOrdered(list,PUBLISH_LEVEL);
		    map<string,RCTransLog*>::const_iterator publishIt;
		    for (unsigned i=0;i<list.size();i++) {
			publishIt =rcData.find(list[i]);
			if (publishIt!=rcData.end()) {
			    publishIt->second->publish(gRawProv,transName);
			    if (PUBLISH_TAVG) TransLogCommandListener::showAvgForAllTransitions(list[i]);
			}
		    }
		    TransLogCommandListener::publishAvgList(string(OH_FORENAME)+"Avg/"+transName,string("Average of all controllers: ")+transName,transName,list,gRootProv);
		}
	    }
	    if (SAVE_SEPERATE_CYCLE && (RCSI.state==NONE_STATE) && (transName!=string(string("UNKNOWN->")+NONE_STATE))) {
		fileName="";

		saveToRoot(true);

		TO_BE_SAVED=false;
		CYCLE_NUMBER++;
	    }
	}

    }

}

void cleanUpGlobals() {
    if (rootTiming) {
	delete rootTiming;
    }
    // clean up the globals
    if (gRawProv) delete gRawProv;
    if (gRootProv) delete gRootProv;
    if (rcst) delete rcst;
    delete ipcPartition;

    std::map<std::string,RCTransLog*>::iterator it;
    for (it=rcData.begin();it!=rcData.end();it++) {
	delete it->second;
    }
    rcData.clear();
}

void configCallback(const std::vector< ::ConfigurationChange*>&, void*) {
    // Since a change in the DB is not properly managed, then send a signal
    // to the process itself and simply exit (see signal_handler)
    ::kill(::getpid(), SIGUSR2);
}

bool subscribeNew() {
    if (WAIT_IS > 0) {
	cout << "Waiting for IS server ";
	if (!VERBOSE) cout << endl;
	ISInfoDictionary dict(*ipcPartition);
	bool stopWait=false;
	while ( true ) {
	    try {
		if (dict.contains(string(IS_RUNCTRL)+"."+ROOT_CTRL)) {
		    stopWait=true;
		}
	    } catch (daq::is::Exception&) {}
	    if (stopWait) break;
	    usleep(1000*WAIT_IS);
	    if (VERBOSE) { cout << "."; cout.flush(); }
	    if (stopAll) {
		cout << "Waiting stopped." << endl;
		if (rcst) {
		    saveToRoot();
		}
		cleanUpGlobals();
		exit(0);
	    }
	}
	if (VERBOSE) cout << " Found!" << endl;
    }

 
    // Create HistogrammProvider and CommandListener
    TransLogCommandListener* lst=NULL;

    if (!NO_OH) {
	lst=new TransLogCommandListener;
	// if one of them is NULL both are NULL, because they're always created at once
	if (!gRawProv) {
	    try {
		gRawProv=new OHRawProvider<OHBins>(*ipcPartition,IS_HISTO,OH_PROV_NAME,lst);
		gRootProv=new OHRootProvider(*ipcPartition,IS_HISTO,OH_PROV_NAME,lst);
	    } catch(daq::oh::Exception & ex) {
		ers::error(ex);
		cerr << "ERROR: OH providers could not be created!" << endl;
		return false;
	    }
	} else {
	    // update the provider information in the IS server, when we subscribe for the second time
	    ISInfoDictionary dict(*ipcPartition);
	    string provName(oh::util::create_provider_name(IS_HISTO,OH_PROV_NAME));
	    ISInfoT<bool> provInfo(true);
	    try {
		dict.getValue(provName,provInfo);
		try {
		    dict.update(provName,provInfo=true);
		} catch (daq::is::Exception&) {
		    cerr << "ERROR during provider setup: Could not update provider entry in IS!\n"
		            "Provider might be invisible for OH Display etc."<< endl;
		}
	    } catch (daq::is::Exception&) {
		try {
		    dict.insert(provName,provInfo=true);
		} catch (daq::is::Exception&) {
		    cerr << "ERROR during provider setup: Could not insert provider entry in IS!\n"
		            "Provider might be invisible for OH Display etc."<< endl;
		}
	    }
	}
    }

    // Create StringTree
    if (!rcst) {
       	rcst = new RCStringTree(partName,dataName,VERBOSE,!WITH_SA,LOG_LEVEL);
	if (!rcst->refreshTree()) {
	    cerr << "ERROR: Could not load information from database, please check the parameters.." << endl;
	    return false;
	}
    }

    try {
	isRec->subscribe(IS_RUNCTRL,RCStateInfo::type() && ".*",callback);
    } catch (daq::is::Exception&) {
	cerr<<"ERROR: Subscription for type RCStateInfo in RunCtrl failed!"<<endl;
	return false;
    }

    cout<<"Subscribed for type RCStateInfo in RunCtrl. Monitoring started..." << endl;

    std::unique_ptr<Configuration> conf;
    ::Configuration::CallbackId cbkHandle = 0;
    
    try {
        conf.reset(new Configuration(""));
        cbkHandle = conf->subscribe(::ConfigurationSubscriptionCriteria(), &configCallback, nullptr); 
    }
    catch(ers::Issue& ex) {
        ers::error(ex);
        return false;
    }
        
    // block, interrupted by semaphore.post()
    int sig = daq::ipc::signal::wait_for({SIGINT, SIGTERM, SIGUSR1, SIGUSR2});
    if (sig==SIGUSR1) {
        saveToRoot();
        TO_BE_SAVED=false;
    }
    else if(sig == SIGUSR2) {
        // SIGUSR2 is sent after receiving a notification from the RDB because of
        // a reload of the database. This application will exit and re-exec itself
        // so that it is silently restarted (ugly ugly work-around)
        RESTART_MYSELF = true;
    }

    stopAll=true;

    try {
        if((conf.get() != nullptr) && (cbkHandle != 0)) {
            conf->unsubscribe(cbkHandle);
        }
    }
    catch(ers::Issue& ex) {
        ers::info(ex);
    }
        
    if (lst) delete lst;

    return true;
}

void unsubscribe() {
    // unsubscribe
    try {
	isRec->unsubscribe(IS_RUNCTRL,RCStateInfo::type() && ".*");
    } catch (daq::is::Exception&) {
	if (VERBOSE) cout<<"Unsubscription in RunCtrl failed. IS server seems to be down."<<endl;
    }
    if (VERBOSE) cout << "Unsubscribed from IS server." << endl;
}

int main( int argc, char ** argv ){
    //Remove ROOT SigHandlers
    for (int sig = 0; sig < kMAXSIGNALS; sig++)
    {
      gSystem->ResetSignal((ESignals)sig);
    }

    //Prevent histogram tracking
    TH1::AddDirectory(kFALSE);

    //Initialise mutexes
    TThread::Initialize();

    ARGV = argv;    

    if (argc==2 && strcmp(argv[1],"--commands")==0 ) {
	cout <<	"Commands that can be send to opmon_monitor via the OHCommandSender,"
	" which is also possible in the OH Display or OH Presenter:\n\n"
	"Save\n"
	"Saves the currently collected data to the ROOT file specified in the command line parameters.\n"
	"Exit\n"
	"Quits opmon_monitor after saving the data to the ROOT file, same as sending the TERM signal or pressing Ctrl-C\n\n"
	"PDia:controller_name:transition_name\n"
	"publish the diagram for <controller_name> and <transition_name>\n\n"
	"PDiaCtrl:controller_name\n"
	"publish all the possible diagrams for <controller_name>\n\n"
	"PDiaCtrlAll\n"
	"publish all the diagrams for all the controllers\n\n"
	"PDiaLvl:controller_name:transition_name\n"
	"publish the diagrams of all the controllers in the same level as this "
	"controller for this transition, <controller_name> can also be a positive "
	"integer giving the level number\n\n"
	"PAvgCtrl:controller_name\n"
	"publish the average for each transition time for <controller_name> in one diagram\n\n"
	"PAvgAll:transition_name:level\n"
	"For transition <transition_name> publish up to level <level> the average time of "
	"each controller in one diagram. E.g. level=1 means root controller plus 1st level "
	"of segment controllers\n"
	"note: <level> is optional, by default all the values of all controllers are published\n\n"
	"PAvgLvl:controller_name:transition_name\n"
	"For transition <transition_name> publish the average "
	"time of each controller of the same level as controller <controller_name> in one"
	" diagram. <controller_name> can also be a positive integer giving the level number\n\n" 
	"PAvgSParent:controller_name:transition_name\n"
	"For transition <transition_name> publish the average time of each controller that has "
	"the same parent controller as <controller_name> in one diagram.\n\n"
	"PLastLvl:controller_name:transition_name:ex_number\n"
	"For all controllers in the same level as <controller_name> publish the time needed "
	"for the last execution of the transition <transition_name>. If the optional "
	"parameter <ex_number> is given, execution number ex_number is taken instead of last execution\n\n"
	"<controller_name> is the full unique name of the controller as it is displayed e.g. in the IGUI\n"
	"<transition_name> is the name for an internal transition, e.g. INITIAL->CONFIGURED or NONE->INITIAL\n"
	"'publish' means that the diagrams are published in the OH using the currently collected"
	" data. The diagrams will NOT be updated everytime when new transitions have been recorded."
	" You need to send the command again to have an updated diagram." << endl;
	return 0;
    }


    CmdArgStr   partition ('p', "partition", "partition", "partition");
    CmdArgStr   data      ('d', "data", "data", "database name in format plugin:parameter, e.g. "
                                                "oksconfig:filename.xml or rdbconfig:RDB "
						"if not used, the value is taken from TDAQ_DB environment variable.");
    CmdArgStr	filename  ('f', "filename", "filename", "complete name of the ROOT file to save the data in;"
				" if this file already exists it will be overwritten; if <filename> is provided"
				" then <pathname> is not taken into account; if <filename> is not provided then "
				"the filename will be constructed, see the -P option.");
    CmdArgStr   pathname  ('P', "pathname","pathname","name of the path, where the ROOT file shall be saved to;"
	                                              "if <pathname> is not provided then the current directory is used;"
						      " the filename is generated from the partition name and the "
						      "date/time when the file is written.");
    CmdArgBool	append	  ('a', "append", "append the new data to an existing file given by <filename>"
					  " Only possible if <filename> is given");
    CmdArgInt   max_level ('l', "max_level","max_level","Restricts the logging to controllors"
				" up to level \'max_level\' in the control tree. If not set then"
			        " all levels available in the partition will be logged, "
				"if set to 0 only the RootController will be logged, if set to 1"
				" the segment controllers one level above will be logged etc.");
    CmdArgBool	with_sa_app	  ('w', "with_sa_app", "include the state aware applications that"
						       " publish RCStateInfo objects on the IS Server");
    CmdArgInt	publish_max_level ('u',"publish_max_level","publish_max_level","Sets the level"
				       "up to which diagrams are published after each transition. "
				       "By default it is set to 0 and only the RootController's diagrams will be published."
				       " If set to -1 the diagrams for all controllers will be published (use this carefully, "
				       "it may represent a significant load on the system for large partitions)."
				       " This level has to be less or equal than max_level, otherwise"
				       " the value of max_level will be used.");
    CmdArgBool	publish_tavg('t', "publish_tavg", "after each transition also publish a diagram"
				  " with the average times for all transitions for each controller included by the choice"
				  " of <publish_max_level>");
    CmdArgInt	waitIS('i', "wait_IS", "wait_IS","if set, the program will periodically"
				       " try to subscribe on the IS server at the beginning"
				       " and will stop running when the IS server goes down;"
				       " the period is the given value in milliseconds");
    CmdArgBool	resubscribe('r', "resubscribe", "try to resubscribe to a new IS server, when"
						" the current one was terminated. Only works"
						" when <wait_IS> is set to a positive value");
    CmdArgBool  saveCycle('c', "savecycle", "save the data in a seperate ROOT file for each cycle"
	                                    " (a cycle starts with the boot transition and ends with"
					    " the RootController being in the 'NONE' state)");
    CmdArgBool	noOH('n', "no_oh", "don't use OH, only log the transition times, nothing will be published, no OH service"
	                           " setup");
    CmdArgBool	verbose('v', "verbose", "verbose mode (e.g. the RunCtrl tree at the beginning, etc.)");

    data="";
    max_level=-1;
    publish_max_level=0;
    waitIS=-1;

    CmdLine cmd(*argv, &partition, &data,&filename,&pathname,&append,&max_level,&with_sa_app,
		       &publish_max_level,&publish_tavg,&waitIS,&resubscribe,&saveCycle,&noOH,&verbose, NULL);

    IPCCore::init(argc, argv);

    CmdArgvIter  arg_iter(--argc, ++argv);

    cmd.description("Command line application to log the transition times of the controllers"
		    " in a given partition and publish them in the OH.\n"
		    "On receiving SIGTERM or SIGINT (same as Ctrl-C) the application will save the collected data to "
		    "the ROOT file and terminate. On receiving SIGUSR1 (=10) it will save the currently collected data to"
		    " to the file and continue the monitoring.\n\n"
		    "Type 'opmon_monitor --commands' to see the commands that can be send from OH to this application.\n");


    cmd.parse(arg_iter);

    if ( (publish_max_level>max_level && max_level!=-1) || (publish_max_level==-1 && publish_max_level!=-1) )
       	PUBLISH_LEVEL=max_level;
    else
	PUBLISH_LEVEL=publish_max_level;
    PUBLISH_TAVG=publish_tavg;

    WAIT_IS=waitIS;
    if (WAIT_IS<=0)
       	RESUBSCRIBE=false;
    else
       	RESUBSCRIBE=resubscribe;

    fileName=filename.isNULL() ? "" : filename;
    if (fileName!="")
	FILENAME_BYUSER=true;
    else
	FILENAME_BYUSER=false;

    if (pathname.isNULL()) {
	pathName="";
    } else {
	pathName=pathname;
	if (!opendir(pathName.c_str())) {
	    cerr << "ERROR: Path '"<<pathname<< "' does not exist! Program canceled!" << endl;
	    return 1;
	}
	if (pathName[pathName.length()-1]!='/') pathName.append("/");
    }
    dataName=data;
    LOG_LEVEL=max_level;
    WITH_SA=with_sa_app;
    VERBOSE=verbose;
    NO_OH=noOH;
    gRawProv=NULL;
    gRootProv=NULL;
    rcst=NULL;
    stopAll=false;
    TO_BE_SAVED=true;
    SAVE_SEPERATE_CYCLE=saveCycle;
    if (SAVE_SEPERATE_CYCLE) {
	fileName="";
	FILENAME_BYUSER=false;
    }
    CYCLE_NUMBER=1;
    rootTiming=new RCTimeSequence(ROOT_CTRL);


    if (!partition.isNULL())
	partName=partition;
    char* TDAQ_PARTITION=getenv("TDAQ_PARTITION");
    if (TDAQ_PARTITION) {
	if (partition.isNULL())
	    partName=TDAQ_PARTITION;
	else
	    if ( partName !=str(TDAQ_PARTITION) ) {
		if (VERBOSE) cout << "Settings seem to be wrong: TDAQ_PARTIION="<< TDAQ_PARTITION << " but you chose partition: "<< partName << endl << "Will use the last one." << endl;
		setenv("TDAQ_PARTITION",partName.c_str(),1);
	    }
    } else { // if (!TDAQ_PARTITION)
	if (!partition.isNULL())
	    setenv("TDAQ_PARTITION",partName.c_str(),1);
	else {
	    cerr << "ERROR: No partition name given. Check your settings!" << endl;
	    return 1;
	}
    }
    cout << "Partition to monitor: " << partName << endl;

    // Create the instance of partition
    ipcPartition=new IPCPartition(partName.c_str());
    if (!ipcPartition) {
       	cerr << "FATAL: Partition does not exist! Check your parameters!" << endl;
    }

    // Create InfoReceiver for subscriptions
    ISInfoReceiver rec(*ipcPartition);
    isRec = &rec;

    if (append) loadFromRoot(str(filename));

    // subscribe in IS
    if (!subscribeNew()) {
	cerr << "FATAL: Could not do setup for logging!" << endl;
	return 1;
    }

    while (RESUBSCRIBE && !stopAll) {
	unsubscribe();

	saveToRoot();

	if (!subscribeNew()) {
	    cerr << "FATAL: Could not do resubscription!" << endl;
	    break;
	}
    }

    cout<<"InfoReceiver for RunCtrl stopped"<<endl;

    unsubscribe();

    // save all the data to the root file
    saveToRoot();

    cleanUpGlobals();

    if(RESTART_MYSELF.load() == true) {
        ::execv(ARGV[0], ARGV);
    }

    return 0;
}
