/** ----------------------------------------------------------------
 *  TransLogCommands.h - contains inline class used in monitor.cxx
 *
 *  class TransLogCommandListener
 *  inline implemented class that inherits from OHCommandListener
 *  in order to be able to receive commands from OHCommandSender
 *
 *  author: Herbert Kaiser, Sep 2006
 *  ----------------------------------------------------------------
 */

class TransLogCommandListener : public OHCommandListener
{
public:
    static bool verbose;

    static bool publishAvgList(const string& name,const string& title, const string& transName, const vector<string>& list,OHRootProvider* prov) {
	TH1D h(name.c_str(),title.c_str(),list.size(),0,0);
	const vector<double>* hp;
	char en[100];
	map<string,RCTransLog*>::const_iterator findIt;
	for (size_t i=0;i<list.size();i++) {
	    findIt = rcData.find(list[i]);
	    if (findIt==rcData.end()) {
		sprintf(en,"%s (N/A)",list[i].c_str());
		h.Fill(en,0);
	    }
	    else if (findIt->second) {
		hp=findIt->second->getTransTimes(transName);
		if (hp) {
		    sprintf(en,"%s (%d)",list[i].c_str(),(int)hp->size());
		    h.Fill(en,average(*hp));
		} else {
		    sprintf(en,"%s (NONE)",list[i].c_str());
		    h.Fill(en,0.0);
		}
	    }
	    else {
		sprintf(en,"%s (NONE)",list[i].c_str());
		h.Fill(en,0.0);
	    }
	}
	h.GetXaxis()->SetTitle("controller");
	h.GetYaxis()->SetTitle("time [millisec]");
	try {
	    prov->publish( h, name, false);
	} catch (daq::oh::Exception&) {
	    cerr << "ERROR in publishAvgList(), publish failed!" << endl;
	    return false;
	}
	return true;
    }

    static bool publishPointList(int number, const string& name,const string& title, const string& transName, const vector<string>& list,OHRootProvider* prov) {
	number--;
	TH1D h(name.c_str(),title.c_str(),list.size(),0,0);
	const vector<double>* hp;
	char en[100];
	map<string,RCTransLog*>::const_iterator findIt;
	int actual_number;
	for (size_t i=0;i<list.size();i++) {
	    findIt = rcData.find(list[i]);
	    if (findIt==rcData.end()) {
		sprintf(en,"%s (N/A)",list[i].c_str());
		h.Fill(en,0);
	    }
	    else if (findIt->second) {
		hp=findIt->second->getTransTimes(transName);
		if (hp) {
		    if (number<=0 || number>=(int)hp->size() )
			actual_number=(hp->size())-1;
		    else
			actual_number=number;
		    sprintf(en,"%s (%d)",list[i].c_str(),actual_number);
		    h.Fill(en,(*hp)[actual_number]);
		} else {
		    sprintf(en,"%s (%d)",list[i].c_str(),0);
		    h.Fill(en,0.0);
		}
	    }
	    else {
		sprintf(en,"%s (NONE)",list[i].c_str());
		h.Fill(en,0.0);
	    }
	}
	h.GetXaxis()->SetTitle("controller");
	h.GetYaxis()->SetTitle("time [millisec]");
	try {
	    prov->publish( h, name, false);
	} catch (daq::oh::Exception&) {
	    cerr << "ERROR in publishPointList(), publish failed!" << endl;
	    return false;
	}
	return true;
    }

    static void showAvgForAllTransitions(const string& controller, int level=-1) {
	if (verbose)
	cout << "Command PAvgCtrl(publish average of all transitions) for controller " << controller << endl;
    map<string,RCTransLog*>::iterator it;

	// special treatment if one wants all the controllers
	if (controller=="ALL") {
	    cout << "All Controllers wanted, MAX_level: " << level << endl;
	    map<string,RCTransLog*>::iterator it;
	    for (it=rcData.begin();it!=rcData.end();it++) {
		if ( rcst->getLevel(it->first) <= level )
		    it->second->publishAllAvg(gRootProv);
	    }
	    return;
	}

	it=rcData.find(controller);
	if (it==rcData.end()) {
	    cout<<"Cannot execute PAvgCtrl, controller "<<controller<<" not found!"<<endl;
	    return;
	}
	it->second->publishAllAvg(gRootProv);
    }

    static void showAvgForAllControllers(const string& transition, int level) {
	if (verbose)
	    cout << "Command PAvgAll(publish average of all controllers) for transition " << transition << "  MAX_level: " << level << endl;
	vector<string> list;
	rcst->getAllLevelOrdered(list,level);
	ostringstream ostr;
	if (level>-1) ostr << "_Lvl<="<<level;
	ostringstream o2str;
	if (level>-1) o2str << "with level <= " <<level;
	publishAvgList(string(OH_FORENAME)+"Avg"+ostr.str()+"/"+transition,"Average of all controllers"+o2str.str()+": "+transition,transition,list,gRootProv);
    }

    static void showAllTransitionDiagrams(const string& controller) {
	if (verbose)
	    cout << "Command PDiaCtrl(publish all diagrams) for controller " << controller << endl;
	map<string,RCTransLog*>::iterator it=rcData.find(controller);
	if (it==rcData.end()) {
	    cout << "Cannot execute PDiaCtrl, controller "<<controller<<" not found!" << endl;
	    return;
	}
	it->second->publishAll(gRawProv);
    }

    static void showAllDiagrams(int level) {
	if (verbose)
	    cout << "Command PDiaCtrlAll(publish all diagrams) MAX_level: " << level << endl;
	map<string,RCTransLog*>::iterator it;
	for (it=rcData.begin();it!=rcData.end();it++) {
	    if ( rcst->getLevel(it->first) <= level )
		it->second->publishAll(gRawProv);
	}
    }

    static void showThisDiagram(const string& controller, const string& transition) {
	if (verbose)
	    cout << "Command PDia(publish this diagram) for controller " << controller << " and transition " << transition << endl;
	map<string,RCTransLog*>::iterator it=rcData.find(controller);
	if (it==rcData.end()) {
	    cout << "Cannot execute PDia, controller "<<controller<<" not found!" << endl;
	    return;
	}
	if (!it->second->publish(gRawProv,transition))
	    cout<<"Could not publish transition "<<transition<<" for controller "<<controller<<"!"<<endl;
    }

    static void showThisLevelsDiagrams(const string& controller, const string& transition) {
	if (verbose)
	    cout << "Command PDiaLvl(publish this level's diagrams) for controller/level " << controller << " and transition " << transition;
	int level=rcst->getLevel(controller);
	if (level==-1) {
	    istringstream istr(controller);
	    istr >> level;
	}
	if (verbose)
	    cout << " -> level is " << level << endl;
	map<string,RCTransLog*>::iterator it;
	for (it=rcData.begin();it!=rcData.end();it++) {
	    if ( rcst->getLevel(it->first) == level )
		it->second->publishAll(gRawProv);
	}
    }

    static void showAvgThisLevel(const string& controller, const string& transition) {
	if (verbose)
	    cout << "Command PAvgLvl(publish average of all controllers of this level) for controller/level " << controller << " and for transition " << transition;

	int level=rcst->getLevel(controller);
	if (level==-1) {
	    istringstream istr(controller);
	    if (!(istr >> level)) {
		cout << "Command PAvgLvl failed, level could not be determined." << endl;
		return;
	    }
	}
	if (verbose)
	    cout << " -> level is " << level << endl;
	ostringstream ostr;
	ostr << level;
	vector<string> list;
	rcst->getAllInThisLevel(level,list);
	publishAvgList(string(OH_FORENAME)+"Avg_Lvl="+ostr.str()+"/"+transition,"Average of each controller in level "+ostr.str()+": "+transition,transition,list,gRootProv);
    }
    
    static void showLastThisLevel(const string& controller, const string& transition, int runnumber) {
	if (verbose)
	    cout << "Command PLastLvl(publish last transition for each controller of the level this controller is in) for controller/level " << controller << " and for transition " << transition;

	int level=rcst->getLevel(controller);
	if (level==-1) {
	    istringstream istr(controller);
	    if (!(istr >> level)) {
		cout << "Command PLastLvl failed, level could not be determined." << endl;
		return;
	    }
	}
	if (runnumber>0 && verbose)
	    cout << "(takes value of transition number "<<runnumber<<")";
	if (verbose)
	    cout << "; level is " << level << endl;
	ostringstream ostr;
	ostr << level;
	vector<string> list;
	rcst->getAllInThisLevel(level,list);
	ostringstream exstr;
	ostringstream titexstr;
	if (runnumber>0) {
	    exstr << "Ex" << runnumber;
	    titexstr << "Transition number "<< runnumber << " ";
	} else {
	    exstr << "Last";
	    titexstr << "Last transition ";
	}
	publishPointList(runnumber,string(OH_FORENAME)+exstr.str()+"_Lvl="+ostr.str()+"/"+transition,string(titexstr.str())+"of each controller in level "+ostr.str()+": "+transition,transition,list,gRootProv);
    }

    static void showAvgThisBranchLevel(const string& controller, const string& transition) {
	string parent=rcst->getParent(controller);
	if (verbose)
	    cout << "Command PAvgSParent(publish average of all controllers having the same parent controller) for controller " << controller << " and for transition " << transition << " -> parent is " << parent << endl;

	if (parent=="N/A") {
	    cout << "Cannot execute PAvgSParent, controller "<<controller<<" not found!" << endl;
	    return;
	}
	if (parent=="IS_ROOT") {
	    cout << "Cannot execute PAvgSParent for RootController!" << endl;
	    return;
	}
	vector<string> list;
	rcst->getChildren(parent,list);
	publishAvgList(string(OH_FORENAME)+"Avg_PARENT="+parent+"/"+transition,"Average of all controllers with parent controller "+parent+": "+transition,transition,list,gRootProv);
    }


    static int parseLevel(string& lstr) {
	if (lstr.length()==0) return -1;
	int level;
	istringstream istr(lstr);
	if (istr >> level)
	    return level;
	else
	    return -1;
    }

    void command ( const std::string & name, const std::string & cmd )
    {
	// parse name to retrieve controller and tranistion
	string controller=name.substr(OH_FORENAME_N);
	string transition=controller.substr(controller.find_last_of('/')+1);
	controller.erase(controller.find_last_of('/'));
	controller=controller.substr(controller.find_last_of('/')+1);

	// parse command to get up to 2 parameters
	string com=cmd.substr(0,cmd.find_first_of(':'));
	string arg1(""),arg2("");
	if (cmd.find_first_of(':')!=string::npos) {
	    arg1=cmd.substr(cmd.find_first_of(':')+1);
	    if (arg1.find_first_of(':')!=string::npos) {
		arg2=arg1.substr(arg1.find_first_of(':')+1);
		arg1.erase(arg1.find_first_of(':'));
	    }
	}
	std::transform(com.begin(), com.end(), com.begin(),(int(*)(int)) toupper);

	verbose=true;
	if (com=="PAVGCTRL") showAvgForAllTransitions(controller,parseLevel(arg1));
	else if (com=="PDIACTRL") showAllTransitionDiagrams(controller);
	else if (com=="PDIACTRLALL") showAllDiagrams(parseLevel(arg1));
	else if (com=="PDIA") showThisDiagram(controller, transition);
	else if (com=="PDIALVL") showThisLevelsDiagrams(controller, transition);
	else if (com=="PAVGLVL") showAvgThisLevel(controller, transition);
	else if (com=="PAVGALL") showAvgForAllControllers(transition,parseLevel(arg1));
	else if (com=="PAVGSPARENT") showAvgThisBranchLevel(controller, transition);
	else if (com=="PLASTLVL") showLastThisLevel(controller, transition, parseLevel(arg1));
	// ideas for other not realized (and maybe not necessery) commands
	//else if (com=="PAvgHistLvl") showAvgHistThisLevel(controller, transition);
	//else if (com=="PAvgHistAll") showAvgHistAll(transition,parseLevel(arg1));
	//else if (com=="PAvgHistSParent") showAvgHistThisBranchLevel(controller, transition);
	//else if (com=="PLastAll") showLastAll(transition,parseLevel(arg1), parseLevel(arg2));
	//else if (com=="PLastSParent") showLastThisBranchLevel(controller, transition, parseLevel(arg1));
	//else if (com=="PLastHistLvl") showLastThisLevel(controller, transition, parseLevel(arg1));
	//else if (com=="PLastHistAll") showLastAll(transition,parseLevel(arg1), parseLevel(arg2));
	//else if (com=="PLastHistSParent") showLastThisBranchLevel(controller, transition, parseLevel(arg1));
	verbose=false;
    }

    void command ( const std::string & cmd )
    {
	// parse command to get up to 4 parameters
	string com=cmd.substr(0,cmd.find_first_of(':'));
	string arg1(""),arg2(""),arg3(""),arg4("");
	if (cmd.find_first_of(':')!=string::npos) {
	    arg1=cmd.substr(cmd.find_first_of(':')+1);
	    if (arg1.find_first_of(':')!=string::npos) {
		arg2=arg1.substr(arg1.find_first_of(':')+1);
		arg1.erase(arg1.find_first_of(':'));
		if (arg2.find_first_of(':')!=string::npos) {
		    arg3=arg2.substr(arg2.find_first_of(':')+1);
		    arg2.erase(arg2.find_first_of(':'));
		    if (arg3.find_first_of(':')!=string::npos) {
			arg4=arg3.substr(arg3.find_first_of(':')+1);
			arg3.erase(arg3.find_first_of(':'));
		    }
		}
	    }
	}
	std::transform(com.begin(), com.end(), com.begin(),(int(*)(int)) toupper);

	if ( com == "SAVE" ) { 
	    saveToRoot();
	    return;
	}
	if ( com == "EXIT" ) { 
	    stopAll=true;
        daq::ipc::signal::raise();
	}

	verbose=true;
	if (com=="PAVGCTRL") showAvgForAllTransitions(arg1,parseLevel(arg2));
	else if (com=="PDIACTRL") showAllTransitionDiagrams(arg1);
	else if (com=="PDIACTRL") showAllDiagrams(parseLevel(arg1));
	else if (com=="PDIA") showThisDiagram(arg1,arg2);
	else if (com=="PDIALVL") showThisLevelsDiagrams(arg1,arg2);
	else if (com=="PAVGLVL") showAvgThisLevel(arg1,arg2);
	else if (com=="PAVGALL") showAvgForAllControllers(arg1,parseLevel(arg2));
	else if (com=="PAVGSPARENT") showAvgThisBranchLevel(arg1,arg2);
	else if (com=="PLASTLVL") showLastThisLevel(arg1, arg2, parseLevel(arg3));
	// ideas for other not realized (and maybe not necessery) commands
	//else if (com=="PAvgHistLvl") showAvgHistThisLevel(arg1,arg2);
	//else if (com=="PAvgHistAll") showAvgHistAll(arg1,arg2);
	//else if (com=="PAvgHistSParent") showAvgHistThisBranchLevel(arg1,arg2);
	//else if (com=="PLastAll") showLastAll(arg1, parseLevel(arg2), parseLevel(arg3));
	//else if (com=="PLastSParent") showLastThisBranchLevel(arg1, arg2, parseLevel(arg3));
	//else if (com=="PLastHistLvl") showLastThisLevel(arg1, arg2, parseLevel(arg3));
	//else if (com=="PLastHistAll") showLastAll(arg1, parseLevel(arg2), parseLevel(arg3));
	//else if (com=="PLastHistSParent") showLastThisBranchLevel(arg1, arg2, parseLevel(arg3));
	verbose=false;
    }
};
bool TransLogCommandListener::verbose=false;
