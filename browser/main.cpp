/*
 * main.cpp
 *
 *  Created on: Nov 11, 2020
 *      Author: kolos
 */
#include <iomanip>
#include <iostream>

#include <boost/program_options.hpp>

#include <TApplication.h>
#include <TBrowser.h>
#include <TDirectory.h>
#include <TEnv.h>
#include <TFile.h>
#include <TImage.h>
#include <TObject.h>
#include <TString.h>
#include <TSystemDirectory.h>
#include <TGFrame.h>
#include <TGMimeTypes.h>
#include <TGComboBox.h>
#include <TGFileBrowser.h>
#include <TRootBrowser.h>

namespace po = boost::program_options;

struct RootBrowser: public TRootBrowser {
    struct FileBrowser: public TGFileBrowser {
        TGComboBox * get() { return fDrawOption; }
    };
    TGComboBox * get() { return ((FileBrowser*)fActBrowser)->get(); }
};

std::string makeSortableName(const char * n)
{
    std::tm t = {};
    std::string name(n);
    size_t pos = name.find('_');
    if (pos != std::string::npos) {
        name = name.substr(pos + 1);
        pos = name.find('.');
        if (pos != std::string::npos) {
            name = name.substr(0, pos);
        } else {
            return n;
        }
    } else {
        return n;
    }

    std::istringstream ss(name);
    ss >> std::get_time(&t, "%Y_%b_%d_%H_%M_%S");
    if (ss.fail()) {
        return name;
    }
    std::stringstream out;
    out << std::put_time(&t, "%Y/%m/%d %H:%M:%S");
    return out.str();
}

int main(int argc, char **argv) {
    po::options_description desc(
            "This program can browse Run Control transitions timing files recorded by opmon_logger");

    std::string file_name;
    std::string directory_name;
    desc.add_options()("help,h", "Print help message")
    ("file-name,f", po::value<std::string>(&file_name), "file name")
    ("directory-name,d", po::value<std::string>(&directory_name), "directory name");

    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    } catch (std::exception &ex) {
        std::cerr << ex.what() << std::endl;
        desc.print(std::cout);
        return EXIT_FAILURE;
    }

    if (vm.count("help")) {
        desc.print(std::cout);
        return EXIT_FAILURE;
    }

    TApplication app("RC Transition Browser", 0, 0);

    gClient->GetMimeTypeList()->AddType("root/tmultigraph", "TMultiGraph",
            "h1_s.xpm", "h1_t.xpm", "->Browse()");

    gClient->GetMimeTypeList()->AddType("root/th2f", "TH2F",
            "h3_s.xpm", "h3_t.xpm", "->Browse()");

    gClient->GetMimeTypeList()->AddType("root/tlist", "TList",
            "branch_folder_s.xpm", "branch_folder_t.xpm", "->Browse()");

    gEnv->SetValue("TGraph.BrowseOption", "A");

    TList * t_files = 0;
    TFile * file = 0;
    if (vm.count("file-name")) {
        file = TFile::Open(file_name.c_str(), "READ");
        if (not file) {
            std::cerr << "Can not open '" << file_name << "' file" << std::endl;
            return EXIT_FAILURE;
        }
    } else if (vm.count("directory-name")) {
        if (directory_name.back() != '/') {
            directory_name += '/';
        }
        TSystemDirectory * dir = new TSystemDirectory(
                directory_name.c_str(), directory_name.c_str());
        if (dir->IsZombie()) {
            std::cerr << "Can not open '" << directory_name << "' directory" << std::endl;
            return EXIT_FAILURE;
        }

        t_files = new TList();
        t_files->SetName(directory_name.c_str());
        TList * files = dir->GetListOfFiles();
        if (files) {
            TSystemFile *it;
            TIter next(files);
            while ((it = (TSystemFile*) next())) {
                TString fname = it->GetName();
                if (not it->IsDirectory() && fname.EndsWith(".root")) {
                    std::string pathname = directory_name + fname.Data();
                    TFile * tf = TFile::Open(pathname.c_str(), "READ");
                    if (not tf) {
                        std::cerr << "Can not open '" << pathname << "' file" << std::endl;
                        continue;
                    }
                    std::string sn = makeSortableName(fname);
                    tf->SetName(sn.c_str());
                    t_files->Add(tf);
                }
            }
        }
        if (t_files->IsEmpty()) {
            std::cerr << "No ROOT files are found in '"
                    << directory_name << "' directory" << std::endl;
            return EXIT_FAILURE;
        }
        t_files->Sort(0);
    } else {
        std::cerr << "Either file or directory name shall be specified" << std::endl;
        desc.print(std::cout);
        return EXIT_FAILURE;
    }

    if (t_files) {
        file = (TFile*)t_files->First();
        t_files->Remove(file);
    }
    TBrowser browser("RC Transition Browser", file, "RC Transition Browser");

    TRootBrowser * impl = (TRootBrowser *)browser.GetBrowserImp();
    impl->Connect(impl, "CloseWindow()", "TApplication", &app, "Terminate()");
    impl->MoveResize(300, 200, 1400, 800);

    ((RootBrowser*)impl)->get()->Select(6, 1);

    for (const auto & f : *t_files) {
        browser.Add(f);
    }

    app.Run();

    return 0;
}
