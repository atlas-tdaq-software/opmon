package opmon

author	Herbert.Kaiser@uni-konstanz.de
manager	Serguei.Kolos@cern.ch

use OnlinePolicy
use cmdl
use ipc
use dal
use config
use is
use oh
use ROOT    *    DQMCExternal
use Boost * TDAQCExternal

public
#################################################################################
# exported
#################################################################################
macro sw.repository.binary.opmon_monitor:name			"opmon_monitor"
macro sw.repository.binary.opmon_analyser:name			"opmon_analyser"
macro sw.repository.binary.opmon_analyser:name			"opmon_merger"

private
#################################################################################
library         opmon      $(lib_opts)     "../src/*.cxx \
					    $(bin)RCTransLogDict.cxx"

#################################################################################
application	opmon_monitor		-no_prototypes	"../monitor/*.cxx \
							 ../monitor/*.imp"
application	opmon_analyser		-no_prototypes	"../analyser/RCT*.cxx \
							 $(bin)AnalyserDict.cxx \
							 ../analyser/analyser.cxx"
application	opmon_merger		-no_prototypes	"../analyser/RCTFile*.cxx \
							 $(bin)MergerDict.cxx \
							 ../analyser/merger.cxx"

#################################################################################
# link against other libraries from used packages
#################################################################################
macro	common_linkopts	"-lohroot -ldaq-core-dal $(socket_libs)"

#macro	root_libs			"-lHist -lCore -lMatrix -lCint \
#					-lHistPainter -lX3d -lGX11 -lGui \
#					-lGraf -lGraf3d -lGpad"

macro	opmon_shlibflags		"$(common_linkopts)"
macro	opmon_monitorlinkopts		"-lopmon $(common_linkopts) -lboost_date_time-$(boost_libsuffix) -lconfig"
macro	opmon_analyserlinkopts		"-lopmon $(common_linkopts) -lGui"
macro	opmon_mergerlinkopts		"-lopmon $(common_linkopts)"

#################################################################################
# dependencies
#################################################################################

macro  opmon_dependencies		"root_dict_opmon"
macro  opmon_monitor_dependencies	"opmon"
macro  opmon_analyser_dependencies	"opmon root_dict_analyser" 
macro  opmon_merger_dependencies	"opmon root_dict_merger" 

action root_dict_opmon	  "( LD_LIBRARY_PATH=$(root_sys)/lib:$(LD_LIBRARY_PATH);  \
			   export LD_LIBRARY_PATH;				\
			   ROOTSYS=$(root_sys);	export ROOTSYS;			\
			   $(root_sys)/bin/rootcint -v -f $(bin)RCTransLogDict.cxx -c ../opmon/RCT*.h \
			   ../src/LinkDef.h )"
action root_dict_analyser "( LD_LIBRARY_PATH=$(root_sys)/lib:$(LD_LIBRARY_PATH);  \
			   export LD_LIBRARY_PATH;				\
			   ROOTSYS=$(root_sys);	export ROOTSYS;			\
			   $(root_sys)/bin/rootcint -v -f $(bin)AnalyserDict.cxx -c ../analyser/RCT*.h \
			   ../analyser/LinkDef.h )"

action root_dict_merger   "( LD_LIBRARY_PATH=$(root_sys)/lib:$(LD_LIBRARY_PATH);  \
			   export LD_LIBRARY_PATH;				\
			   ROOTSYS=$(root_sys);	export ROOTSYS;			\
			   $(root_sys)/bin/rootcint -v -f $(bin)MergerDict.cxx -c ../analyser/RCTFile*.h )"

#################################################################################
# installation
#################################################################################
macro           opmon_libs	"libopmon.so"
				
macro           opmon_apps	"opmon_monitor opmon_analyser opmon_merger"
		
apply_pattern   install_libs		files="$(opmon_libs) *.pcm"
apply_pattern   install_apps		files="$(opmon_apps)"

########################################################################################################
#       Documentation
########################################################################################################
action	document_generation	"\
	( cd ../$(tag); mkdir -p dev; doxygen ../doc/dev/doxygen.conf; cd dev/latex; make pdf;		\
		mkdir -p $(docdir)/opmon/online-doc;							\
		cp refman.pdf $(docdir)/opmon/online-doc/developers-ug.pdf; cd ..;				\
		cp -r html $(docdir)/opmon/online-doc/developers );					\
	( cd ../$(tag); mkdir -p user; doxygen ../doc/user/doxygen.conf; cd user/latex; make pdf;	\
		cp refman.pdf $(docdir)/opmon/online-doc/users-ug.pdf; cd ..;				\
		cp -r html $(docdir)/opmon/online-doc/users; )"


macro_remove cppflags "-W"
					
