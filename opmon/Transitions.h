/** ------------------------------------------------------------------------
 *  Transitions.h - header file that provides constants for transition order
 *
 *  This file is used to make the order of the transitions available
 *  in opmon_analyser. It also defines the order of the internal
 *  transitions for 'boot', 'configure', etc.
 *
 *  You can easily change values or add special combinations to make
 *  them available in opmon_analyser. This is the only place where
 *  the actual state names are used.
 *
 *  Two sets of orders are provided in this file, one for tdaq-01-06-00
 *  and one for tdaq-01-04-00 (just added 'Old' to the constant names).
 *  
 *  author: Herbert Kaiser, Sep 2006
 *  update to revised state transitions from tdaq-01-09-00 onwards: D. Burckhart-Chromek
 *  ------------------------------------------------------------------------------------
 */

#ifndef TRANSITION_NAMES_H
#define TRANSITION_NAMES_H

// ######################################################################################
// this defines the combined transitions, for name changes it's just enough to change
// names here

// --------------------------------------------------------------------------------------
// part for tdaq-05-0X

const char * const bootTrans[]=	    {"initialize",
				     "NONE->INITIAL",0};
const char * const configureTrans[]={"configure",
				     "INITIAL->CONFIGURED","CONFIGURED->CONNECTED",0};
const char * const startTrans[]=    {"start",
				     "CONNECTED->RUNNING",0};
const char * const stopTrans[]=	    {"stop",
				     "RUNNING->ROIBSTOPPED",
				     "ROIBSTOPPED->DCSTOPPED","DCSTOPPED->HLTSTOPPED",
				     "HLTSTOPPED->SFOSTOPPED","SFOSTOPPED->GTHSTOPPED","GTHSTOPPED->CONNECTED",0};
const char*const unconfigureTrans[]={"unconfigure",
				     "CONNECTED->CONFIGURED","CONFIGURED->INITIAL",0};
const char * const shutdownTrans[]= {"shutdown",
				     "INITIAL->NONE",0};

// PUT in the right names here in the right order!
#define BUILD_COMBINED_TRANS_VECTOR(name)	\
    vector<const char* const*> name;		\
    name.push_back(bootTrans);			\
    name.push_back(configureTrans);		\
    name.push_back(startTrans);			\
    name.push_back(stopTrans);			\
    name.push_back(unconfigureTrans);		\
    name.push_back(shutdownTrans);

// this array represents the internal transitions and their order for the average
// diagrams showing the transitions of one controller
const char* const transitionsOrder[]= {"NONE->INITIAL","INITIAL->CONFIGURED",
	    "CONFIGURED->CONNECTED","CONNECTED->RUNNING",
	    "RUNNING->ROIBSTOPPED","ROIBSTOPPED->DCSTOPPED",
	    "DCSTOPPED->HLTSTOPPED", "HLTSTOPPED->SFOSTOPPED",
	    "SFOSTOPPED->GTHSTOPPED","GTHSTOPPED->CONNECTED",
	    "CONNECTED->CONFIGURED","CONFIGURED->INITIAL",
	    "INITIAL->NONE",0};

#endif // TRANSITION_NAMES_H

