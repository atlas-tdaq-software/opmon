/** ------------------------------------------------------------
 *  RCTimeSequence.h - header file for class RCTimeSequence
 *
 *  Contains a sequence of pairs with transition name and
 *  the time the transition ended
 *  One objects of this class is saved in the ROOT file
 *  created by opmon_monitor
 *  for the timing of the RootController
 *
 *  author: Herbert Kaiser, Mar 2007
 *  ------------------------------------------------------------
 */

#ifndef RC_TIME_SEQUENCE_H
#define RC_TIME_SEQUENCE_H

#include<string>
#include<list>
#include<vector>
#include<TObject.h>

using std::string;
using std::pair;
using std::vector;

class RCTimeSequence {
private:
	string name;				// name for the list in database
	std::list<string> transSeq;	        // the names sequence
	std::list<string> timeSeq;	        // the times sequence

public:
	// constructors
	RCTimeSequence () : name("") {;};
	RCTimeSequence (const std::string& _name) : name(_name) {;};

	// destructor
	virtual ~RCTimeSequence() {};

	const string& getName() const { return name; };
	void setName(const string& n) { name=n; };

	pair< const std::list<string>* ,const std::list<string>* > getSequence() const;
	void recordValue(const string& transName,  const string& time);
	void getCombinedSequence(const vector<const char* const*>& combVec,std::list<string>& names,std::list<string>& times) const;

	ClassDef(RCTimeSequence, 1)	// class for logging the timing of transitions for one controller

};

#endif // RC_TIME_SEQUENCE_H

