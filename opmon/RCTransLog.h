/** ------------------------------------------------------------
 *  RCTransLog.h - header file for class RCTransLog
 *
 *  Contains the collected transition times for one controller.
 *  A set of objects of this class is saved in the ROOT file
 *  created by opmon_monitor
 *  As well it contains some methods to publish diagrams
 *  of the data in OH and some constants concerning OH
 *  used by opmon_monitor
 *
 *  author: Herbert Kaiser, Sep 2006
 *  ------------------------------------------------------------
 */

#ifndef RC_TRANS_LOG_H
#define RC_TRANS_LOG_H

#include <string>
#include <map>
#include <vector>
#include <TObject.h>

using namespace std;

// name of Histogramming IS server
#define IS_HISTO "Histogramming"
// name of OH provider
#define OH_PROV_NAME "TransitionTimes"
// forename for the histograms: IS_HISTO.OH_PROV_NAME./MON/
#define OH_FORENAME "MON/trans_log/"
// length of OH_FORENAME
#define OH_FORENAME_N 14

// build average of a double vector
inline double average(const vector<double>& v) {
    if (v.size()==0) return 0.0;
    double r=0.0;
    for (vector<double>::const_iterator it=v.begin(); it != v.end(); it++) {
	r+=*it;
    }
    return r/v.size();
}

class OHBins;
class OHRootProvider;
template <class T>
class OHRawProvider;

class RCTransLog {
private:
	string state;				// current state of controller
	string name;				// name of controller in database
	string fullName;			//! name with parents in controller tree (will be saved in obj name)
	map<string,vector<double> > stateMap;	// the actual data
	//void addTrans(const string& transName,  double transTime);
public:
	// constructors
	RCTransLog();
	RCTransLog(const string& _name,const string& _fullName);

	// destructor
	virtual ~RCTransLog();

	const string& getName() const { return name; }
	const string& getFullName() const { return fullName; }
	void setFullName(const string& n) { fullName=n; }
	string recordTrans(const string& newState,  double transTime);
	bool publish(OHRawProvider<OHBins>* ohProv,const string& transName);
	bool publishAll(OHRawProvider<OHBins>* ohProv);
	bool publishAllAvg(OHRootProvider* ohProv);
	void getTransList(vector<string>& transList) const;
	bool replaceValues(const string& transName,const vector<double>& values);
	const vector<double>* getTransTimes(const string& transName) const;
	const double getAverageTime(const string& transName) const;
	bool getCombinedTransTimes(const char* const* comb, vector<double>& values) const;
	void appendValues(RCTransLog* second);

	void deleteData();

	ClassDef(RCTransLog, 1)			// class for logging transitions of one controller

};


#endif // RC_TRANS_LOG_H

