/** ------------------------------------------------------------
 *  RCStringTree.h - header file for RCStringTree class
 *
 *  Contains the Run Control tree of a partition with segment
 *  controllers and (if requested) state aware applications.
 *  There are 2 possibilities to retrieve the tree:
 *  - Using DAL (oks or rdb)
 *    (this is used in opmon_monitor)
 *  - by providing a "branch ordered" list (can be retrieved
 *    with getAllBranchOrdered() ) with the full names of the
 *    controllers (can be retrieved with getFullName(ctlr_name)
 *    (this is used in opmon_analyser)
 *
 *  author: Herbert Kaiser, Sep 2006
 *  ------------------------------------------------------------
 */

#ifndef RC_STRING_TREE_H
#define RC_STRING_TREE_H

#include <iostream>
#include <map>
#include <vector>
#include "config/Configuration.h"


/** Nodes of the RunControl String tree */
struct RCStringTreeNode {
    RCStringTreeNode* parent; /**< pointer to parent */
    std::string name; /**< controller name */
    int level; /**< the tree level of this node */
    bool segment; /**< \c true if it's a segment controller */
    std::vector<RCStringTreeNode*>* children; /**< pointers to children */
};

namespace daq {
    namespace core {
        class Segment;
    }
}

/**
 * \brief Holds a RunControl tree with the controller names.
 * Useful for displaying the RunControl tree and to get lists with names
 * of segment controllers and state-aware applications.\n
 * \b IMPORTANT: You need to call \c refreshTree() to load the tree for the first time.\n
 * The database is read with DAL so you can choose between RDB and OKS.\n
 * For an output to standard out just set \c verbose=true in the constructor
 * and call \c refreshTree().\n
 * All the lists that are given as references e.g. in \c getChildren(name,childrenNames) are not emptied before
 * appending the data! If you don't want things to be appended, call e.g. \c childrenNames.clear() before 
 * the get method.
 */
class RCStringTree {
    public:
	const std::string partition; /**< partition to work on */
	const std::string data; /**< data base to work on */
	const bool verbose; /**< \b true if \c refreshTree() \c cout s the tree */
	const bool onlySegments; /**< only segment controllers in the tree */
	const int MAXLEVEL; /**< maximum level this tree is build of */
	const char nameSeperator; /**< seperator for full name creation */

	/** \brief Constructor for the tree
	 *  \param _partition partition to work on
	 *  \param _data      data base to work on
	 *  \param _verbose   set to \c true if you want \c refreshTree() to \c cout the tree
	 *  \param _onlySegments set to\c true to have only segment controllers in the tree
	 *  \param _MAXLEVEL  set to build the tree only up to certain level, -1 means full tree
	 */
	inline RCStringTree(const std::string& _partition, const std::string& _data,
	    const bool _verbose=false, const bool _onlySegments=false, const int _MAXLEVEL=-1) :
	    partition(_partition),data(_data),verbose(_verbose),
	    onlySegments(_onlySegments),MAXLEVEL(_MAXLEVEL),nameSeperator('/'),level(0),rootNode(NULL) {}

	/** constructor to get tree from full names list, names are given with
	 * \param seperator seperator used in the full name, '|' is
	 * used for the trans_log ROOT files */
	inline RCStringTree(const char seperator, const bool _verbose=false, const int _MAXLEVEL=-1 ) :
	    partition(),data(),verbose(_verbose),
	    onlySegments(false),MAXLEVEL(_MAXLEVEL),nameSeperator(seperator),level(0),rootNode(NULL) {}


	/** frees the memory */
	~RCStringTree();

	/** call this to refresh the data from the database */
	bool refreshTree();

	/** call this to refresh the data from list with full names, where the
	 * names are branch ordered like in the list you get from
	 * getAllBranchOrdered()
	 * \param getNext a function that iterates through the list. Each call
	 * gives back a pointer to the string thats next in the list of full
	 * names. If there is no next name NULL must be returned
	 */
	bool refreshTree(const char* (*getNext)() );

	/** look for a controller in the tree
	 *  \param name name of the controller to look for
	 *  \return wether the controller was found
	 */
	bool exists(const std::string& name) const;

	/** get the level of this controller
	 *  /param name name of the controller
	 *  /return the level of this controller in the RunCtrl tree, if \c name doesn't exist -1
	 */
	int getLevel(const std::string& name) const;

	/** get the root controller
	 *  call only after refresh, no tests!
	 *  /return the name of the RootController
	 */
	const std::string& getRootName() const {
	    return rootNode->name;
	}

	/** get the controller that is just above
	 *  /param name name of the controller to determine the parent controller
	 *  /return the name of the parent, if \c name doesn't exist "N/A", if it's the root "IS_ROOT"
	 */
	std::string getParent(const std::string& name) const;

	/** get a full name
	 *  \param name name of the controller, e.g.: \c SubSegmentCtrl
	 *  \return something like: \c RootController/Segment1Controller/SubSegmentCtrl, if \c name doesn't exist "N/A", if it's a state aware application ":APP" is put to the end of the name
	 */
	std::string getFullName(const std::string& name) const;

	/** get a list of children
	 *  \param name name of the controller
	 *  \param childrenNames the list to write to
	 */
	void getChildren(const std::string& name, std::vector<std::string>& childrenNames) const;


	/** get a list of all the sub controllers
	 *  \param name name of the controller
	 *  \param getMe if \c true this controller is also on the list
	 *  \param childrenNames list of sub controllers ordered by branches
	 */
	void getAllChildrenBranchOrdered(const std::string& name, std::vector<std::string>& childrenNames, bool getMe=false) const;


	/** get a list of all the sub controllers
	 *  \param name name of the controller
	 *  \param getMe if \c true this controller is also on the list
	 *  \param childrenNames list of sub controllers ordered by levels
	 */
	void getAllChildrenLevelOrdered(const std::string& name, std::vector<std::string>& childrenNames, bool getMe=false) const;

	/** get a list of all the controllers
	 *  \param names list of all controllers ordered by branch
	 */
	void getAllBranchOrdered(std::vector<std::string>& names, int max_level=-1) const;

	/** get a list of all the controllers
	 *  \param names list of all controllers ordered by level
	 */
	void getAllLevelOrdered(std::vector<std::string>& names, int max_level=-1) const;

	/** get a list of all the controllers having this level
	 *  \param name controller name
	 *  \param names list of controllers in this level
	 */
	void getAllInThisLevel(const std::string& name, std::vector<std::string>& names) const;

	/** get a list of all the controllers having this level number
	 *  \param lev number of the level, 0 is RootController level, 1 is one level above...
	 *  \param names list of controllers in this level
	 */
	void getAllInThisLevel(int lev, std::vector<std::string>& names) const;

	/** if it's a segment controller
	 *  \param name controller name
	 *  \return \c true if it's a segment controller, \c false otherwise
	 */
	bool isSegment(const std::string& name) const;

	/** info about relationships
	 *  \param name controller name
	 *  \param parent name of possible parent
	 *  \return true if \c parent is ancestor of \c name, \c false otherwise
	 */
	bool isParent(const std::string& name, const std::string& parent) const;

    private:
	int level;
	RCStringTreeNode* rootNode;
	std::map<std::string,RCStringTreeNode*> searchMap;
	void freeMemory();
	bool writeName(const std::string& fullname, std::string& name) const;
	int countLevel(const char* fullname) const;
	void addSubChildren(RCStringTreeNode* parent, const daq::core::Segment& segment, Configuration& config);
	bool addSubChildren(RCStringTreeNode* parent, const char* & cur, const char* (*getNext)() );
	void getAllChildrenBranchOrdered(const RCStringTreeNode* node, std::vector<std::string>& names, bool getMe, int max_level=-1, int min_level=0) const;
	void getAllChildrenLevelOrdered(const RCStringTreeNode* node, std::vector<std::string>& names, bool getMe, int max_level=-1) const;
	void appendSubChildren(std::vector<std::string>& names, const RCStringTreeNode* node, int max_level, int min_level) const;
	bool isParent(const RCStringTreeNode* ctrl, const RCStringTreeNode* parent) const;
};

#endif // RC_STRING_TREE_H

