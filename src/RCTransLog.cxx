/** ------------------------------------------------------------
 *  RCTransLog.cxx - implementation of class RCTransLog
 *  author: Herbert Kaiser, Sep 2006
 *  ------------------------------------------------------------
 */
#include <iostream>
#include <oh/OHRootProvider.h>
#include <oh/OHRawProvider.h>
#include "opmon/RCTransLog.h"

using std::cout;
using std::cerr;
using std::endl;

ClassImp(RCTransLog)


//void RCTransLog::addTrans(const string& transName,double transTime) {
    
//}

RCTransLog::RCTransLog() : state("UNKNOWN"),name(""),fullName("") { }
RCTransLog::RCTransLog(const string& _name,const string& _fullName) : state("UNKNOWN"),name(_name),fullName(_fullName) {}
RCTransLog::~RCTransLog() { }

string RCTransLog::recordTrans(const string& newState,double transTime) {
  std::string transName=state+"->"+newState; 
  if (newState!=state) {
    stateMap[transName].push_back(transTime);
    //    addTrans(transName,transTime);
    state=newState;
  }
  else {
    transName="";
  }
  //std::cout << "State is still " << state << std::endl;
  return transName;
}

const vector<double>* RCTransLog::getTransTimes(const string& transName) const {
  map<string,vector<double> >::const_iterator it=stateMap.find(transName);
  if (it!=stateMap.end()) {
    return &(it->second);
  }    
  return NULL;
 
}

bool RCTransLog::replaceValues(const string& transName,const vector<double>& values) {
  map<string,vector<double> >::iterator it=stateMap.find(transName);
  if (it==stateMap.end()) return false;
  it->second.clear();
  for (unsigned i=0;i<values.size();i++) {
    it->second.push_back(values[i]);
  }
  return true;
}

const double RCTransLog::getAverageTime(const string& transName) const {
  map<string,vector<double> >::const_iterator it=stateMap.find(transName);
  if (it!=stateMap.end())
    return average(it->second);
  return 0.0;
}

bool RCTransLog::publish(OHRawProvider<OHBins>* ohProv,const string& transName) {
    // don't publish transitions with UNKNOWN...
    if(transName == "UNKNOWN") return false;
    map<string,vector<double> >::const_iterator iter=stateMap.find(transName);
    if (iter==stateMap.end()) return false;

    try {
      //	std::cout << "in rctpublish before" << std::endl;
	ohProv->publish(string(OH_FORENAME)+fullName+"/"+transName,name+" - "+transName+" [millisec]",
			OHMakeAxis("transition number",iter->second.size(),0,1),&(iter->second)[0],static_cast<double*>(NULL),false,false);

    } catch (daq::oh::Exception & ex) {
	ers::error(ex);
	return false;
    }
    return true;
}

bool RCTransLog::publishAll(OHRawProvider<OHBins>* ohProv) {
    map<string,vector<double> >::const_iterator iter;
    for (iter=stateMap.begin();iter!=stateMap.end();iter++) {

      if (iter->first == "UNKNOWN") continue;
	try {
	  //	    std::cout << "in rctpublishAll before" << std::endl;
	    ohProv->publish(string(OH_FORENAME)+fullName+"/"+iter->first,name+" - "+iter->first+" [millisec]",OHMakeAxis("transition number",iter->second.size(),0,1),&(iter->second)[0],static_cast<double*>(NULL),false,false);
	  //  std::cout << "in rctpublishAll after" << std::endl;
	} catch (daq::oh::Exception & ex) {
	    ers::error(ex);
	    return false;
	}
    }
    return true;
}

bool RCTransLog::publishAllAvg(OHRootProvider* ohProv) {
    TH1D h("Averages",(name+" - Average transition times").c_str(),stateMap.size(),0,0);
    char en[100];
    map<string,vector<double> >::const_iterator iter;
    //  std::cout << "publishAllAbg before" << std::endl;
    for (iter=stateMap.begin();iter!=stateMap.end();iter++) {
      
	if (iter->first == "UNKNOWN") continue;
      
	sprintf(en,"%s (%.0lu)",iter->first.c_str(),iter->second.size());
	h.Fill(en,average(iter->second));
    }
    // std::cout << "publishAllAbg after" << std::endl;
    h.GetXaxis()->SetTitle("transition");
    h.GetYaxis()->SetTitle("time [millisec]");
    try {
	ohProv->publish( h, string(OH_FORENAME)+fullName+"/AllAVG", false);
    } catch (daq::oh::Exception & ex) {
	ers::error(ex);
	return false;
    }
    return true;
}


void RCTransLog::getTransList(vector<string>& list) const {
    list.resize(stateMap.size());
    map<string,vector<double> >::const_iterator iter;
    unsigned i;
    for (iter=stateMap.begin(),i=0;iter!=stateMap.end();iter++,i++) {
	list[i]=iter->first;
    }
}

bool RCTransLog::getCombinedTransTimes(const char* const* comb, vector<double>& values) const {
    const vector<double>* vals=getTransTimes(comb[1]);
    if (!vals) return false;
    values.resize(vals->size());
    memcpy(&values[0],&(*vals)[0],sizeof(double)*vals->size());
    unsigned j=2;
    bool notYetShown=true;
    while (comb[j]) {
	vals=getTransTimes(comb[j]);
	if (!vals) return false;
	if (vals->size()!=values.size()) {
	    if (notYetShown) {
		cout << "Problem during creating combined transition '"<<name<<":"<<comb[0]<<"': internal transitions do not have the same number of executions." << endl;
		notYetShown=false;
	    }
	    if (vals->size()<values.size())
		values.resize(vals->size());
	}
	for (unsigned k=0;k<vals->size();k++) {
	    if (k>=values.size()) break;
	    values[k]+=(*vals)[k];
	}
	j++;
    }
  if (!vals) return false;
    return true;
}


void RCTransLog::appendValues(RCTransLog* app) {
    map<string,vector<double> >::iterator iter;
    map<string,vector<double> >::const_iterator app_iter;
    for (app_iter=app->stateMap.begin();app_iter!=app->stateMap.end();app_iter++) {
	iter=stateMap.find(app_iter->first);
	if (iter!=stateMap.end()) {
	    vector<double>::const_iterator it;
	    for (it=app_iter->second.begin();it!=app_iter->second.end();it++)
		iter->second.push_back(*it);
	} else {
	    stateMap[app_iter->first]=app_iter->second;
	}
    }
    
}

void RCTransLog::deleteData() {
    stateMap.clear();
}


