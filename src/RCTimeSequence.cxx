/** ------------------------------------------------------------
 *  RCTimeSequence.cxx - implementation of class RCTimeSequence
 *  author: Herbert Kaiser, Sep 2006
 *  ------------------------------------------------------------
 */

#include <string.h>

#include <iostream>
#include "opmon/RCTimeSequence.h"

ClassImp(RCTimeSequence)

pair< const std::list<string>* ,const std::list<string>* > RCTimeSequence::getSequence() const {
    return pair< const std::list<string>* ,const std::list<string>* >(&transSeq,&timeSeq);
}

void RCTimeSequence::recordValue(const string& transName,  const string& time) {
    transSeq.push_back(transName);
    timeSeq.push_back(time);
}

void RCTimeSequence::getCombinedSequence(const vector<const char* const*>& combVec,std::list<string>& names,std::list<string>& times) const {
    names.clear();
    times.clear();

    std::list<string>::const_iterator it=transSeq.begin();
    std::list<string>::const_iterator timesIt=timeSeq.begin();
    std::list<string>::const_iterator innerIt;
    std::list<string>::const_iterator innerTimesIt;
    bool found;
    while ( it!=transSeq.end() ) {
	for (unsigned j=0;j<combVec.size();j++) {
	    if ( strcmp( (*it).c_str(),combVec[j][1] )==0 ) {
		innerIt=it;
		innerTimesIt=timesIt;
		found=true;
		int k=2;
		innerIt++;
		innerTimesIt++;
		while ( combVec[j][k] ) {
		    if ( (innerIt==transSeq.end()) || (strcmp( (*innerIt).c_str(),combVec[j][k] )!=0) ) {
			found=false;
			break;
		    }
		    k++;
		    innerIt++;
		    innerTimesIt++;
		}
		if (found) {
		    innerTimesIt--;
		    names.push_back( combVec[j][0] );
		    times.push_back( (*innerTimesIt) );
		}
	    }
	}
	it++;
	timesIt++;
    }
}

