/** ------------------------------------------------------------
 *  LinkDef.h - file for the production of the ROOT dictionary
 *
 *  used to generate dictionory for RCTransLog,
 *  to be able to save it in a ROOT file
 *
 *  it is intentionally in the src directory because you
 *  will never want to include it in your source code
 *  it's only used by rootcint
 *
 *  author: Herbert Kaiser, Sep 2006
 *  ------------------------------------------------------------
 */

#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class RCTransLog+;
#pragma link C++ class RCTimeSequence+;

#endif
