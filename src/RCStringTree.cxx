/** ------------------------------------------------------------
 *  RCStringTree.cxx - implementation of class RCStringTree
 *  author: Herbert Kaiser, Sep 2006
 *  ------------------------------------------------------------
 */

#include <sstream>
#include "dal/Partition.h"
#include "dal/RunControlApplicationBase.h"
#include "dal/BaseApplication.h"
#include "dal/OnlineSegment.h"
#include "dal/Segment.h"
#include "dal/util.h"
#include "config/Configuration.h"
#include "opmon/RCStringTree.h"

RCStringTree::~RCStringTree() {
    freeMemory();
}


void RCStringTree::freeMemory() {
    if (searchMap.empty()) return;

    // free all the nodes
    std::map<std::string,RCStringTreeNode*>::iterator it;
    for (it=searchMap.begin();it!=searchMap.end();it++) {
	if ( it->second->children ) {
	    delete it->second->children;
	    it->second->children=NULL;
	}
	delete it->second;
	it->second=NULL;
    }
    searchMap.clear();
}

bool RCStringTree::refreshTree() {

  freeMemory();

  try {

    Configuration config(data);

    // get the partition
    const daq::core::Partition * root = daq::core::get_partition(config, partition);
    if (!root)
    {
	std::cerr << "ERROR in RCStringTree::refreshTree(): Partition not found\n";
	return false;
    }

    if (!root->get_OnlineInfrastructure())
    {
	std::cerr << "ERROR in RCStringTree::refreshTree(): OnlineInfrastructure not found\n";
	return false;
    }

    const daq::core::Segment* seg = root->get_segment(root->get_OnlineInfrastructure()->UID());

    // save the root controller
    rootNode = new RCStringTreeNode;
    rootNode->parent=NULL;
    rootNode->name=seg->get_controller()->UID();
    rootNode->level=0;
    rootNode->segment=true;
    searchMap[rootNode->name]=rootNode;

    if (verbose) std::cout << "Root: " << rootNode->name << std::endl;

    if (MAXLEVEL!=-1 && MAXLEVEL<1) {
	rootNode->children=NULL;
	return true;
    }
    level=1;    
    addSubChildren(rootNode, *seg, config);

    return true;
  }
  catch ( daq::config::Exception & ex ) {
    std::cerr << "Caught configuration database exception:" << ex << std::endl;
    return false;
  }

}


void RCStringTree::addSubChildren(RCStringTreeNode* parent, const daq::core::Segment& segment, Configuration& config) {
  if(segment.is_disabled() == false) {
    RCStringTreeNode* child;
    
    // search for sub segments
    parent->children= new std::vector<RCStringTreeNode*>;
    for (const auto a : segment.get_nested_segments() ) {
      if(a->is_disabled() == false) {
	child= new RCStringTreeNode;
	parent->children->push_back(child);
	child->parent=parent;
	child->name=a->get_controller()->UID();
	child->level=level;
	child->children=NULL;
	child->segment=true;
	
	searchMap[child->name]=child;
	//std::cout << "Added child " << child->name << " at level " << level << std::endl; 
	if (MAXLEVEL!=-1 && MAXLEVEL<=level) continue;
	level++;
	addSubChildren(child, *a, config);
	level--;
      }
    }
    
    // this is for the other state aware applications, not for segment controllers
    if (!onlySegments) {
      // walk through the applications of this segment
      for (const auto a : segment.get_applications()) {
	// apps are only added if they are children of a RunControl class
	if ( !(config.cast<daq::core::RunControlApplicationBase>(a))) continue;
	child = new RCStringTreeNode;
	child->parent=parent;
	child->name=a->UID();
	child->level=level;
	child->segment=false;
	child->children=NULL; // apps cannot have children
	
	parent->children->push_back(child);
	searchMap[child->name]=child;
      }
    }
    
    // clean up if segment has no children
    if (parent->children->empty()) {
      // if (verbose) std::cout << std::string(level,'\t') << "Segment with no children" << std::endl;
      delete parent->children;
      parent->children=NULL;
    }
  }
}

int RCStringTree::countLevel(const char* fullname) const {
    if (!fullname) return 0;
    int count=0,i=0;
    while (fullname[i++]!=0)
	if (fullname[i]==nameSeperator) count++;
    return count;
}

bool RCStringTree::writeName(const std::string& fullname, std::string& name) const {
    name= fullname.substr(fullname.find_last_of(nameSeperator)+1);
    if (name.size()>4 && name[name.size()-4]==':' &&
	 name[name.size()-3]=='A'&& name[name.size()-2]=='P'&& name[name.size()-1]=='P') {
	name.erase(name.size()-4);
	return false;
    }
    return true;
}

bool RCStringTree::refreshTree(const char* (*getNext)() ) {
    freeMemory();
    const char* nextL=(*getNext)();
    if (!nextL) return false;
    rootNode = new RCStringTreeNode;
    rootNode->parent=NULL;
    rootNode->name=std::string(nextL);
    rootNode->level=0;
    rootNode->segment=true;
    rootNode->children=NULL;
    searchMap[rootNode->name]=rootNode;
    if (verbose) std::cout << "Root: " << rootNode->name << std::endl;

    level=1;
    nextL=(*getNext)();
    if ( nextL ) {
	if ( strncmp(nextL,std::string(rootNode->name+nameSeperator).c_str(),rootNode->name.length()+1)==0 )
	    if (addSubChildren(rootNode,nextL,getNext))
		if ( !nextL ) return true;
	return false;
    } else
	return true;
}
    
bool RCStringTree::addSubChildren(RCStringTreeNode* parent, const char* & cur, const char* (*getNext)()  ) {
    parent->children= new std::vector<RCStringTreeNode*>;
    RCStringTreeNode* child;
    std::string parentFullName;
    RCStringTreeNode* pN=parent;
    while (pN) {
	parentFullName.insert(0,pN->name+nameSeperator);
	pN=pN->parent;
    }
    std::string lastSaved;
    do {
	if ( countLevel(cur) > level) {
	    if ( lastSaved=="" || strncmp(cur,lastSaved.c_str(),lastSaved.length())!=0 ) {
		std::string nextSegment(cur+parentFullName.length());
		nextSegment.erase(nextSegment.find(nameSeperator));
		if (nextSegment!=parent->name) {

		    child= new RCStringTreeNode;
		    parent->children->push_back(child);
		    child->parent=parent;
		    child->name=nextSegment;
		    child->segment=true;
		    child->level=level;
		    child->children=NULL;

		    if ( searchMap.find(child->name)!=searchMap.end()) {
			std::cerr << "WARNING: The name '" << child->name << "' occurs more then once in the tree structure. File may be corrupted!" << std::endl;
			child->name=child->name+"|DOUBLE";
		    }
		    searchMap[child->name]=child;

		    if (verbose)
			std::cout<< std::string(level,'\t') << child->name << std::endl;
		} else {
		    parent->name=nextSegment+nameSeperator+nextSegment;
		    delete parent->children;
		    child=parent;
		    searchMap[child->name]=child;
		}
	    }
	    if (MAXLEVEL==-1 || MAXLEVEL>level) {
		level++;
		if (!addSubChildren(child,cur,getNext)) return false;
		level--;
	    }

	    if (!cur) return true;
	    if ( strncmp(cur,parentFullName.c_str(),parentFullName.length())==0 )
		continue;
	    else
		return true;

	} else {
	    child= new RCStringTreeNode;
	    parent->children->push_back(child);
	    child->parent=parent;
	    child->segment=writeName(std::string(cur),child->name);
	    child->level=level;
	    child->children=NULL;

	    if ( searchMap.find(child->name)!=searchMap.end()) {
		std::cerr << "WARNING: The name '" << child->name << "' occurs more then once in the tree structure. File may be corrupted!" << std::endl;
		child->name=child->name+"|DOUBLE";
	    }
	    searchMap[child->name]=child;
	    lastSaved=std::string(cur);

	    if (verbose) {
		std::cout<< std::string(level,'\t') << child->name;
		if (!(child->segment)) std::cout<< ":APP";
		std::cout << std::endl;
	    }
	}

	cur=(*getNext)();
	if ( !cur ) return true; 

    } while ( strncmp(cur,parentFullName.c_str(),parentFullName.length())==0 );
    return true;
}

bool RCStringTree::exists(const std::string& name) const {
    return (searchMap.find(name)!=searchMap.end());
}
	
int RCStringTree::getLevel(const std::string& name) const {
    std::map<std::string,RCStringTreeNode*>::const_iterator it=searchMap.find(name);
    if (it==searchMap.end()) return -1;
    return it->second->level;
}


std::string RCStringTree::getParent(const std::string& name) const {
    std::map<std::string,RCStringTreeNode*>::const_iterator it=searchMap.find(name);
    if (it==searchMap.end()) return "N/A";

    if (it->second->parent)
	return it->second->parent->name;
    else
	return "IS_ROOT";
}


std::string RCStringTree::getFullName(const std::string& name) const {
    std::map<std::string,RCStringTreeNode*>::const_iterator it=searchMap.find(name);
    if (it==searchMap.end()) return "N/A";

    RCStringTreeNode* parent;
    std::string fullName=name;
    parent=it->second->parent;
    while (parent) {
	fullName.insert(0,parent->name+nameSeperator);
	parent=parent->parent;
    }
    if (!(it->second->segment)) fullName.append(":APP");
    return fullName;
}

void RCStringTree::getChildren(const std::string& name, std::vector<std::string>& childrenNames) const {
    std::map<std::string,RCStringTreeNode*>::const_iterator findIt=searchMap.find(name);
    if (findIt==searchMap.end()) return;

    std::vector<RCStringTreeNode*>* children=findIt->second->children;
    if (!children) return;
    std::vector<RCStringTreeNode*>::const_iterator it;
    for (it=children->begin();it!=children->end();it++) {
	childrenNames.push_back((*it)->name);
    }
}

void RCStringTree::getAllChildrenBranchOrdered(const std::string& name, std::vector<std::string>& childrenNames, bool getMe) const {
    std::map<std::string,RCStringTreeNode*>::const_iterator findIt=searchMap.find(name);
    if (findIt==searchMap.end()) return;

    getAllChildrenBranchOrdered(findIt->second, childrenNames, getMe);
}

void RCStringTree::getAllChildrenLevelOrdered(const std::string& name, std::vector<std::string>& childrenNames, bool getMe) const {
    std::map<std::string,RCStringTreeNode*>::const_iterator findIt=searchMap.find(name);
    if (findIt==searchMap.end()) return;

    getAllChildrenLevelOrdered(findIt->second, childrenNames, getMe);
}

void RCStringTree::getAllBranchOrdered(std::vector<std::string>& names, int max_level) const {
    return getAllChildrenBranchOrdered(rootNode, names, true, max_level);
}

void RCStringTree::getAllLevelOrdered(std::vector<std::string>& names, int max_level) const {
    return getAllChildrenLevelOrdered(rootNode, names, true, max_level);
}


void RCStringTree::getAllChildrenBranchOrdered(const RCStringTreeNode* node, std::vector<std::string>& names,  bool getMe, int max_level, int min_level) const {
    if (!node) return;

    if (getMe && node->level>=min_level && (max_level==-1 || node->level<=max_level) )
       	names.push_back(node->name);

    if (node->children && (max_level==-1 || node->level<max_level) ) appendSubChildren(names,node,max_level,min_level);
}

void RCStringTree::appendSubChildren(std::vector<std::string>& names, const RCStringTreeNode* node, int max_level, int min_level) const {
    std::vector<RCStringTreeNode*>::const_iterator it;
    for (it=node->children->begin();it!=node->children->end();it++) {
	if ( (*it)->level >=min_level)
	    names.push_back((*it)->name);
	if ( (*it)->children  && (max_level==-1 || (*it)->level<max_level) )
	    appendSubChildren(names,*it,max_level,min_level);
    }
}


void RCStringTree::getAllChildrenLevelOrdered(const RCStringTreeNode* node, std::vector<std::string>& names,  bool getMe, int max_level) const {
    if (!node) return;

    if (getMe && (max_level==-1 || node->level<=max_level) ) names.push_back(node->name);

    if (!node->children || (max_level!=-1 && node->level>=max_level) ) return;
    std::vector<RCStringTreeNode*>* nodelist=new std::vector<RCStringTreeNode*>( (*node->children) );
    std::vector<RCStringTreeNode*>* newNodelist;

    do {
	newNodelist=new std::vector<RCStringTreeNode*>;
	for (unsigned i=0;i<nodelist->size();i++) {
	    names.push_back((*nodelist)[i]->name);
	    if ( ( (*nodelist)[i]->children ) && (max_level==-1 || (*nodelist)[i]->level < max_level) ) {
		for (unsigned j=0;j<(*nodelist)[i]->children->size();j++) {
		    newNodelist->push_back( (*(*nodelist)[i]->children)[j] );
		}
	    }
	}
	delete nodelist;
	nodelist=newNodelist;
    } while ( nodelist->size() > 0 );

    delete newNodelist;
}

void RCStringTree::getAllInThisLevel(const std::string& name, std::vector<std::string>& names) const {
    std::map<std::string,RCStringTreeNode*>::const_iterator it=searchMap.find(name);
    if (it==searchMap.end()) return;

    getAllChildrenBranchOrdered(rootNode,names,true,it->second->level,it->second->level);
}

void RCStringTree::getAllInThisLevel(int lev, std::vector<std::string>& names) const {
    if (level<0) return;
    getAllChildrenBranchOrdered(rootNode,names,true,lev,lev);
}

bool RCStringTree::isSegment(const std::string& name) const {
    std::map<std::string,RCStringTreeNode*>::const_iterator it=searchMap.find(name);
    if (it==searchMap.end()) return false;
    return it->second->segment;
}

bool RCStringTree::isParent(const RCStringTreeNode* ctrl,const RCStringTreeNode* parent) const {
    if (!ctrl->parent) return false;
    if (ctrl->parent==parent) return true;
    return isParent(ctrl->parent,parent);
}

bool RCStringTree::isParent(const std::string& name, const std::string& parent) const {
    std::map<std::string,RCStringTreeNode*>::const_iterator it=searchMap.find(name);
    if (it==searchMap.end()) return false;
    RCStringTreeNode* node=it->second;
    it=searchMap.find(parent);
    if (it==searchMap.end()) return false;
    return isParent(node,it->second);
}

