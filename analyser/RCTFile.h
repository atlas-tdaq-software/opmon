/** -------------------------------------------------------------
 *  RCTFile.h - class RCTFile, RCTFileException
 *
 *  interface to the ROOT files produced by opmon_monitor
 *
 *  See the constructor of RCTFile for the use of RCStringTree
 *  to retrieve the run control tree from the ROOT file,
 *  using that the controllers are saved 'branch ordered'
 *  with their 'full names'. 
 *  See getTransLogByFull() for how to retrieve the
 *  RCTransLog objects from the file.
 *
 *  RCTFileException is the exception thrown if there are
 *  errors loading the file.
 *
 *  defines the name of the root controller as "RootController"
 *
 *  author: Herbert Kaiser, Sep 2006
 *  -------------------------------------------------------------
 */

#ifndef RCT_FILE_H
#define RCT_FILE_H

#include "TNamed.h"
#include "RCTFileList.h"

#define ROOT_CTRL "RootController"

using std::string;

class RCTFileException {
    public:
	const string msg;
	RCTFileException(const string& message) : msg(message) {}
};

class TBrowser;
class TFile;
class RCStringTree;
class RCTransLog;
class RCTimeSequence;

class RCTFile: public TNamed {
    friend void RCTFileList::mergeFiles(const char* fileName, bool force);
    friend TFile* RCTFileList::getFileForUpdate(int curFile);
    private:
	TFile* mFile;		   //! the file with the data
	RCStringTree* rcst;	   //! the tree of this file's partition
	RCTimeSequence* rootTimes; //! timing of the RootController
	string partition;          //! partition this file belongs to
	string updatetime;         //! date of last update 
    public:
	RCTFile (const char * fName);
	RCTFile (const char * fName,TFile* rootFile);
	~RCTFile();
	void reloadAll();
	const RCStringTree* getTree() const { return rcst; }
	Bool_t IsFolder() const { return kFALSE; }
	virtual void Browse(TBrowser *b) {}
	const char* GetPartition() const { return partition.c_str(); }
	const char* GetTime() const { return updatetime.c_str(); }
	const RCTimeSequence* getRootTimes() { return rootTimes; };
	const RCTransLog* getTransLogByFull(const string& fullName);

	ClassDef(RCTFile, 1)	// class that holds a ROOT file
};

#endif // RCT_FILE_H

