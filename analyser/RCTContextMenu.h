#ifndef RCT_CONTEXT_MENU_H
#define RCT_CONTEXT_MENU_H

#include <TGMenu.h>
#include <TVirtualX.h>

class RCTContextMenu : public TGPopupMenu
{
    TGTransientFrame * synch;
    Int_t selected_id;

public:
    RCTContextMenu( const TGWindow * parent) :
	TGPopupMenu(parent),synch(NULL),selected_id(-1) {}

    Int_t Show( int x, int y ) {
	PlaceMenu( x, y, true, true );
	synch = new TGTransientFrame( fClient->GetRoot(), fClient->GetRoot(), 0, 0 );
	fClient->WaitFor( synch );
	return selected_id;
    }

    Int_t Show( const TGWindow * w, int orgX, int orgY ) {
	Window_t wdum;
	int x, y;
	gVirtualX->TranslateCoordinates( w->GetId(), GetParent()->GetId(), orgX, orgY, x, y, wdum );
	return Show( x, y );
    }

    void Activated(Int_t id) {
	selected_id = id;
	if ( synch )
	{
	    delete synch;
	    synch = 0;
	}
    }

    void PoppedDown() {
	selected_id = -1;
	if ( synch )
	{
	    delete synch;
	    synch = 0;
	}
    }

};

#endif // RCT_CONTEXT_MENU_H

