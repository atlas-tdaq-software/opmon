/** ------------------------------------------------------------
 *  analyser.cxx - main file of opmon_analyser application
 *
 *  parses the command line and starts the ROOT GUI application
 *
 *  author: Herbert Kaiser, Sep 2006
 *  ------------------------------------------------------------
 */

#include <string>
#include <vector>
#include "TApplication.h"
#include "TBrowser.h"
#include "RCTDisplayGuiFactory.h"
#include "RCTStartFolder.h"
#include "RCTFile.h"
#include "RCTSelectionList.h"
#include "RCTController.h"

#include <iostream>

using std::string;
using std::vector;
using std::cout;
using std::cerr;
using std::endl;


/**********************************************************************************************/

int main( int argc, char ** argv )
{
    TApplication * theApp = new TApplication( "opmon_analyser", 0, 0 );

    if (argc<2) {
	cout << "You need to specify at least one filename of a ROOT file produced"
		" by opmon_monitor.\n"
		"Usage: opmon_analyser [--old-tdaq] [-s selection.sel] [-r] file1.root [file2.root] [file3.root] ..."
	     << endl;
	return 1;
    }

    vector<string> list;
    bool refresh=false;
    const char* selFile=NULL;
    for (int i=1;i<argc;i++) {
	if (strcmp("-h",argv[i])==0 || strcmp("--help",argv[i])==0) {
	    cout << "Usage: opmon_analyser [--old-tdaq] [-s selection.sel] [-r] file1.root [file2.root] [file3.root] ...\n\n"
	         "The given files must be ROOT files produced by opmon_monitor\n"
		 "Set the flag '--old-tdaq' to analyse files from tdaq-01-04-00 partitions\n\n"
		 "-s selection.sel	load file selection.sel with selections\n"
		 "-r			refresh the selections with the new data at the beginning"
	         << endl;
	    return 0;
	}
	if (strcmp("--old-tdaq",argv[i])==0) {
	    RCTController::oldtdaq=true;
	    continue;
	}
	if (strcmp("-s",argv[i])==0) {
	    if (++i>=argc || selFile) {
		cout << "Bad parameters! Try '-h' for help!" << endl;
		return 1;
	    } else
		selFile=argv[i];
	    continue;
	}
	if (strcmp("-r",argv[i])==0) {
	    refresh=true;
	    continue;
	}
	list.push_back(argv[i]);
    }

    RCTStartFolder * startfolder;
    try {
	startfolder = new RCTStartFolder(list);
    } catch(RCTFileException e) {
	return 0;
    }

    RCTDisplayGuiFactory * factory = new RCTDisplayGuiFactory(startfolder->getSelectionList());
    TBrowser * browser  = new TBrowser( "opmon_analyser", new TObject(), "Opmon Analyser" );
    startfolder->Browse( browser );
    if (selFile)
	startfolder->getSelectionList()->loadSelectionsFromFile(selFile,refresh);

    theApp->Run();

    delete startfolder;
    delete browser;
    delete factory;
    return 0;
}

