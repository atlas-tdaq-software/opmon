/** -------------------------------------------------------------------------------------
 *  RCTDisplayGuiFactory.h : helper class for RCTBrowser
 * 
 *  replaces ROOT's original TRootGuiFactory object pointed by
 *  gGuiFactory. New object of OHDisplayGuiFactory class will use instances
 *  of RCTBrowser instead of TRootBrowser, every time a new TBrowser
 *  object is created.
 *  RCTBrowser is a modified version of TRootBrowser
 * 
 *  No additional code or instantiation is needed: static object 
 *  RCTDisplayGuiFactory::gMyGuiFactory is instantaited when the library is being loaded,
 *  and gGuiFactory global pointer is updated.
 *
 *  author: Herbert Kaiser, Sep 2006 based on OHDisplayGuiFactory from OH
 *  -------------------------------------------------------------------------------------
 */

#ifndef __RCTDisplayGuiFactory__
#define __RCTDisplayGuiFactory__

#include "TRootGuiFactory.h"
class RCTSelectionList;

class RCTDisplayGuiFactory: public TRootGuiFactory
{
    TGuiFactory * old_factory;
    RCTSelectionList* selList;
    
    TBrowserImp* CreateBrowserImp(TBrowser *b, const char *title, UInt_t width, UInt_t height, const Option_t * =0);
    TBrowserImp* CreateBrowserImp(TBrowser *b, const char *title, Int_t x, Int_t y,UInt_t width, UInt_t height, const Option_t* =0);
     
  public:
    RCTDisplayGuiFactory(RCTSelectionList* sel);
    ~RCTDisplayGuiFactory();

    ClassDef(RCTDisplayGuiFactory,1) // My Gui Factory    
};

#endif // __RCTDisplayGuiFactory__
