/** ---------------------------------------------------------------------
 *  RCTBrowser.h - class RCTBrowser
 *
 *  provides the GUI browser for the analyser application
 *  processes mesagges from the browser window
 *  manages the tree and list view by calling the Browse() methods
 *  of the items
 *  provides the context menus, toolbar, etc.
 *
 *  for the creation of the several elements see CreateBrowser()
 *  for message processing (menus, mouse clicks.. ) see ProcessMessage()
 *  for adding the object items see AddToTree(),AddToBox()
 *
 *  author: Herbert Kaiser, Sep 2006 based on OHBrowser
 *          from OH package, but with many changes
 *  --------------------------------------------------------------------
 */

#ifndef RCT_BROWSER_H
#define RCT_BROWSER_H

#ifndef ROOT_TBrowserImp
#include "TBrowserImp.h"
#endif

#ifndef ROOT_TGFrame
#include "TGFrame.h"
#endif

class RCTIconBox;
class TGMenuBar;
class TGPopupMenu;
class TGLayoutHints;
class TGStatusBar;
class TGHorizontal3DLine;
class TGToolBar;
class TGLabel;
class TGListView;
class TGCanvas;
class TGListTree;
class TGListTreeItem;
class TList;
class RCTSelectionList;
class RCTContextMenu;


class RCTBrowser : public TGMainFrame, public TBrowserImp {
private:
    TGHorizontal3DLine  *fToolBarSep;
    TGToolBar           *fToolBar;
    TGStatusBar         *fStatusBar;
    TGVerticalFrame     *fV1;
    TGVerticalFrame     *fV2;
    TGLabel             *fLbl1;
    TGLabel             *fLbl2;
    TGHorizontalFrame   *fHf;
    TGCompositeFrame    *fTreeHdr;
    TGCompositeFrame    *fListHdr;
    TGListView          *fListView;
    RCTIconBox		*fIconBox;

    TGCanvas            *fTreeView;
    TGListTree          *fLt;

    TGLayoutHints       *fMenuBarLayout;
    TGLayoutHints       *fMenuBarItemLayout;
    TGLayoutHints       *fMenuBarHelpLayout;
    TGLayoutHints       *fComboLayout;
    TGLayoutHints       *fBarLayout;

    TGMenuBar           *fMenuBar;
    TGPopupMenu         *fFileMenu;
    TGPopupMenu         *fViewMenu;
    RCTContextMenu	*fScatterPlotMenu;
    RCTContextMenu	*fScatterPlotRealMenu;
    RCTContextMenu	*fControllerMenu;
    RCTContextMenu	*fSelectionMenu;
    RCTContextMenu	*fItemMenu;
    RCTContextMenu	*fSelectionListMenu;
    RCTContextMenu	*fFileListMenu;

    TGLabel		*fCurrentSelection;
    TGLabel		*fCurrentTransition;

    TList               *fWidgets;

    char                 fCurrentDir[1024];
    Cursor_t             fWaitCursor;        // busy cursor
    TGListTreeItem      *fListLevel;         // current TGListTree level
    Bool_t               fTreeLock;          // true when we want to lock TGListTree
    bool                 doMyRefresh;
    Int_t                fViewMode;          // current IconBox view mode

    RCTSelectionList	*selectionList;

    void  CreateBrowser(const char *name);
    void  ListTreeHighlight(TGListTreeItem *item);
    void  IconBoxAction(TObject *obj);
    void  Chdir(TGListTreeItem *item);
    void  DisplayDirectory();
    void  DisplayTotal(Int_t total, Int_t selected);
    void  SetViewMode(Int_t new_mode, Bool_t force = kFALSE);
    bool  RecursiveDeleteItem( TGListTree * tree, TGListTreeItem *item, TObject * userData );

public:
    RCTBrowser(TBrowser *b, const char *title, UInt_t width, UInt_t height, RCTSelectionList* sel);
    RCTBrowser(TBrowser *b, const char *title, Int_t x, Int_t y, UInt_t width, UInt_t height, RCTSelectionList* sel);
    virtual ~RCTBrowser();

    void     Add(TObject *obj, const char *name , int );
    void     Add(TObject *obj, const char *name = 0 )
    { Add ( obj, name, 0 ); }
    void     AddToBox(TObject *obj, const char *name);
    void     AddToTree(TObject *obj, const char *name);
    void     BrowseObj(TObject *obj);
    void     ExecuteDefaultAction(TObject *obj) {}
    void     Iconify() { }
    void     RecursiveRemove(TObject *obj);
    void     MyRefresh(Bool_t force = false);
    void     Refresh(Bool_t force = kFALSE);
    void     ResizeBrowser() { }
    void     ShowToolBar(Bool_t show = kTRUE);
    void     ShowStatusBar(Bool_t show = kTRUE);
    void     Show() { MapRaised(); }
    void     SetDefaults(const char *iconStyle = 0, const char *sortBy = 0);
    void     RedrawTreeList();


    // overridden from TGMainFrame
    void     CloseWindow();
    Bool_t   ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);


    ClassDef(RCTBrowser,0) // the run control transition times browser
};

#endif // RCT_BROWSER_H
