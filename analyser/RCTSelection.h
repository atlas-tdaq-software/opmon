/** -------------------------------------------------------------
 *  RCTSelection.h - definition of classes:
 *
 *  - RCTSelection
 *  contains a hash list of items in the selection
 *  See saveToFile() and the constructor for how a selection
 *  is saved in a ROOT file.
 *
 *  - RCTSelectionItem, RCTSelectionItemCombination
 *  contain the data of the selected controller-transition item
 *  RCTSelectionItemCombination is the specialization for
 *  combinations of internal transitions.
 *
 *  - RCTFilesInformation
 *  contains the list of files used to collect the data in this
 *  selection (is also saved in the selection ROOT file)
 *
 *  - RCTSelectionItemCreationFailed
 *  exception thrown by the constructor of the items
 *
 *  author: Herbert Kaiser, Sep 2006
 *  -------------------------------------------------------------
 */

#ifndef RCT_SELECTION_H
#define RCT_SELECTION_H

#include <vector>
#include <string>
#include "TNamed.h"
#include "THashList.h"
#include "RCTSelectionList.h"

using std::vector;
using std::string;

class TBrowser;
class RCTSelectionItem;
class RCTFileList;
class RCTSelectionGraphList;
class RCTFilesInformation;

class RCTSelectionItemCreationFailed {
    public:
	RCTSelectionItemCreationFailed() {}
};

class RCTSelection: public TNamed {
    friend void RCTSelectionList::activateSelection(RCTSelection* sel);
    friend bool RCTSelectionList::addSelection(const string& name);
    friend void RCTSelectionList::removeSelection(RCTSelection* sel);
protected:
    RCTSelectionList* parent;
    RCTFileList* list;
    bool active;
    bool notUpdated;
    THashList items;
    RCTFilesInformation* filesInfo;
    RCTSelectionGraphList* graphlist;
    void setActive(bool status) { active=status; }

public:
    RCTSelection(const char* name,RCTSelectionList* selList,RCTFileList* fileList);
    RCTSelection(RCTSelectionList* selList,RCTFileList* fileList, TDirectory* dir);
    ~RCTSelection();
    virtual Bool_t IsFolder() const { return kTRUE; }
    virtual void Browse(TBrowser *b);
    bool addItem(const string& controller,const string& transition);
    bool addItem(const string& controller,const char* const* comb);
    void removeItem(RCTSelectionItem* item);
    void activate();
    bool isActive() { return active; }
    bool isNotUpdated() { return notUpdated; }
    void remove();
    void saveToFile(const char* fileName);
    void refresh();
    void saveToCSV(const char* fileName);

    ClassDef(RCTSelection,1) // folder for one selection
};


class RCTSelectionItem: public TNamed {
protected:
    RCTSelection* selection; //! will be set after opening
    string controller;
    string transition;
    RCTFileList* list; //! will be set after opening
    vector<double> values;
    vector<int> distribution;
    bool tainted;
    double avg;
public:
    /** should not be used, only for loading from ROOT file */
    RCTSelectionItem() : selection(NULL),controller(),transition(),list(NULL),values(),distribution() {}
    RCTSelectionItem(RCTSelection* parent,const char* name,const string& ctrl,const string& trans,RCTFileList* _list,bool verbose=true,bool refr=true) :
	TNamed(name,"Selection Item"),selection(parent),controller(ctrl),transition(trans),list(_list)
    { if (refr) refresh(true,verbose); }

    virtual Bool_t IsFolder() const { return kFALSE; }
    virtual void Browse(TBrowser *b) {}
    virtual void refresh(bool fromConstructor=false,bool verbose=true);
    /** need to call this immediately after loading the class from a file, otherwise refreshing and removing will fail */
    void setFilesAndSelection(RCTFileList* _list,RCTSelection* parent) { list=_list; selection=parent; }
    void remove();
    const string& getController() { return controller; }
    const string& getTransition() { return transition; }
    bool isTainted() {return tainted; }
    const vector<double>* getValues() {return &values; }
    unsigned getEntries() {return values.size(); }
    double getAverage() {return avg; }
    const vector<int>* getDistribution() {return &distribution;}

    ClassDef(RCTSelectionItem,1) // selection item
};


class RCTSelectionItemCombination: public RCTSelectionItem {
protected:
    vector<string> combination;
public:
    RCTSelectionItemCombination() : RCTSelectionItem(),combination() {}
    RCTSelectionItemCombination(RCTSelection* parent,const char* name,const string& ctrl,const char* const* comb,RCTFileList* _list,bool verbose=true);

    virtual void refresh(bool fromConstructor=false,bool verbose=true);

    ClassDef(RCTSelectionItemCombination,1) // selection item
};


class RCTFilesInformation: public TNamed {
protected:
    vector<string> fileNames;
public:
    /** should not be used, only for loading from ROOT file */
    RCTFilesInformation() : fileNames() {}
    RCTFilesInformation(RCTFileList* list);
    const vector<string>* getFileNames() { return &fileNames; }
    void refresh(RCTFileList* list);

    ClassDef(RCTFilesInformation,1) // information about the files
};

#endif //  RCT_SELECTION_H

