/** -------------------------------------------------------------
 *  RCTStartFolder.h - definition of class RCTStartFolder
 *
 *  contains the three basic folders of the browser
 *  RootController, Selections, Files
 *
 *  author: Herbert Kaiser, Sep 2006
 *  -------------------------------------------------------------
 */

#ifndef _STARTFOLDER_H_
#define _STARTFOLDER_H_

#include <vector>
#include <string>
#include "TNamed.h"

using std::vector;
using std::string;

class TBrowser;
class RCTController;
class RCTFileList;
class RCTSelectionList;

class RCTStartFolder: public TNamed {

    RCTController* rootCtrl;
    RCTFileList* list;
    RCTSelectionList* selection;

    public:
    RCTStartFolder(const vector<string>& fileList);
    ~RCTStartFolder();
    virtual Bool_t IsFolder() const { return kTRUE; }
    virtual void Browse(TBrowser *b);
    RCTSelectionList* getSelectionList() { return selection; }

    ClassDef(RCTStartFolder,1) // Start Folder for the Browser
};

#endif // _STARTFOLDER_H_
