/** -------------------------------------------------------
 *  RCTGraphs.h - defines classes:
 *
 *  - RCTScatterGraph
 *  provides the graph showing transition time values
 *  for each file on a seperate graph line
 *
 *  - RCTCombinedScatterGraph
 *  the same but for combinations of internal transitions 
 *
 *  - RCTAveragesDiagram
 *  bar diagram showing the average time of all internal
 *  transitions at once having one bar for each file
 *
 *  - RCTCombinedAveragesDiagram
 *  the same for combinations of internal transitions
 *
 *  See Browse() for creation and drawing of the diagrams.
 *  Diagrams are only created when Browse() is called
 *  for the first time. After that they are in memory.
 *  NOTE: Actually there is one ROOT histogram or graph
 *  for each file in the list.
 *  If the user deletes a graph or histo in the canvas
 *  this leads to a segmentation fault when trying to
 *  draw that diagram again from memory.
 *
 *  author: Herbert Kaiser, Sep 2006
 *  -------------------------------------------------------
 */

#ifndef RCT_GRAPHS_H
#define RCT_GRAPHS_H

#include "TGraph.h"
#include "TH1.h"
#include "TLegend.h"

class TBrowser;
class RCTFileList;

class RCTScatterGraph: public TNamed {
protected:
    string title;
    string controller;
    string transition;
    RCTFileList* list;
    vector<TGraph*> graphs;
    TLegend* legend;
    TGraph* biggestGraph; //!

    double maxValue;
    double minValue;

private:
    unsigned entries;
    double average;

public:
    RCTScatterGraph(const char* name,const string& _title, const string& ctrl, const string& trans, RCTFileList* _list, unsigned _entries, double _average) :
	TNamed(name,"Scatter Plot"),title(_title),controller(ctrl), transition(trans),
       	list(_list),graphs(0),legend(NULL),biggestGraph(NULL),maxValue(0.0),
	entries(_entries),average(_average) {}
    virtual ~RCTScatterGraph();

    Bool_t IsFolder() const { return kFALSE; }
    virtual void Browse(TBrowser *b);
    static double* countUp(unsigned max);
    const string& getController() { return controller; };
    const string& getTransition() { return transition; };
    unsigned getEntries() { return entries; };
    double getAverage() { return average; };

    ClassDef(RCTScatterGraph,1) // class for "scatter plots"
};


class RCTCombinedScatterGraph: public RCTScatterGraph {
protected:
    const char* const* combination; //!
public:
    RCTCombinedScatterGraph(const char* name,const string& _title, const string& ctrl, const char* const* combinationArray, RCTFileList* _list,unsigned _entries,double _average) :
    RCTScatterGraph(name,_title,ctrl,combinationArray[0],_list,_entries,_average),
	combination(combinationArray) {}

    virtual void Browse(TBrowser *b);
    const string& getController() { return controller; };
    const char* const* getCombination() { return combination; };

    ClassDef(RCTCombinedScatterGraph,1) // class for combined "scatter plots"
}; 


class RCTAveragesDiagram: public TNamed {
protected:
    string title;
    string controller;
    const char* const* transitions; //!
    RCTFileList* list;
    vector<TH1D*> hists;
    TLegend* legend;
    double maxValue;
    double minValue;

public:
    RCTAveragesDiagram(const char* name, const string& _title, const string& ctrl, const char* const* transOrder, RCTFileList* _list) :
	TNamed(name,"Averages"),title(_title),controller(ctrl),transitions(transOrder),list(_list),hists(0),legend(NULL) {}
    virtual ~RCTAveragesDiagram();

    Bool_t IsFolder() const { return kFALSE; }
    virtual void Browse(TBrowser *b);

    ClassDef(RCTAveragesDiagram,1) // class for "averages"
};


class RCTCombinedAveragesDiagram: public RCTAveragesDiagram {
protected:
    vector<const char* const*> combs; //!
public:
    RCTCombinedAveragesDiagram(const char* name, const string& _title, const string& ctrl,const vector<const char* const*> combinationsVector, RCTFileList* _list) :
	RCTAveragesDiagram(name,_title,ctrl,NULL,_list), combs(combinationsVector) {}

    virtual void Browse(TBrowser *b);

    ClassDef(RCTCombinedAveragesDiagram,1) // class for "combined averages"
};


#endif // RCT_GRAPHS_H
