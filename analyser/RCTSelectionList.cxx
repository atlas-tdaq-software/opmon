/** -----------------------------------------------------------------
 *  RCTSelectionList.cxx - implementation of class RCTSelectionList
 *  author: Herbert Kaiser, Sep 2006
 *  -----------------------------------------------------------------
 */

#include "TCollection.h"
#include "TFile.h"
#include "TKey.h"
#include "TBrowser.h"
#include "TGMsgBox.h"
#include "RCTSelection.h"
#include "RCTFileList.h"
#include "RCTSelectionList.h"

ClassImp(RCTSelectionList)

RCTSelectionList::~RCTSelectionList() {
    for (unsigned i=0;i<selections.size();i++)
	delete selections[i];
}


void RCTSelectionList::Browse(TBrowser *b) {
    for (unsigned i=0;i<selections.size();i++) {
	b->RecursiveRemove(selections[i]);
	b->Add(selections[i],selections[i]->GetName() );
    }
}

bool RCTSelectionList::addSelection(const string& name) {
    for (unsigned i=0;i<selections.size();i++)
	if (name==selections[i]->GetName()) return false;
    activeSelection= new RCTSelection(name.c_str(),this,list);
    activeSelection->setActive(true);
    selections.push_back(activeSelection);
    return true;
}

void RCTSelectionList::activateSelection(RCTSelection* sel) {
    activeSelection=sel;
    sel->setActive(true);
}

void RCTSelectionList::removeSelection(RCTSelection* sel) {
    if (activeSelection==sel) {
	activeSelection->setActive(false);
	activeSelection=NULL;
    }
    vector<RCTSelection*>::iterator it=selections.begin();
    while (*it!=sel) it++;
    if (it!=selections.end()) {
	delete sel;
	selections.erase(it);
    }
}

void RCTSelectionList::loadSelectionsFromFile(const char* fileName,bool refresh) {
    TFile f(fileName,"READ");
    if (f.IsZombie()) {
	new TGMsgBox(gClient->GetRoot(),gClient->GetWindowByName("opmon_analyser"),"Invalid File",(string("File ")+fileName+" is not a valid ROOT selection file.").c_str(),kMBIconStop);
	return;
    }
    TKey *key;
    TIter next(f.GetListOfKeys());
    while ((key = (TKey *) next())) {
	TDirectory* dir= static_cast<TDirectory*>(key->ReadObj());
	if (!dir) {
	    new TGMsgBox(gClient->GetRoot(),gClient->GetWindowByName("opmon_analyser"),"Invalid File",(string("File ")+fileName+" is not a valid ROOT file with a selection.").c_str(),kMBIconStop);
	    return;
	} else {
	    try {
		RCTSelection* sel=new RCTSelection(this,list,dir);
		selections.push_back(sel);
		if (refresh) sel->refresh();
	    } catch (RCTSelectionItemCreationFailed) {
		new TGMsgBox(gClient->GetRoot(),gClient->GetWindowByName("opmon_analyser"),"Invalid File",(string("File ")+fileName+" is not a valid ROOT file with a selection.").c_str(),kMBIconStop);
		return;
	    }
	}
    }
    f.Close();
}

