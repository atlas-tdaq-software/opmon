/** -------------------------------------------------------------------
 *  RCTBrowser.cxx - implementation of class RCTBrowser
 *
 *  Additional inline classes:
 *  - RCTDisplayObjItem for the items in the browser
 *  - RCTIconBox container for the list view (right part of the window)
 *  - RCTNewSelectionDialog to request names for new selections
 *  - RCTValueDialog to request a value from the user in message box
 *
 *  author: Herbert Kaiser, Sep 2006 based on OHBrowser
 *          from OH package, but with many changes
 *  -------------------------------------------------------------------
 */

#include "TRootApplication.h"
#include "TGCanvas.h"
#include "TGMenu.h"
#include "TGFileDialog.h"
#include "TGStatusBar.h"
#include "TGLabel.h"
#include "TGTextEntry.h"
#include "TGButton.h"
#include "TGListView.h"
#include "TGListTree.h"
#include "TGListBox.h"
#include "TGToolBar.h"
#include "TGSplitter.h"
#include "TG3DLine.h"
#include "TGMimeTypes.h"
#include "TGMsgBox.h"
#include "TGFileDialog.h"
#include "TGPicture.h" 
#include "TROOT.h" 
#include "TEnv.h"
#include "TBrowser.h"
#include "TApplication.h"
#include "TFile.h"
#include "TKey.h"
#include "TKeyMapFile.h"
#include "TClass.h"
#include "TContextMenu.h"
#include "TSystem.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TInterpreter.h"
#include "TMath.h"
#include "TVirtualX.h"

#include "RCTController.h"
#include "RCTFile.h"
#include "RCTFileList.h"
#include "RCTGraphs.h"
#include "RCTContextMenu.h"
#include "RCTSelection.h"
#include "RCTSelectionList.h"
#include "RCTSelectionGraphs.h"
#include "opmon/RCTransLog.h"

#include "RCTBrowser.h"

#include <fstream>
#include <ios>
#include <iostream>
using std::cout; using std::endl;


// Browser menu command ids
enum ERootBrowserCommands 
  {   
    kFileQuit = 1001,
    
    kViewToolBar,
    kViewStatusBar,
    kViewLargeIcons,
    kViewSmallIcons,
    kViewList,
    kViewDetails,
    kViewLineUp,
    
    kCmdRefresh,

    kNewSelection,
    
    kOneLevelUp,            // One level up toolbar button
    kFSComboBox,            // File system combobox in toolbar
    
    kScatterPlotAddToSelection,
    kScatterPlotSelectTransition,
    kScatterPlotDeleteValues,
    kControllerAddToSelection,
    kControllerAddSubControllers,
    kControllerAddSubSubControllers,
    kControllerAddAllTransitions,
    kControllerAddAllCombined,
    kControllerSaveToCSV,
    kSelectionSetActive,
    kSelectionSaveToFile,
    kSelectionSaveToCSV,
    kSelectionChangeName,
    kSelectionRefresh,
    kSelectionDelete,
    kItemFindInTree,
    kItemDelete,
    kSelectionListNewSelection,
    kSelectionListLoadFromFile,
    kFileListMenuMerge
  };


//----- Struct for default icons
struct DefaultIcon_t {
  const char      *fPicnamePrefix;
  const TGPicture *fIcon[2];
};


#if 0
static DefaultIcon_t gDefaultIcon[] = 
{
  { "folder",  { 0, 0 } },
  { "app",     { 0, 0 } },
  { "doc",     { 0, 0 } },
  { "slink",   { 0, 0 } },
  { "histo",   { 0, 0 } },
  { "object",  { 0, 0 } }
};
#endif


//----- Toolbar stuff... 
static ToolBarData_t gToolBarData[] = {
  { "tb_bigicons.xpm",  "Large Icons",    kTRUE,  kViewLargeIcons, 0 },
  { "tb_smicons.xpm",   "Small Icons",    kTRUE,  kViewSmallIcons, 0 },
  { "tb_list.xpm",      "List",           kTRUE,  kViewList, 0 },
  { "tb_details.xpm",   "Details",        kTRUE,  kViewDetails, 0 },
  { "", "",  kTRUE,  0, 0 },
  { "refresh2.xpm",     "Refresh",        kFALSE, kCmdRefresh, 0 },
  { "", "",  kTRUE,  0, 0 },
  { "selection_t.xpm",     "New Selection",        kFALSE, kNewSelection, 0 },
  { 0, 0, kFALSE, 0, 0 }
};



class RCTDisplayObjItem : public TGLVEntry{
public:
  RCTDisplayObjItem( const TGWindow *p, const TGPicture *bpic, const TGPicture *spic, TGString *name, TObject *obj, EListViewMode viewMode );
};

//______________________________________________________________________________
RCTDisplayObjItem::RCTDisplayObjItem( const TGWindow *p, const TGPicture *bpic, const TGPicture *spic, TGString *name, TObject *obj, EListViewMode viewMode) :
  TGLVEntry(p, bpic, spic, name, 0, viewMode)
{
  // Create an icon box item.
  delete [] fSubnames;
  if ( obj->IsA()==RCTFile::Class() )
    {
      fSubnames = new TGString* [4];
      fSubnames[0] = new TGString(((RCTFile*)obj)->GetTitle());
      fSubnames[1] = new TGString(((RCTFile*)obj)->GetPartition());
      fSubnames[2] = new TGString(((RCTFile*)obj)->GetTime());
      fSubnames[3] = 0;
    }
  else if ( obj->IsA()==RCTSelectionItem::Class() || obj->IsA()==RCTSelectionItemCombination::Class() )
    {
      fSubnames = new TGString* [4];
      fSubnames[0] = new TGString(((RCTSelectionItem*)obj)->GetTitle());
      char floatvalue[1024];
      sprintf(floatvalue,"%u",static_cast<RCTSelectionItem*>(obj)->getEntries());
      fSubnames[1] = new TGString(floatvalue);
      sprintf(floatvalue,"%f",static_cast<RCTSelectionItem*>(obj)->getAverage());
      fSubnames[2] = new TGString(floatvalue);
      fSubnames[3] = 0;
    }
  else if ( obj->IsA()==RCTScatterGraph::Class() || obj->IsA()==RCTCombinedScatterGraph::Class() )
    {
      fSubnames = new TGString* [4];
      fSubnames[0] = new TGString(((RCTScatterGraph*)obj)->GetTitle());
      char floatvalue[1024];
      sprintf(floatvalue,"%u",static_cast<RCTScatterGraph*>(obj)->getEntries());
      fSubnames[1] = new TGString(floatvalue);
      sprintf(floatvalue,"%f",static_cast<RCTScatterGraph*>(obj)->getAverage());
      fSubnames[2] = new TGString(floatvalue);
      fSubnames[3] = 0;
    }
  else if ( obj->IsA()==RCTAveragesDiagram::Class() || obj->IsA()==RCTCombinedAveragesDiagram::Class() ) 
    {
      fSubnames = new TGString* [4];
      fSubnames[0] = new TGString(obj->GetTitle());
      fSubnames[1] = new TGString(" ");
      fSubnames[2] = new TGString(" ");
      fSubnames[3] = 0;

    }
  else 
    {
      fSubnames = new TGString* [2];
      fSubnames[0] = new TGString(obj->GetTitle());
      fSubnames[1] = 0;
    }
  int i;
  for (i = 0; fSubnames[i] != 0; ++i);
  fCtw = new int[i];
  for (i = 0; fSubnames[i] != 0; ++i) 
    fCtw[i] = gVirtualX->TextWidth(fFontStruct, fSubnames[i]->GetString(),
				   fSubnames[i]->GetLength());
}


class RCTIconBox : public TGLVContainer
{
private:
  Bool_t  fCheckHeaders;   // if true check headers
  
public:
  RCTIconBox(const TGWindow *p, TGListView *lv, UInt_t w, UInt_t h,
		   UInt_t options = kSunkenFrame,
		   ULong_t back = fgDefaultFrameBackground);
  
  void   AddObjItem(const char *name, TObject *obj);
  void   SetObjHeaders(TClass* type,const char *param=NULL, const char *param2=NULL);
  void   Refresh();
  void   RemoveAll();
};


RCTIconBox::RCTIconBox(const TGWindow *p, TGListView *lv, UInt_t w,
				   UInt_t h, UInt_t options, ULong_t back)
  : TGLVContainer(p, w, h, options, back)
{
  // Create iconbox containing ROOT objects in browser.
  fListView = lv;
  fCheckHeaders = kTRUE;
  //gSystem->RemoveTimer( (TTimer*)fRefresh );
  SetObjHeaders(this->IsA());
}


void RCTIconBox::AddObjItem(const char *name, TObject *obj)
{
  // Add object to iconbox.
  
  const TGPicture * pic = 0, * spic = 0;
  
  if(obj->IsA() == RCTFile::Class())
    {
      pic = fClient->GetPicture("doc_s.xpm");
      spic = fClient->GetPicture("doc_t.xpm");	
      if (fCheckHeaders) SetObjHeaders(obj->IsA(),"Partition","Time");
    }
  else if(obj->IsA() == RCTScatterGraph::Class())
    {
      pic = fClient->GetPicture("profile_s.xpm");
      spic = fClient->GetPicture("profile_t.xpm");	
      if (fCheckHeaders) SetObjHeaders(obj->IsA(),"Entries","Average");
    }
  else if(obj->IsA() == RCTCombinedScatterGraph::Class())
    {
      pic = fClient->GetPicture("profile_s.xpm");
      spic = fClient->GetPicture("profile_t.xpm");	
      if (fCheckHeaders) SetObjHeaders(obj->IsA(),"Entries","Average");
    }
  else if(obj->IsA() == RCTAveragesDiagram::Class())
    {
      pic = fClient->GetPicture("h2_t.xpm");
      spic = pic;
      if (fCheckHeaders) SetObjHeaders(obj->IsA(),"Entries","Average");
    }
  else if(obj->IsA() == RCTCombinedAveragesDiagram::Class())
    {
      pic = fClient->GetPicture("h2_t.xpm");
      spic = pic;
      if (fCheckHeaders) SetObjHeaders(obj->IsA(),"Entries","Average");
    }
  else if(obj->IsA() == RCTSelection::Class())
    {
      if (static_cast<RCTSelection*>(obj)->isNotUpdated())
	  pic = fClient->GetPicture("f2_t.xpm");
      else
	  pic = fClient->GetPicture("selection_t.xpm");
      spic = pic;
      if (fCheckHeaders) SetObjHeaders(obj->IsA());
    }
  else if(obj->IsA() == RCTSelectionItem::Class() || obj->IsA()==RCTSelectionItemCombination::Class() )
    {
      if (static_cast<RCTSelectionItem*>(obj)->isTainted())
	  pic = fClient->GetPicture("marker30.xpm");
      else
	  pic = fClient->GetPicture("marker29.xpm");
      spic = pic;
      if (fCheckHeaders) SetObjHeaders(obj->IsA(),"Values","Average");
    }
  else if(obj->IsA() == RCTSelectionHistogram::Class())
    {
      pic = fClient->GetPicture("h1_t.xpm");
      spic = pic;
      if (fCheckHeaders) SetObjHeaders(obj->IsA());
    }
  else if(obj->IsA() == RCTSelectionDiagram::Class())
    {
      pic = fClient->GetPicture("h2_t.xpm");
      spic = pic;
      if (fCheckHeaders) SetObjHeaders(obj->IsA());
    }
  else if(obj->IsA() == RCTSelectionComparison::Class())
    {
      pic = fClient->GetPicture("bld_copy.xpm");
      spic = pic;
      if (fCheckHeaders) SetObjHeaders(obj->IsA());
    }
  else if(obj->IsA() == RCTSelectionGraph::Class())
    {
      pic = fClient->GetPicture("profile_t.xpm");
      spic = pic;
      if (fCheckHeaders) SetObjHeaders(obj->IsA());
    }
  else {
      // only add known objects, others are just strange...
      return;
  }
  TGLVEntry* fi = new RCTDisplayObjItem(this, pic, spic, new TGString(name), obj, fViewMode);
 
  if (fi) fi->SetUserData(obj);
  
  AddItem(fi);
  
  fTotal++;
}


void RCTIconBox::SetObjHeaders(TClass* type,const char *param, const char* param2)
{
    // avoid setting this too often
    static TClass* lastType;
    if (type==lastType) return;
    lastType=type;

  // Set list box headers used to display detailed object information.
  // Currently this is only "Name" and "Title".
  if (!param)
      fListView->SetHeaders(2);
  else if (!param2)
      fListView->SetHeaders(3);
  else // if (param && param2)
      fListView->SetHeaders(4);

  fListView->SetHeader("Name",  kTextLeft, kTextLeft, 0);
  fListView->SetHeader("Title", kTextLeft, kTextLeft, 1);
  if (param) {
      fListView->SetHeader(param, kTextLeft, kTextLeft, 2);   
      if (param2)
	  fListView->SetHeader(param2, kTextLeft, kTextLeft, 3);
  }

}


void RCTIconBox::Refresh()
{
  // Sort icons, and send message to browser with number of objects
  // in box.
  // This automatically calls layout
  // Sort(fSortType); Disabled Sorting because its quite useless to sort alphabetically

  TGCanvas *canvas = (TGCanvas *) this->GetParent()->GetParent();
  canvas->Layout();
  
  // Make Browser display total objects in status bar
  SendMessage(fMsgWindow, MK_MSG(kC_CONTAINER, kCT_SELCHANGED),
	      fTotal, fSelected);
  
  MapSubwindows();
}


void RCTIconBox::RemoveAll()
{
  // Reset the fCheckHeaders flag.
  fCheckHeaders = kTRUE;

  fSelected = fTotal = 0;
  TGLVContainer::RemoveAll();
}


class RCTNewSelectionDialog : public TGTransientFrame {
    TGLabel* label;
    TGTextEntry* text;
    TGTextButton* okButton;
    TGTextButton* cancelButton;
    TGLayoutHints* layout;
    RCTSelectionList* selectionList;
    RCTSelection*& selection;
    bool changing;
public:
    RCTNewSelectionDialog(RCTBrowser* main, RCTSelectionList* selList,RCTSelection*& sel,const char* oldName=NULL);
    ~RCTNewSelectionDialog();
    void ShowModal();
    Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t );
};

RCTNewSelectionDialog::RCTNewSelectionDialog(RCTBrowser* main, RCTSelectionList* selList,RCTSelection*& sel,const char* oldName) :
    TGTransientFrame(gClient->GetRoot(),main,250,150),selectionList(selList),selection(sel),changing(false)
{
    if (oldName)
	SetWindowName("Change Name");
    else
	SetWindowName("New Selection");

    label=new TGLabel(this,"Give a name for the selection:");

    if (oldName) {
	text=new TGTextEntry(this,oldName);
	changing=true;
    } else
	text=new TGTextEntry(this,"");

    okButton=new TGTextButton(this,"&OK",11);
    cancelButton=new TGTextButton(this,"&Cancel",12);
    layout = new TGLayoutHints( kLHintsTop | kLHintsLeft, 5, 5, 2, 2);
    AddFrame(label,layout);
    AddFrame(text,layout);
    AddFrame(okButton,layout);
    AddFrame(cancelButton,layout);
    okButton->Associate(this);
    cancelButton->Associate(this);
}

RCTNewSelectionDialog::~RCTNewSelectionDialog() {
    delete label;
    delete text;
    delete okButton;
    delete cancelButton;
    delete layout;
}

void RCTNewSelectionDialog::ShowModal() {
    // position relative to the parent's window
    Window_t wdum;
    int ax, ay;
    gVirtualX->TranslateCoordinates(fMain->GetId(), GetParent()->GetId(),
			(Int_t)(((TGFrame *) fMain)->GetWidth() - fWidth) >> 1,
			(Int_t)(((TGFrame *) fMain)->GetHeight() - fHeight) >> 1,
			ax, ay, wdum);
    Move(ax, ay);
    Resize(210,110);
    SetWMPosition(ax, ay);
    SetWMSize(210,110);
    MapSubwindows();
    MapWindow();
    fClient->WaitFor(this);
}

Bool_t RCTNewSelectionDialog::ProcessMessage(Long_t msg, Long_t parm1, Long_t ) {
    if ( GET_MSG(msg) == kC_COMMAND ) {
	switch (parm1) {
	    case 11: 
		// OK clicked
		if (strcmp(text->GetText(),"")==0) return kTRUE;
		if (changing) {
		    selection->SetName(text->GetText());
		    delete this;
		    break;
		}
		if (!selectionList->addSelection(text->GetText())) {
		    new TGMsgBox( gClient->GetRoot(), this,
			    "Name already exits",
			    "Chose a different name!",kMBIconStop);
		    break;
		}
		selection=selectionList->getActiveSelection();
		delete this;
		break;
	    case 12:
		// Cancel clicked
		if (!changing) selection=NULL;
		delete this;
		break;
	}
    }
    return kTRUE;
}

class RCTYesNoDialog : public TGTransientFrame {
    TGLabel* label;
    TGTextButton* yesButton;
    TGTextButton* noButton;
    TGLayoutHints* layout;
    bool& value;
public:
    RCTYesNoDialog(const TGWindow* main,const char* windowTitle,const char* question,bool& _value);
    ~RCTYesNoDialog();
    void ShowModal();
    Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t );
};

RCTYesNoDialog::RCTYesNoDialog(const TGWindow* main,const char* windowTitle,const char* question,bool& _value) :
    TGTransientFrame(gClient->GetRoot(),main,400,80),value(_value)
{
    SetWindowName(windowTitle);
    label=new TGLabel(this,question);
    yesButton=new TGTextButton(this,"&Yes",11);
    noButton=new TGTextButton(this,"&No",12);
    layout = new TGLayoutHints( kLHintsTop | kLHintsLeft, 5, 5, 2, 2);
    AddFrame(label,layout);
    AddFrame(yesButton,layout);
    AddFrame(noButton,layout);
    yesButton->Associate(this);
    noButton->Associate(this);
    ShowModal();
}

RCTYesNoDialog::~RCTYesNoDialog() {
    delete label;
    delete yesButton;
    delete noButton;
    delete layout;
}

void RCTYesNoDialog::ShowModal() {
    // position relative to the parent's window
    Window_t wdum;
    int ax, ay;
    gVirtualX->TranslateCoordinates(fMain->GetId(), GetParent()->GetId(),
			(Int_t)(((TGFrame *) fMain)->GetWidth() - fWidth) >> 1,
			(Int_t)(((TGFrame *) fMain)->GetHeight() - fHeight) >> 1,
			ax, ay, wdum);
    Move(ax, ay);
    Resize(400,80);
    SetWMPosition(ax, ay);
    SetWMSize(400,80);
    Layout();
    MapSubwindows();
    MapWindow();
    fClient->WaitFor(this);
}

Bool_t RCTYesNoDialog::ProcessMessage(Long_t msg, Long_t parm1, Long_t ) {
    if ( GET_MSG(msg) == kC_COMMAND ) {
	switch (parm1) {
	    case 11: 
		// Yes clicked
		value=true;
		delete this;
		break;
	    case 12:
		// No clicked
		value=false;
		delete this;
		break;
	}
    }
    return kTRUE;
}

class RCTValueDialog : public TGTransientFrame {
    TGLabel* label;
    TGTextEntry* text;
    TGTextButton* okButton;
    TGTextButton* cancelButton;
    TGLayoutHints* layout;
    string*& value;
    bool allowEmpty;
public:
    RCTValueDialog(RCTBrowser* main,const char* windowTitle,const char* question,string*& value,const char* initValue=NULL,bool allowEmpty=false);
    ~RCTValueDialog();
    void ShowModal();
    Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t );
};

RCTValueDialog::RCTValueDialog(RCTBrowser* main,const char* windowTitle,const char* question,string*& _value,const char* initValue,bool _allowEmpty) :
    TGTransientFrame(gClient->GetRoot(),main,250,150),value(_value),allowEmpty(_allowEmpty)
{
    SetWindowName(windowTitle);
    label=new TGLabel(this,question);
    if (initValue)
	text=new TGTextEntry(this,initValue);
    else
	text=new TGTextEntry(this,"");
    okButton=new TGTextButton(this,"&OK",11);
    cancelButton=new TGTextButton(this,"&Cancel",12);
    layout = new TGLayoutHints( kLHintsTop | kLHintsLeft, 5, 5, 2, 2);
    AddFrame(label,layout);
    AddFrame(text,layout);
    AddFrame(okButton,layout);
    AddFrame(cancelButton,layout);
    okButton->Associate(this);
    cancelButton->Associate(this);
    ShowModal();
}

RCTValueDialog::~RCTValueDialog() {
    delete label;
    delete text;
    delete okButton;
    delete cancelButton;
    delete layout;
}

void RCTValueDialog::ShowModal() {
    // position relative to the parent's window
    Window_t wdum;
    int ax, ay;
    gVirtualX->TranslateCoordinates(fMain->GetId(), GetParent()->GetId(),
			(Int_t)(((TGFrame *) fMain)->GetWidth() - fWidth) >> 1,
			(Int_t)(((TGFrame *) fMain)->GetHeight() - fHeight) >> 1,
			ax, ay, wdum);
    Move(ax, ay);
    Resize(210,110);
    SetWMPosition(ax, ay);
    SetWMSize(210,110);
    Layout();
    MapSubwindows();
    MapWindow();
    fClient->WaitFor(this);
}

Bool_t RCTValueDialog::ProcessMessage(Long_t msg, Long_t parm1, Long_t ) {
    if ( GET_MSG(msg) == kC_COMMAND ) {
	switch (parm1) {
	    case 11: 
		// OK clicked
		if ( (!allowEmpty) && strcmp(text->GetText(),"")==0) return kTRUE;
		value=new string(text->GetText());
		delete this;
		break;
	    case 12:
		// Cancel clicked
		value=NULL;
		delete this;
		break;
	}
    }
    return kTRUE;
}

class RCTDeleteDialog : public TGTransientFrame {
    TGLabel* filesLabel;
    TGLabel* entriesLabel;
    TGLabel* newLabel;
    TGLabel* newFileNameLabel;
    TGTextButton* okButton;
    TGTextButton* cancelButton;
    TGListBox* fileBox;
    TGListBox* valueBox;
    TGLayoutHints* layout;
    RCTFileList* fileList;
    const string& controller;
    const string& transition;
    int valueSize;
    int curFile;
    string newFileName;
    string oldFileName;
    bool firstSelect;
public:
    RCTDeleteDialog(RCTBrowser* main, RCTFileList* _fileList,const string& _controller,const string& _transition);
    ~RCTDeleteDialog();
    void ShowModal();
    Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t );
};

RCTDeleteDialog::RCTDeleteDialog(RCTBrowser* main, RCTFileList* _fileList,const string& _controller,const string& _transition) :
    TGTransientFrame(gClient->GetRoot(),main,450,400),fileList(_fileList),controller(_controller),transition(_transition),valueSize(0),curFile(0),newFileName(""),oldFileName(""),firstSelect(true)
{
    SetWindowName( string(string("Delete Values in ")+controller+string(" - ")+transition).c_str() );

    filesLabel=new TGLabel(this,"Choose one file:");
    entriesLabel=new TGLabel(this,"Choose value(s)");
    newLabel=new TGLabel(this,"The file with the chosen values deleted will be saved as:");
    newFileNameLabel=new TGLabel(this,"new Filename");

    okButton=new TGTextButton(this,"&OK",11);
    cancelButton=new TGTextButton(this,"&Cancel",12);
    fileBox=new TGListBox(this,13);
    TIter next(fileList);
    int id=0;
    while ( RCTFile* f=static_cast<RCTFile*>(next()) ) {
	fileBox->AddEntry(f->GetName(),id);
	id++;
    }
    fileBox->Resize(400,100);
    valueBox=new TGListBox(this,14);
    valueBox->SetMultipleSelections(true);
    valueBox->Resize(100,150);
    layout = new TGLayoutHints( kLHintsTop | kLHintsLeft, 5, 5, 2, 2);
    AddFrame(filesLabel,layout);
    AddFrame(fileBox,layout);
    AddFrame(entriesLabel,layout);
    AddFrame(valueBox,layout);
    AddFrame(newLabel,layout);
    AddFrame(newFileNameLabel,layout);
    AddFrame(okButton,layout);
    AddFrame(cancelButton,layout);
    okButton->Associate(this);
    cancelButton->Associate(this);
}

RCTDeleteDialog::~RCTDeleteDialog() {
    delete filesLabel;
    delete entriesLabel;
    delete fileBox;
    delete valueBox;
    delete newLabel;
    delete newFileNameLabel;
    delete okButton;
    delete cancelButton;
    delete layout;
}

void RCTDeleteDialog::ShowModal() {
    // position relative to the parent's window
    Window_t wdum;
    int ax, ay;
    gVirtualX->TranslateCoordinates(fMain->GetId(), GetParent()->GetId(),
			(Int_t)(((TGFrame *) fMain)->GetWidth() - fWidth) >> 1,
			(Int_t)(((TGFrame *) fMain)->GetHeight() - fHeight) >> 1,
			ax, ay, wdum);
    Move(ax, ay);
    Resize(450,400);
    SetWMPosition(ax, ay);
    SetWMSize(450,400);
    MapSubwindows();
    Layout();
    MapWindow();
    fileBox->Select(0);
    ProcessMessage(0,13,0);

    fClient->WaitFor(this);
}

Bool_t RCTDeleteDialog::ProcessMessage(Long_t msg, Long_t parm1, Long_t ) {
    if ( GET_MSG(msg) == kC_COMMAND || firstSelect) {
	switch (parm1) {
	    case 11: 
		{
		vector<int> selected;
		for (int i=0;i<valueSize;i++) {
		    if (valueBox->GetSelection(i+1000))
			selected.push_back(i);
		}
		if (selected.size()==0) {
		    std::cout << "No values to be deleted! Cancelled!" << std::endl;
		    delete this;
		    break;
		}

		TIter next(fileList);
		RCTFile* f=0;
		for (int i=0;i<=curFile;i++)
		    f=static_cast<RCTFile*>(next());
		if (!f) {
		    std::cerr << "ERROR while trying to open the values to be deleted!" << std::endl;
		    delete this;
		    break;
		}
		string fullName=fileList->getFullName(controller);
		const RCTransLog* rctl=f->getTransLogByFull(fullName);
		if (!rctl) {
		    std::cerr << "ERROR while trying to open the values to be deleted!" << std::endl;
		    delete this;
		    break;
		}
		const vector<double>* valuesPointer=rctl->getTransTimes(transition);
		if (!valuesPointer) {
		    std::cout << "Found no values to be deleted! Cancelled!" << std::endl;
		    delete this;
		    break;
		}
		vector<double> values;
		int j=0;
		for (int i=0;i< (int)valuesPointer->size();i++) {
		    if (i==selected[j])
			j++;
		    else
			values.push_back( (*valuesPointer)[i] );
		}

		FILE* testf=0;
		TFile* newFile=0;
		if (newFileName==oldFileName) {
		    newFile=fileList->getFileForUpdate(curFile);
		    if (!newFile) {
			std::cerr << "ERROR while trying to open the values to be deleted!" << std::endl;
			delete this;
			break;
		    }
		    if (newFile->ReOpen("UPDATE")==-1) {
			std::cerr << "ERROR while trying to open the values to be deleted!" << std::endl;
			delete this;
			break;
		    }
		} else if ((testf=fopen(newFileName.c_str(),"r")))  {
		    fclose(testf);
		    bool yes=false;
		    new RCTYesNoDialog(gClient->GetRoot(),"WARNING - Overwrite?",string(string("A file named ")+newFileName+" already exists. Overwrite?").c_str(),yes);
		    if (!yes) {
			delete this;
			break;
		    }
		    if (!fileList->removeFileAt(curFile)) {
			delete this;
			break;
		    }
		    newFile=new TFile(newFileName.c_str(),"UPDATE");
		} else {
		    std::ifstream infile(oldFileName.c_str(), std::ios_base::binary);
		    std::ofstream outfile(newFileName.c_str(), std::ios_base::binary);
		    outfile << infile.rdbuf();
		    outfile.flush();
		    infile.close();
		    outfile.close();
		    if (!fileList->removeFileAt(curFile)) {
			delete this;
			break;
		    }
		    newFile=new TFile(newFileName.c_str(),"UPDATE");
		}

		rctl=static_cast<RCTransLog*>(newFile->GetObjectChecked(fullName.c_str(),RCTransLog::Class()));
		if (!rctl) {
		    cout<< "Could not find "<< controller << " in the new file. No changes are made, but " << newFileName << " is opened instead of " << oldFileName << endl;
		} else {
		    RCTransLog rt(*rctl);
		    const vector<double>* valuesToReplace=rt.getTransTimes(transition);

		    if (!valuesToReplace) {
			cout<< "Could not find "<< controller << " - " << transition << " in the new file. No changes are made, but " << newFileName << " is opened instead of " << oldFileName << endl;
		    } else {
			if (!rt.replaceValues(transition,values))
			    cout<< "Could not delete values in  "<< controller << " - " << transition << " in the new file. No changes are made, but " << newFileName << " is opened instead of " << oldFileName << endl;
			else {
			    TKey* key=newFile->GetKey(fullName.c_str());
			    int cycle = key->GetCycle();
			    char cyclestr[64];
			    sprintf(cyclestr,"%u",cycle);
			    newFile->WriteObject(&rt,fullName.c_str());
			    newFile->Delete(string(fullName+";"+cyclestr).c_str());
			}
		    }
		}
		if (newFile->ReOpen("READ")==-1) {
		    std::cerr << "ERROR: Could not reopen the file " << newFileName << " for reading!" << endl;
		    delete newFile;
		    delete this;
		    break;
		}
		if (newFileName==oldFileName) {
		    f->reloadAll();
		} else {
		    fileList->addFileAt(curFile,new RCTFile(newFileName.c_str(),newFile));
		}
		
		delete this;
		}
		break;
	    case 12:
		delete this;
		break;
	    case 13:
		if (firstSelect) firstSelect=false;
		curFile=fileBox->GetSelected();
		TIter next(fileList);
		RCTFile* f=0;
		for (int i=0;i<=curFile;i++)
		    f=static_cast<RCTFile*>(next());
		if (!f) break;

		oldFileName=f->GetName();
		if (oldFileName.substr(oldFileName.length()-13)==".changed.root")
		    newFileName=oldFileName;
		else if (oldFileName.substr(oldFileName.length()-5)==".root")
		    newFileName=oldFileName.substr(0,oldFileName.length()-5)+".changed.root";
		else
		    newFileName=oldFileName+".changed.root";
			    
		newFileNameLabel->SetText(newFileName.c_str());
		for (int i=0;i<valueSize;i++)
		    valueBox->RemoveEntry(i+1000);
		valueSize=0;
		const RCTransLog* rctl=f->getTransLogByFull(fileList->getFullName(controller));
		if (rctl) {
		    const vector<double>* values=rctl->getTransTimes(transition);
		    if (values) {
			char floatvalue[1024];
			for (int id=0;id < (int)values->size();id++) {
			    sprintf(floatvalue,"%f",(*values)[id]);
			    valueBox->AddEntry(floatvalue,id+1000);
			    valueSize++;
			}
		    }
		}
		MapSubwindows();
		fileBox->Layout();
		fileBox->MapSubwindows();
		valueBox->Layout();
		valueBox->MapSubwindows();
		Layout();
		break;
	}
    }
    return kTRUE;
}

ClassImp(RCTBrowser)


RCTBrowser::RCTBrowser(TBrowser *b, const char *name, UInt_t width, UInt_t height,RCTSelectionList* sel)
: TGMainFrame(gClient->GetRoot(), width, height), TBrowserImp(b),selectionList(sel)
{
    // Create browser with a specified width and height.
    CreateBrowser(name);
    Resize(width, height);
    Show();
}


RCTBrowser::RCTBrowser(TBrowser *b, const char *name, Int_t x, Int_t y,
	UInt_t width, UInt_t height,RCTSelectionList* sel)
: TGMainFrame(gClient->GetRoot(), width, height), TBrowserImp(b),selectionList(sel)
{
    // Create browser with a specified width and height and at position x, y.
    //SetIconName("app_s.xpm");
    CreateBrowser(name);
    MoveResize(x, y, width, height);
    SetWMPosition(x, y);
    Show();
}

RCTBrowser::~RCTBrowser()
{
    // Browser destructor.
    delete fToolBarSep;
    delete fToolBar;
    delete fStatusBar;
    delete fV1;
    delete fV2;
    delete fLbl1;
    delete fLbl2;
    delete fHf;
    delete fLt;
    delete fTreeHdr;
    delete fListHdr;
    delete fIconBox;
    delete fListView;
    delete fTreeView;

    delete fMenuBar;
    delete fFileMenu;
    delete fViewMenu;

    delete fMenuBarLayout;
    delete fMenuBarItemLayout;
    delete fMenuBarHelpLayout;
    delete fComboLayout;
    delete fBarLayout;

    delete fScatterPlotMenu;
    delete fScatterPlotRealMenu;
    delete fSelectionMenu;
    delete fItemMenu;
    delete fControllerMenu;
    delete fSelectionListMenu;
    delete fFileListMenu;

    delete fCurrentSelection;
    delete fCurrentTransition;

    if (fWidgets) fWidgets->Delete();
  delete fWidgets;
}

void RCTBrowser::CreateBrowser(const char *name)
{
  fWidgets = new TList;
  
  // Create menus
  fFileMenu = new TGPopupMenu(fClient->GetRoot());
  fFileMenu->AddEntry("&Quit",          kFileQuit);

  fViewMenu = new TGPopupMenu(fClient->GetRoot());
  fViewMenu->AddEntry("&Toolbar",            kViewToolBar);
  fViewMenu->AddEntry("Status &Bar",         kViewStatusBar);
  fViewMenu->AddSeparator();
  fViewMenu->AddEntry("Lar&ge Icons",        kViewLargeIcons);
  fViewMenu->AddEntry("S&mall Icons",        kViewSmallIcons);
  fViewMenu->AddEntry("&List",               kViewList);
  fViewMenu->AddEntry("&Details",            kViewDetails);
  fViewMenu->AddSeparator();
  fViewMenu->AddEntry("Lin&e up Icons",      kViewLineUp);
  
  fViewMenu->CheckEntry(kViewToolBar);
  fViewMenu->CheckEntry(kViewStatusBar);
  
  // This main frame will process the menu commands
  fFileMenu->Associate(this);
  fViewMenu->Associate(this);

  // Create context menus for selections and so on
  fScatterPlotMenu = new RCTContextMenu(fClient->GetRoot());
  fScatterPlotMenu->AddEntry("&Add to Selection", kScatterPlotAddToSelection);
  fScatterPlotMenu->AddEntry("&Set as Active Transition", kScatterPlotSelectTransition);

  fScatterPlotRealMenu = new RCTContextMenu(fClient->GetRoot());
  fScatterPlotRealMenu->AddEntry("&Add to Selection", kScatterPlotAddToSelection);
  fScatterPlotRealMenu->AddEntry("&Set as Active Transition", kScatterPlotSelectTransition);
  fScatterPlotRealMenu->AddSeparator();
  fScatterPlotRealMenu->AddEntry("&Delete Values", kScatterPlotDeleteValues);

  fControllerMenu= new RCTContextMenu(fClient->GetRoot());
  fControllerMenu->AddEntry("&Add to Selection", kControllerAddToSelection);
  fControllerMenu->AddEntry("Add &Subcontrollers to Selection", kControllerAddSubControllers);
  fControllerMenu->AddEntry("Add S&ubsubcontrollers to Selection", kControllerAddSubSubControllers);
  fControllerMenu->AddSeparator();
  fControllerMenu->AddEntry("Add all &Transitions to Selection", kControllerAddAllTransitions);
  fControllerMenu->AddEntry("Add all &combined Transitions", kControllerAddAllCombined);
  fControllerMenu->AddSeparator();
  fControllerMenu->AddEntry("Save To CS&V", kControllerSaveToCSV);

  fSelectionMenu= new RCTContextMenu(fClient->GetRoot());
  fSelectionMenu->AddEntry("&Set Active", kSelectionSetActive);
  fSelectionMenu->AddEntry("Change &Name", kSelectionChangeName);
  fSelectionMenu->AddSeparator();
  fSelectionMenu->AddEntry("Save to &File", kSelectionSaveToFile);
  fSelectionMenu->AddEntry("&Refresh with current Data", kSelectionRefresh);
  fSelectionMenu->AddSeparator();
  fSelectionMenu->AddEntry("Save to &CSV", kSelectionSaveToCSV);
  fSelectionMenu->AddSeparator();
  fSelectionMenu->AddEntry("&Delete", kSelectionDelete);

  fItemMenu= new RCTContextMenu(fClient->GetRoot());
  fItemMenu->AddEntry("&Show Full Name", kItemFindInTree);
  fItemMenu->AddEntry("&Delete", kItemDelete);

  fSelectionListMenu= new RCTContextMenu(fClient->GetRoot());
  fSelectionListMenu->AddEntry("&New Selection", kSelectionListNewSelection);
  fSelectionListMenu->AddEntry("&Load Selection from File", kSelectionListLoadFromFile);

  fFileListMenu= new RCTContextMenu(fClient->GetRoot());
  fFileListMenu->AddEntry("&Merge Files to One", kFileListMenuMerge);

  fScatterPlotMenu->Associate(this);
  fScatterPlotRealMenu->Associate(this);
  fControllerMenu->Associate(this);
  fSelectionMenu->Associate(this);
  fItemMenu->Associate(this);
  fSelectionListMenu->Associate(this);
  
  
  // Create menubar layout hints
  fMenuBarLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX, 0, 0, 1, 1);
  fMenuBarItemLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0);
  fMenuBarHelpLayout = new TGLayoutHints(kLHintsTop | kLHintsRight);
  
  // Create menubar
  fMenuBar = new TGMenuBar(this, 1, 1, kHorizontalFrame);
  fMenuBar->AddPopup("&File",    fFileMenu,    fMenuBarItemLayout);
  fMenuBar->AddPopup("&View",    fViewMenu,    fMenuBarItemLayout);
  
  AddFrame(fMenuBar, fMenuBarLayout);
  
  // Create toolbar and separator
  fToolBarSep = new TGHorizontal3DLine(this);
  fToolBar = new TGToolBar(this, 60, 20, kHorizontalFrame);
  fComboLayout = new TGLayoutHints(kLHintsLeft | kLHintsExpandY, 0, 0, 2, 0);
  
  int spacing = 8;
  for (int i = 0; gToolBarData[i].fPixmap; i++) 
    {
      if (strlen(gToolBarData[i].fPixmap) == 0) 
	{
	  spacing = 8;
	  continue;
      	}
      fToolBar->AddButton(this, &gToolBarData[i], spacing);
      spacing = 0;
    }  

  TGLayoutHints *lo;
  TGLabel *la;

  lo=new TGLayoutHints(kLHintsCenterY,20,0,0,0);
  la=new TGLabel(fToolBar,"Active Selection:");
  fWidgets->Add(lo);
  fWidgets->Add(la);
  fToolBar->AddFrame(la,lo);
  fCurrentSelection=new TGLabel(fToolBar,"none                              ");
  fCurrentSelection->SetForegroundColor(0xFF);
  fCurrentSelection->SetTextJustify(kTextLeft);
  lo=new TGLayoutHints(kLHintsCenterY,2,0,0,0);
  fWidgets->Add(lo);
  fToolBar->AddFrame(fCurrentSelection,lo);
  fCurrentTransition=new TGLabel(fToolBar,"none                                          ");
  fCurrentTransition->SetForegroundColor(0xFF0000);
  fCurrentTransition->SetTextJustify(kTextLeft);
  la=new TGLabel(fToolBar,"Active Transition:");
  lo=new TGLayoutHints(kLHintsCenterY,30,0,0,0);
  fWidgets->Add(la);
  fWidgets->Add(lo);
  fToolBar->AddFrame(la,lo);
  lo=new TGLayoutHints(kLHintsCenterY,2,0,0,0);
  fWidgets->Add(lo);
  fToolBar->AddFrame(fCurrentTransition,lo);
  
  fBarLayout = new TGLayoutHints(kLHintsTop | kLHintsExpandX);
  AddFrame(fToolBarSep, fBarLayout);
  AddFrame(fToolBar, fBarLayout);
  
  // Create panes
  fHf = new TGHorizontalFrame(this, 10, 10);
  
  fV1 = new TGVerticalFrame(fHf, 10, 10, kFixedWidth);
  fV2 = new TGVerticalFrame(fHf, 10, 10);
  fTreeHdr = new TGCompositeFrame(fV1, 10, 10, kSunkenFrame);
  fListHdr = new TGCompositeFrame(fV2, 10, 10, kSunkenFrame);
  
  fLbl1 = new TGLabel(fTreeHdr, "All Folders");
  fLbl2 = new TGLabel(fListHdr, "Contents of \".\"");
  
  lo = new TGLayoutHints(kLHintsLeft | kLHintsCenterY, 3, 0, 0, 0);
  fWidgets->Add(lo);
  fTreeHdr->AddFrame(fLbl1, lo);
  fListHdr->AddFrame(fLbl2, lo);
  
  lo = new TGLayoutHints(kLHintsTop | kLHintsExpandX, 0, 0, 1, 2);
  fWidgets->Add(lo);
  fV1->AddFrame(fTreeHdr, lo);
  fV2->AddFrame(fListHdr, lo);
  fV1->Resize(fTreeHdr->GetDefaultWidth()+100, fV1->GetDefaultHeight());
  
  lo = new TGLayoutHints(kLHintsLeft | kLHintsExpandY);
  fWidgets->Add(lo);
  fHf->AddFrame(fV1, lo);
  TGVSplitter *splitter = new TGVSplitter(fHf);
  splitter->SetFrame(fV1, kTRUE);
  lo = new TGLayoutHints(kLHintsLeft | kLHintsExpandY);
  fWidgets->Add(splitter);
  fWidgets->Add(lo);
  fHf->AddFrame(splitter, lo);
  lo = new TGLayoutHints(kLHintsRight | kLHintsExpandX | kLHintsExpandY);
  fWidgets->Add(lo);
  fHf->AddFrame(fV2, lo);
  
  // Create tree
  fTreeView = new TGCanvas(fV1, 10, 10, kSunkenFrame | kDoubleBorder);
  fLt = new TGListTree(fTreeView->GetViewPort(), 10, 10, kHorizontalFrame,
		       fgWhitePixel);
  fLt->Associate(this);
  fLt->SetAutoTips();
  fTreeView->SetContainer(fLt);
  fLt->SetCanvas(fTreeView);
  
  lo = new TGLayoutHints(kLHintsExpandX | kLHintsExpandY);
  fWidgets->Add(lo);
  fV1->AddFrame(fTreeView, lo);
  
  // Create list view (icon box)
  fListView = new TGListView(fV2, 520, 250);
  fIconBox = new RCTIconBox(fListView->GetViewPort(), fListView, 520, 250,
				  kHorizontalFrame, fgWhitePixel);
  fIconBox->Associate(this);
  fListView->GetViewPort()->SetBackgroundColor(fgWhitePixel);
  fListView->SetContainer(fIconBox);
  
  // reuse lo from "create tree"
  fV2->AddFrame(fListView, lo);
  
  AddFrame(fHf, lo);

  // Statusbar
  int parts[] = { 25, 75 };
  fStatusBar = new TGStatusBar(this, 60, 10);
  fStatusBar->SetParts(parts, 2);
  lo = new TGLayoutHints(kLHintsBottom | kLHintsExpandX, 0, 0, 3, 0);
  AddFrame(fStatusBar, lo);
  fWidgets->Add(lo);
  
  // Misc
  SetWindowName(name);
  SetIconName(name);
  SetIconPixmap("selection_t.xpm");
  SetClassHints("Browser", "Browser");
  
  SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
  
  fWaitCursor = gVirtualX->CreateCursor(kWatch);
  fListLevel = 0;
  fTreeLock  = kFALSE;
  
  MapSubwindows();

  SetDefaults();
  // we need to use GetDefaultSize() to initialize the layout algorithm...
  Resize(GetDefaultSize());

}

void RCTBrowser::Add(TObject *obj, const char *name, int )
{
  // Add items to the browser. This function has to be called
  // by the Browse() member function of objects when they are
  // called by a browser.
  
  if (obj) 
    {
	  if (!name) name = obj->GetName();
	  
	  if (obj->IsFolder() ) {
	      AddToTree(obj, name);
	  }
	  else {
	      AddToBox(obj, name);
	  }

	  if (obj->IsA()==RCTSelection::Class() )
	      AddToBox(obj, name);
    }
}


//______________________________________________________________________________
void RCTBrowser::AddToBox(TObject *obj, const char *name)
{
    // Add items to the iconbox of the browser.
    if (obj) 
    {
	if (!name)
            name = obj->GetName() ? obj->GetName() : "NoName";
      	fIconBox->AddObjItem(name, obj);
    }
}


//______________________________________________________________________________
void RCTBrowser::AddToTree(TObject *obj, const char *name)
{
  // Add items to the current TGListTree of the browser.
  if (obj && !fTreeLock) 
    {
      if (!name) name = obj->GetName();
      
      if (obj->IsA() == RCTFileList::Class())
	{
	  const TGPicture            *pic ;
	  pic = fClient->GetPicture("hdisk_t.xpm");
	  fLt->AddItem(fListLevel, name, obj,pic,pic);
	}
      else if (obj->IsA() == RCTController::Class())
	{
	  const TGPicture            *pic ;
	  const TGPicture            *spic ;
	  if (static_cast<RCTController*>(obj)->isNotInAllFiles()) {
	      pic = fClient->GetPicture("return_object_t.xpm");
	      spic = pic;
	  } else {
	      pic = fClient->GetPicture("ofolder_t.xpm");
	      spic = fClient->GetPicture("folder_t.xpm");
	  }
	  fLt->AddItem(fListLevel, name, obj,pic,spic);
	}
      else if (obj->IsA() == RCTSelectionList::Class())
	{
	  const TGPicture            *pic ;
	  pic = fClient->GetPicture("query_new.xpm");
	  fLt->AddItem(fListLevel, name, obj,pic,pic);
	}
      else if (obj->IsA() == RCTSelection::Class())
	{
	  const TGPicture            *pic ;
	  if (static_cast<RCTSelection*>(obj)->isNotUpdated()) {
	      pic = fClient->GetPicture("f2_t.xpm");
	  } else {
	      pic = fClient->GetPicture("selection_t.xpm");
	  }
	  fLt->AddItem(fListLevel, name, obj,pic,pic);
	}
      else 
	{
	  fLt->AddItem(fListLevel, name, obj);
	}
    }
}


//______________________________________________________________________________
void RCTBrowser::BrowseObj(TObject *obj)
{
  // Browse object. This, in turn, will trigger the calling of
  // RCTBrowser::Add() which will fill the IconBox and the tree.
  fIconBox->RemoveAll();
  obj->Browse(fBrowser);
  fIconBox->Refresh();
  if (fBrowser)
    fBrowser->SetRefreshFlag(kFALSE);
}


//______________________________________________________________________________
void RCTBrowser::CloseWindow()
{
  // In case window is closed via WM we get here.
  gInterpreter->DeleteGlobal(fBrowser);
  delete fBrowser;  // this in turn will delete this object
  gSystem->Exit(0);
}


//______________________________________________________________________________
void RCTBrowser::DisplayTotal(Int_t total, Int_t selected)
{
  // Display in statusbar total number of objects and number of
  // selected objects in IconBox.
  char tmp[128];
  const char *fmt;
  
  if (selected)
    fmt = "%d Object%s, %d selected.";
  else
    fmt = "%d Object%s.";
  
  sprintf(tmp, fmt, total, (total == 1) ? "" : "s", selected);
  fStatusBar->SetText(tmp, 0);
}


//______________________________________________________________________________
void RCTBrowser::DisplayDirectory()
{
  // Display current directory in second label, fLbl2.
  char *p, path[1024];
  
  fLt->GetPathnameFromItem( fListLevel, path, 3 );
  p = path;

  if (p== 0 || strlen(p) == 0)
    fLbl2->SetText(new TGString("Contents of \".\""));
  else
    fLbl2->SetText(new TGString(Form("Contents of \"%s\"", p)));
  fListHdr->Layout();
}


//______________________________________________________________________________

// only used for showing the path of a controller in the tree
string buildPathName(string fullName) {
    unsigned len=fullName.length();
    for (unsigned i=0;i<len;i++)
	if (fullName[i]=='|') fullName[i]='/'; 
    if (fullName[len-4]==':' && fullName[len-3]=='A' && fullName[len-2]=='P'
	    && fullName[len-1]=='P' )
	fullName.erase(len-4);
    fullName="/"+fullName;
    return fullName;
}

Bool_t RCTBrowser::ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)
{
  // Handle menu and other command generated by the user.
  switch (GET_MSG(msg)) 
    {
    case kC_COMMAND:
      
      switch (GET_SUBMSG(msg)) 
	{
	case kCM_BUTTON:
	case kCM_MENU:
	  
	  switch (parm1) 
	    {
	    case kFileQuit:
	      gApplication->Terminate(0);
	      break;
					
	      // Handle View menu items...
	    case kViewToolBar:
	      if (fViewMenu->IsEntryChecked(kViewToolBar))
		ShowToolBar(kFALSE);
	      else
		ShowToolBar();
	      break;
	    case kViewStatusBar:
	      if (fViewMenu->IsEntryChecked(kViewStatusBar))
		ShowStatusBar(kFALSE);
	      else
		ShowStatusBar();
	      break;
	    case kViewLargeIcons:
	    case kViewSmallIcons:
	    case kViewList:
	    case kViewDetails:
	      SetViewMode((Int_t)parm1);
	      break;
	    case kCmdRefresh:
	      {
		MyRefresh(true);
	      }
	      break;
	    case kNewSelection:
	      {
		RCTSelection* sel=NULL;
		(new RCTNewSelectionDialog(this,selectionList,sel))->ShowModal();
		if (!sel) break;
		fCurrentSelection->SetText(sel->GetName());
	      }
	      break;
	    case kViewLineUp:
	      break;	      

	    }
	  
	default:
	  // We should send command here
	  break;
	}
      break;
      
    case kC_LISTTREE:///tu
      switch (GET_SUBMSG(msg)) 
	{
	case kCT_ITEMCLICK:
	  if (parm1 == kButton1 || parm1 == kButton3) 
	  {
	      TGListTreeItem *item = fLt->GetSelected();
	      if (item)
	      {
		  if ( item != fListLevel ) 
	          {
		     ListTreeHighlight(item);
		     fClient->NeedRedraw(fLt);
                  }
		  if (parm1 == kButton3) {
		      TObject *obj = (TObject *)item->GetUserData();
		      if (obj) { if (obj->IsA() == RCTSelection::Class() ) {
			  Int_t x = (Int_t)(parm2 & 0xffff);
			  Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
			  Int_t res = fSelectionMenu->Show( x, y );
			  if (res==kSelectionSetActive) {
			      static_cast<RCTSelection*>(obj)->activate();
			      fCurrentSelection->SetText(obj->GetName());
			  } else if (res==kSelectionRefresh) {
			      static_cast<RCTSelection*>(obj)->refresh();
			      MyRefresh();
			  } else if (res==kSelectionChangeName) {
			      RCTSelection* sel=static_cast<RCTSelection*>(obj);
			      (new RCTNewSelectionDialog(this,selectionList,sel,sel->GetName()))->ShowModal();
			      MyRefresh();
			  } else if (res==kSelectionSaveToFile) {
			      static const char *FileType[] = {"Selection file", "*.sel", 0, 0 };
			      TGFileInfo* fInfo=new TGFileInfo();
			      fInfo->fFileTypes=FileType;
			      fInfo->fFilename=new char[300];
			      strcpy(fInfo->fFilename,obj->GetName());
			      strcat(fInfo->fFilename,".sel");
			      new TGFileDialog(gClient -> GetRoot(),this,kFDSave,fInfo);
			      if (!fInfo->fFilename) break;
			      static_cast<RCTSelection*>(obj)->saveToFile(fInfo->fFilename);
			      delete fInfo;
			  } else if (res==kSelectionSaveToCSV) {
			      static const char *FileType[] = {"CSV file", "*.csv", 0, 0 };
			      TGFileInfo* fInfo=new TGFileInfo();
			      fInfo->fFileTypes=FileType;
			      fInfo->fFilename=new char[300];
			      strcpy(fInfo->fFilename,obj->GetName());
			      strcat(fInfo->fFilename,".csv");
			      new TGFileDialog(gClient -> GetRoot(),this,kFDSave,fInfo);
			      if (!fInfo->fFilename) break;
			      static_cast<RCTSelection*>(obj)->saveToCSV(fInfo->fFilename);
			      delete fInfo;
			  } else if (res==kSelectionDelete) {
			      fLt->DeleteItem(item);
			      static_cast<RCTSelection*>(obj)->remove();
			      MyRefresh();
			  }
		      } else if (obj->IsA()==RCTSelectionList::Class() ) {
			  Int_t x = (Int_t)(parm2 & 0xffff);
			  Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
			  Int_t res = fSelectionListMenu->Show( x, y );
			  if (res==kSelectionListNewSelection) {
			      RCTSelection* sel=NULL;
			      (new RCTNewSelectionDialog(this,selectionList,sel))->ShowModal();
			      if (!sel) break;
			      fCurrentSelection->SetText(sel->GetName());
			      fIconBox->RemoveAll();
			      MyRefresh();
			      BrowseObj(obj);
			  } else if (res==kSelectionListLoadFromFile) {
			      static const char *FileType[] = {"Selection file","*.sel", "All files","*",0,0};
			      TGFileInfo* fInfo=new TGFileInfo();
			      fInfo->fFileTypes=FileType;
			      new TGFileDialog(gClient -> GetRoot(),this,kFDOpen,fInfo);
			      if (!fInfo->fFilename) break;
			      selectionList->loadSelectionsFromFile(fInfo->fFilename);
			      delete fInfo;
			      fIconBox->RemoveAll();
			      MyRefresh();
			      BrowseObj(obj);
			  }
		      } else if (obj->IsA()==RCTController::Class() ) {
			  Int_t x = (Int_t)(parm2 & 0xffff);
			  Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
			  Int_t res = fControllerMenu->Show( x, y );
			  if (	   res==kControllerAddToSelection
				|| res==kControllerAddSubControllers
				|| res==kControllerAddSubSubControllers
				|| res==kControllerAddAllTransitions
				|| res==kControllerAddAllCombined)
			  {
			      if (res!=kControllerAddAllCombined && res!=kControllerAddAllTransitions
				      && strcmp(fCurrentTransition->GetTitle(),
					  "none                                          ")==0) {
				  new TGMsgBox( gClient->GetRoot(), this,
					  "No transition selected!",
					  "You need to set active a transition first."
					  " Do this by right click on one of the "
					  "'Scatter Plot' items.",kMBIconStop);
				  break;
			      }
			      RCTSelection* sel;
			      if ( !(sel=selectionList->getActiveSelection()) ) {
				  (new RCTNewSelectionDialog(this,selectionList,sel))->ShowModal();
				  if (!sel) break;
				  fCurrentSelection->SetText(sel->GetName());
			      }
			      if (res==kControllerAddToSelection)
				  static_cast<RCTController*>(obj)->addToSelection(this,sel,fCurrentTransition->GetTitle());
			      else if (res==kControllerAddSubControllers)
				  static_cast<RCTController*>(obj)->addSubToSelection(this,sel,fCurrentTransition->GetTitle());
			      else if (res==kControllerAddSubSubControllers)
				  static_cast<RCTController*>(obj)->addSubSubToSelection(this,sel,fCurrentTransition->GetTitle());
			      else if (res==kControllerAddAllTransitions)
				  static_cast<RCTController*>(obj)->addAllTransitionsToSelection(this,sel);
			      else
				  static_cast<RCTController*>(obj)->addAllCombinedToSelection(this,sel);
			  } else if (res==kControllerSaveToCSV) {
			      string* value;
			      new RCTValueDialog(this,"Choose Maximum Level",
				      "Choose a level number up to which\n"
				      " the controllers are saved.\n\n"
				      "E.g. 0 will only save this controller's data,\n"
				      "1 will save this controller and the next level...\n"
				      "-1 will just save all the controllers of this branch",value,"-1");
			      if (!value) break;
			      int upTo=-1;
			      sscanf(value->c_str(),"%d",&upTo);
			      delete value;
			      static const char *FileType[] = {"CSV file", "*.csv", 0, 0 };
			      TGFileInfo* fInfo=new TGFileInfo();
			      fInfo->fFileTypes=FileType;
			      fInfo->fFilename=new char[300];
			      strcpy(fInfo->fFilename,obj->GetName());
			      strcat(fInfo->fFilename,".csv");
			      new TGFileDialog(gClient -> GetRoot(),this,kFDSave,fInfo);
			      if (!fInfo->fFilename) break;
			      static_cast<RCTController*>(obj)->saveToCSV(fInfo->fFilename,upTo);
			      delete fInfo;
			  }
		      } else if (obj->IsA()==RCTFileList::Class() ) {
			  Int_t x = (Int_t)(parm2 & 0xffff);
			  Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
			  Int_t res = fFileListMenu->Show( x, y );
			  if (res==kFileListMenuMerge) {
			      static const char *FileType[] = {"ROOT file", "*.root", 0, 0 };
			      TGFileInfo* fInfo=new TGFileInfo();
			      fInfo->fFileTypes=FileType;
			      new TGFileDialog(gClient -> GetRoot(),this,kFDSave,fInfo);
			      if (!fInfo->fFilename) break;
			      //if (strlen(fInfo->fFilename)<5 || strcmp(&fInfo->fFilename[strlen(fInfo->fFilename)-5],".root")!=0)
				  //selectionList->getFileList()->mergeFiles((string(fInfo->fFilename)+".root").c_str(),true);
			      //else
			      selectionList->getFileList()->mergeFiles(fInfo->fFilename,true);
			      delete fInfo;
			  }
		      } }
		  }
	      }
            }
	    break;
	case kCT_ITEMDBLCLICK:
	  if (parm1 == kButton1) 
	  {	
	     fClient->NeedRedraw(fLt);
	  }
	  break;
	default:
	  break;
        }
      break;
      
    case kC_CONTAINER:
      switch (GET_SUBMSG(msg)) 
	{
	case kCT_ITEMCLICK:
	  if (fIconBox->NumSelected() == 1) 
	  {	
	      // display title of selected object
	     TGLVEntry*item;
	     TObject *obj=NULL;
	     void *p = 0;
	     if ((item = (TGLVEntry*) fIconBox->GetNextSelected(&p)) != 0) 
	     {
		obj = (TObject *) item->GetUserData();
		fStatusBar->SetText(obj->GetTitle(), 1);
		  	  
	     }
	     if ( parm1 == kButton3 )
	     {
		 if ( obj )
		 {
		     if ( obj->IsA() == RCTCombinedScatterGraph::Class() )
		     {
			 Int_t x = (Int_t)(parm2 & 0xffff);
			 Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
			 Int_t res = fScatterPlotMenu->Show( x, y );
			 if (res==kScatterPlotAddToSelection)
			 {
			     RCTSelection* sel;
			     if ( !(sel=selectionList->getActiveSelection()) ) {
				 (new RCTNewSelectionDialog(this,selectionList,sel))->ShowModal();
				 if (!sel) break;
				 fCurrentSelection->SetText(sel->GetName());
			     }
			     RCTCombinedScatterGraph* g=static_cast<RCTCombinedScatterGraph*>(obj);
			     sel->addItem(g->getController(),g->getCombination());
			 } else if (res==kScatterPlotSelectTransition) {
			     fCurrentTransition->SetText(obj->GetName());
			 }
		     }
		     else if ( obj->IsA() == RCTScatterGraph::Class() ) {
			 Int_t x = (Int_t)(parm2 & 0xffff);
			 Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
			 Int_t res = fScatterPlotRealMenu->Show( x, y );
			 if (res==kScatterPlotAddToSelection)
			 {
			     RCTSelection* sel;
			     if ( !(sel=selectionList->getActiveSelection()) ) {
				 (new RCTNewSelectionDialog(this,selectionList,sel))->ShowModal();
				 if (!sel) break;
				 fCurrentSelection->SetText(sel->GetName());
			     }
			     RCTScatterGraph* g=static_cast<RCTScatterGraph*>(obj);
			     sel->addItem(g->getController(),g->getTransition());
			 } else if (res==kScatterPlotSelectTransition) {
			     fCurrentTransition->SetText(obj->GetName());
			 } else if (res==kScatterPlotDeleteValues) {
			     RCTScatterGraph* g=static_cast<RCTScatterGraph*>(obj);
			     (new RCTDeleteDialog(this,selectionList->getFileList(),g->getController(),g->getTransition()))->ShowModal();
			     MyRefresh(true);
			 }
		     }
		     else if (obj->IsA() == RCTSelectionItem::Class() ||
			      obj->IsA() == RCTSelectionItemCombination::Class() ) {
			 Int_t x = (Int_t)(parm2 & 0xffff);
			 Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
			 Int_t res = fItemMenu->Show( x, y );
			 if (res==kItemDelete) {
			     static_cast<RCTSelectionItem*>(obj)->remove();
			     MyRefresh(true);
			 } else if (res==kItemFindInTree) {
			     new TGMsgBox( gClient->GetRoot(), this, "Full Name",buildPathName(selectionList->getFileList()->getFullName(static_cast<RCTSelectionItem*>(obj)->getController())).c_str(),kMBIconAsterisk);
			 }

		     }
		     else if (obj->IsA() == RCTSelection::Class() ) {
			 Int_t x = (Int_t)(parm2 & 0xffff);
			 Int_t y = (Int_t)((parm2 >> 16) & 0xffff);
			 Int_t res = fSelectionMenu->Show( x, y );
			 if (res==kSelectionSetActive) {
			     static_cast<RCTSelection*>(obj)->activate();
			     fCurrentSelection->SetText(obj->GetName());
			 } else if (res==kSelectionDelete) {
			     static_cast<RCTSelection*>(obj)->remove();
			     MyRefresh(true);
			 }
		     }
		 }
	     }
	  }
	  break;
	case kCT_ITEMDBLCLICK:
	  if (parm1 == kButton1) 
	    {		
	      if (fIconBox->NumSelected() == 1) 
		{
		  void *p = 0;
		  TGLVEntry*item;
		  if ((item = (TGLVEntry*) fIconBox->GetNextSelected(&p)) != 0)
		    IconBoxAction((TObject *)item->GetUserData());
		}
	    }
	  break;
	case kCT_SELCHANGED:
	  {
	    DisplayTotal((Int_t)parm1, (Int_t)parm2);
	  }
	  break;
	default:
	  break;
	}
      break;
      
    default:
      break;
    }
  fClient->NeedRedraw(fIconBox);
  return kTRUE;
  
}

//______________________________________________________________________________
void RCTBrowser::Chdir(TGListTreeItem *item)
{
  // Make object associated with item the current directory.
  if (item) 
    {
      TGListTreeItem *i = item;
      TString dir;
      while (i) 
	{
	  TObject *obj = (TObject*) i->GetUserData();
	  if (obj) 
	    {
	      if (obj->IsA() == TDirectory::Class()) 
		{
		  dir = "/" + dir;
		  dir = obj->GetName() + dir;
            	}
	      if (obj->IsA() == TFile::Class()) 
		{
		  dir = ":/" + dir;
		  dir = obj->GetName() + dir;
            	}
	      if (obj->IsA() == TKey::Class()) 
		{
		  if ( obj->IsA() == TDirectory::Class() ) 
		    {
		      dir = "/" + dir;
		      dir = obj->GetName() + dir;
		    }
            	}
	    }
	  i = i->GetParent();
      	}
      if (gDirectory && dir.Length()) gDirectory->cd(dir.Data());
    }
}


//______________________________________________________________________________
void RCTBrowser::ListTreeHighlight(TGListTreeItem *item)///kwery do servera po flt i add 
{
  // Open tree item and list in iconbox its contents.
  //
  
  fListLevel = item;
  
  if (fListLevel) 
    {
      DisplayDirectory();
      TObject *obj = (TObject *) fListLevel->GetUserData();
      
      if (obj) 
	{
	  gVirtualX->SetCursor(fId, fWaitCursor);
	  gVirtualX->Update();
	  
	  if (fListLevel->IsActive())
	    {
	      BrowseObj(obj);
	    }
	  gVirtualX->SetCursor(fId, kNone);
	}
    }
}



//______________________________________________________________________________
void RCTBrowser::IconBoxAction(TObject *obj)
{
  // Default action when double clicking on icon.
  
  if (obj) 
    {
      const TGPicture            *pic ;
      const TGPicture            *spic ;
      
      if ( obj->IsA()== RCTFileList::Class() )
	{
	  pic = fClient->GetPicture("home_t.xpm");
	  spic = fClient->GetPicture("home_t.xpm");
	}
      else if ( obj->IsA()== RCTController::Class() )
	{
	    if (static_cast<RCTController*>(obj)->isNotInAllFiles()) {
		pic = fClient->GetPicture("return_object_t.xpm");
		spic = pic;
	    } else {
		pic = fClient->GetPicture("ofolder_t.xpm");
		spic = fClient->GetPicture("folder_t.xpm");
	    }
	}
      else if (obj->IsA() == RCTSelectionList::Class())
      {
	  pic = fClient->GetPicture("query_new.xpm");
	  spic= pic;
      }
      else if (obj->IsA() == RCTSelection::Class())
      {
	  if (static_cast<RCTController*>(obj)->isNotInAllFiles())
	      pic = fClient->GetPicture("f2_t.xpm");
	  else
	      pic = fClient->GetPicture("selection_t.xpm");

	  spic= pic;
      }
      else 
	{
	  pic = fClient->GetPicture("ofolder_t.xpm");
	  spic = fClient->GetPicture("folder_t.xpm");
	}
      
      Bool_t useLock = kTRUE;
      
      gVirtualX->SetCursor(fId, fWaitCursor);
      gVirtualX->Update();
      
      if (obj->IsFolder()) 
	{
	  fIconBox->RemoveAll();
	  TGListTreeItem *itm = 0;
	  
	  if (fListLevel) 
	    {
	      fLt->OpenItem(fListLevel);
	      itm = fListLevel->GetFirstChild();
	    } 
	  else
	    itm = fLt->GetFirstItem();

	  while (itm && (itm->GetUserData() != obj))
	    itm = itm->GetNextSibling();
	  
	  if (!itm && fListLevel) 
	    {
	      itm = fLt->AddItem(fListLevel, obj->GetName(),pic,spic);
	      if (itm) itm->SetUserData(obj);
	    }
	  
	  if (itm) 
	    {
	      fListLevel = itm;
	      DisplayDirectory();
	      TObject *kobj = (TObject *) itm->GetUserData();
	      if (kobj->IsA() == TKey::Class()) 
		{
		  Chdir(fListLevel->GetParent());
		  
		  kobj = gROOT->FindObject(kobj->GetName());
		  
		  if (kobj) 
		    {
		      TGListTreeItem *parent = fListLevel->GetParent();
		      fLt->DeleteItem(fListLevel);
		      TGListTreeItem *kitem = fLt->AddItem(parent, kobj->GetName(),pic,spic);
		      if (kitem) 
			{
			  obj = kobj;
			  useLock = kFALSE;
			  kitem->SetUserData(kobj);
			  fListLevel = kitem;
			} 
		      else
			fListLevel = parent;
		    }
            	}
	      fLt->ClearHighlighted();
	      fLt->HighlightItem(fListLevel);
	      fClient->NeedRedraw(fLt);
	    }
      	}
      
      if (useLock) fTreeLock = kTRUE;
      obj->Browse(fBrowser);
      if (useLock) fTreeLock = kFALSE;
      
      Chdir(fListLevel);
      
      if (obj->IsFolder()) 
	{
	  fIconBox->Refresh();
	  if (fBrowser)
	    fBrowser->SetRefreshFlag(kFALSE);
      	} 
      else
      	gVirtualX->SetCursor(fId, kNone);
    }
}

bool RCTBrowser::RecursiveDeleteItem( TGListTree * tree, TGListTreeItem * item, TObject * ptr )
{
  // Delete item with fUserData == ptr. Search tree downwards starting
  // at item.
  
  if ( item && ptr )
    {
      if ( item -> GetUserData() == ptr )
	{
	  tree -> DeleteItem( item );
	  return true;
	}
      else
	{
	  if ( item -> GetFirstChild() )
            RecursiveDeleteItem( tree, item -> GetFirstChild(),  ptr );
	  return RecursiveDeleteItem( tree, item -> GetNextSibling(), ptr );
	}
    }
  return false;
}

//______________________________________________________________________________
void RCTBrowser::RecursiveRemove(TObject *obj)
{
  RecursiveDeleteItem( fLt, fLt->GetFirstItem(), obj );
}

//______________________________________________________________________________
// Refresh the browser contents.
// it is empty because in application refresh colud be done only with clicking on some icon
void RCTBrowser::Refresh(Bool_t ) {}

void RCTBrowser::MyRefresh(Bool_t ctrl_Refresh)
{
  doMyRefresh=true;
  if (ctrl_Refresh) {
      TGListTreeItem *item = fLt->GetSelected();
      TObject* obj=static_cast<TObject*>(item->GetUserData());
      if ( obj->IsA()==RCTController::Class() ) {
	  static_cast<RCTController*>(obj)->setRefresh();
      }
  }
  RedrawTreeList();
  if ((fBrowser && fBrowser->GetRefreshFlag())) 
    {
      gVirtualX->SetCursor(fId, fWaitCursor);
      gVirtualX->Update();
      
      // Refresh the IconBox
      if (fListLevel) 
	{
	  fIconBox->RemoveAll();
	  TObject *obj = (TObject *) fListLevel->GetUserData();
	  if (obj) 
	    {
	      BrowseObj(obj);
	    }
      	} 
      //else
      //!!!!!!!!! BrowseObj(gROOT);  
      
      fClient->NeedRedraw(fLt);
      
      gVirtualX->SetCursor(fId, kNone);
      }
    doMyRefresh=false;
}



//______________________________________________________________________________
void RCTBrowser::ShowToolBar(Bool_t show)
{
  // Show or hide toolbar.
  if (show) 
    {
      ShowFrame(fToolBar);
      ShowFrame(fToolBarSep);
      fViewMenu->CheckEntry(kViewToolBar);
    } 
  else 
    {
      HideFrame(fToolBar);
      HideFrame(fToolBarSep);
      fViewMenu->UnCheckEntry(kViewToolBar);
    }
}


//______________________________________________________________________________
void RCTBrowser::ShowStatusBar(Bool_t show)
{

  // Show or hide statusbar.
  if (show) {
    ShowFrame(fStatusBar);
    fViewMenu->CheckEntry(kViewStatusBar);
  } else {
    HideFrame(fStatusBar);
    fViewMenu->UnCheckEntry(kViewStatusBar);
  }
}

//______________________________________________________________________________
void RCTBrowser::SetDefaults(const char *iconStyle, const char *sortBy)
{
  
  // IconStyle: details by default
  SetViewMode(kViewDetails, kTRUE);
  
}


//______________________________________________________________________________
void RCTBrowser::SetViewMode(Int_t new_mode, Bool_t force)
{
  // Set iconbox's view mode and update menu and toolbar buttons accordingly.
  int i, bnum;
  EListViewMode lv;
  
  if (force || (fViewMode != new_mode)) 
    {
      switch (new_mode) 
	{
	default:
	  if (!force)
	    return;
	  else
	    new_mode = kViewLargeIcons;
	case kViewLargeIcons:
	  bnum = 0;
	  lv = kLVLargeIcons;
	  break;
	case kViewSmallIcons:
	  bnum = 1;
	  lv = kLVSmallIcons;
	  break;
	case kViewList:
	  bnum = 2;
	  lv = kLVList;
	  break;
	case kViewDetails:
	  bnum = 3;
	  lv = kLVDetails;
	  break;
      	}
      
      fViewMode = new_mode;
      fViewMenu->RCheckEntry(fViewMode, kViewLargeIcons, kViewDetails);
      
      for (i = 0; i < 4; ++i)
	gToolBarData[i].fButton->SetState((i == bnum) ? kButtonEngaged : kButtonUp);
      
      fListView->SetViewMode(lv);
    }
}


void RCTBrowser::RedrawTreeList( )
{
    if ( !fListLevel )
	return;
	
    gVirtualX->SetCursor(fId, fWaitCursor);
    gVirtualX->Update();
    fIconBox->RemoveAll();
    TObject *obj = (TObject *) fListLevel->GetUserData();
    if (obj) 
    {
	if (!doMyRefresh)
	    fTreeLock = kTRUE;
	else
	    fTreeLock=kFALSE;
	BrowseObj(obj);
	fTreeLock = kFALSE;
    }
    gVirtualX->SetCursor(fId, kNone);
}

