/** -----------------------------------------------------------------
 *  RCTSelectionList.h - definition of class RCTSelectionList
 *
 *  provides the interface for the list of selections shown in
 *  the browser
 *
 *  author: Herbert Kaiser, Sep 2006
 *  -----------------------------------------------------------------
 */

#ifndef RCT_SELECTION_LIST_H
#define RCT_SELECTION_LIST_H

#include <vector>
#include <string>
#include "TNamed.h"

using std::vector;
using std::string;

class TBrowser;
class RCTSelection;
class RCTFileList;

class RCTSelectionList: public TNamed {
protected:
    RCTFileList* list;
    vector<RCTSelection*> selections;
    RCTSelection* activeSelection;

public:
    RCTSelectionList(RCTFileList* fileList) :
       	list(fileList),selections(0),activeSelection(NULL) {}
    ~RCTSelectionList();
    virtual Bool_t IsFolder() const { return kTRUE; }
    virtual void Browse(TBrowser *b);
    bool addSelection(const string& name);
    void removeSelection(RCTSelection* sel);
    void activateSelection(RCTSelection* sel);
    void loadSelectionsFromFile(const char* fileName,bool refresh=false);
    RCTSelection* getActiveSelection() { return activeSelection; }
    RCTFileList* getFileList() { return list; }

    ClassDef(RCTSelectionList,1) // root folder for selections
};

#endif //  RCT_SELECTION_LIST_H

