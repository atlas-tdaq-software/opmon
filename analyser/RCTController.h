/** -------------------------------------------------------------
 *  RCTController.h - class RCTController
 *
 *  Provides the controller items of the run control tree.
 *  See CreateChildren() for the search of the subcontrollers
 *  and the construction of the graphs shown for that controller.
 *  See addToSelection() etc. for adding items to a selection.
 *  If isNotInAllFiles() gives true the items are shown with a
 *  red '!', to make clear that not all files have this ctrlr.
 *
 *  author: Herbert Kaiser, Sep 2006
 *  -------------------------------------------------------------
 */

#ifndef RCT_CONTROLLER_H
#define RCT_CONTROLLER_H

#include <string>
#include <vector>
#include "TNamed.h"
#include "THashList.h"

using std::string;
using std::vector;
using std::pair;

class TGWindow;
class TBrowser;
class RCTSelection;
class RCTFileList;


class RCTController: public TNamed {
    RCTFileList* list;
    THashList children;
    unsigned childNumber;
    bool refresh;
    bool notInAllFiles;
    void CreateChildren(TBrowser *b=NULL);
    void addOrdered(const vector< pair<string,pair<unsigned,double> > >& transNames, const vector<const char* const*>& combs,const char* const* transOrder, TBrowser* b=NULL);
    void saveChildren(std::ofstream& ostr,int level,int maxlevel);

public:
    static bool oldtdaq;

    RCTController(const char* name, RCTFileList* fList,bool _notInAllFiles=false);
    ~RCTController();
    virtual Bool_t IsFolder() const {return true; };
    virtual void Browse(TBrowser *b);
    void setRefresh() {refresh=true; };
    bool isNotInAllFiles() {return notInAllFiles;}
    void addToSelection(TGWindow* msgParent,RCTSelection* sel,const char* transName);
    void addSubToSelection(TGWindow* msgParent,RCTSelection* sel,const char* transName);
    void addSubSubToSelection(TGWindow* msgParent,RCTSelection* sel,const char* transName);
    void addAllCombinedToSelection(TGWindow* msgParent,RCTSelection* sel);
    void addAllTransitionsToSelection(TGWindow* msgParent,RCTSelection* sel);
    void saveToCSV(const char* fileName,int upto);

    ClassDef(RCTController,1) // controller class
};

#endif // RCT_CONTROLLER_H

