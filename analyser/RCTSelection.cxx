/** -------------------------------------------------
 *  RCTSelection.cxx - implementation of classes:
 *  - RCTSelection
 *  - RCTSelectionItem, RCTSelectionItemCombination
 *  - RCTFilesInformation
 *  author: Herbert Kaiser, Sep 2006
 *  -------------------------------------------------
 */

#include <fstream>
#include <iostream>
#include "TCollection.h"
#include "TFile.h"
#include "TKey.h"
#include "TBrowser.h"
#include "TGMsgBox.h"
#include "opmon/RCTransLog.h"
#include "RCTFileList.h"
#include "RCTFile.h"
#include "RCTSelectionList.h"
#include "RCTSelectionGraphs.h"
#include "RCTSelection.h"

using std::ofstream;
using std::cout;
using std::cerr;
using std::endl;


ClassImp(RCTSelection)

RCTSelection::RCTSelection(const char* name,RCTSelectionList* selList,RCTFileList* fileList) :
       	TNamed(name,"Selection"),parent(selList),list(fileList),active(false),notUpdated(false)
{
    items.SetOwner(true);
    filesInfo=new RCTFilesInformation(fileList);
    graphlist= new RCTSelectionGraphList(name,&items,filesInfo);
}

RCTSelection::RCTSelection(RCTSelectionList* selList,RCTFileList* fileList, TDirectory* dir) :
       	TNamed(dir->GetName(),"Selection"),parent(selList),list(fileList),active(false),notUpdated(true)
{
    TKey *key;
    TIter next(dir->GetListOfKeys());
    key=(TKey*) next();
    if (!key) throw RCTSelectionItemCreationFailed();
    filesInfo=static_cast<RCTFilesInformation*>(key->ReadObj());
    if (!filesInfo) throw RCTSelectionItemCreationFailed();
    while ((key = (TKey *) next())) {
	RCTSelectionItem* item= static_cast<RCTSelectionItem*>(key->ReadObj());
	if (!item) throw RCTSelectionItemCreationFailed();
	item->setFilesAndSelection(fileList,this);
	items.Add(item);
    }
    graphlist= new RCTSelectionGraphList(dir->GetName(),&items,filesInfo);
}

RCTSelection::~RCTSelection() {
    items.Delete();
    delete filesInfo;
    delete graphlist;
}

void RCTSelection::Browse(TBrowser *b) {
    unsigned count=0;
    TIter next(&items);
    while (TObject* item=next() ) {
	b->RecursiveRemove(item);
	b->Add(item, item->GetName() );
	count++;
    }
    b->RecursiveRemove(graphlist);
    if (count>0)
	b->Add(graphlist,graphlist->GetName());
}

bool RCTSelection::addItem(const string& controller,const string& transition) {
    string name=controller+": "+transition;
    if (items.FindObject(name.c_str())) return false;
    try {
	items.Add(new RCTSelectionItem(this,name.c_str(),controller,transition,list));
    } catch (RCTSelectionItemCreationFailed) {
	new TGMsgBox(gClient->GetRoot(),gClient->GetWindowByName("opmon_analyser"),"Information",(string("Item '")+controller+":"+transition+"' not available from these files").c_str(),kMBIconExclamation);
	return false;
    }
    graphlist->requestRefresh();
    return true;
}

bool RCTSelection::addItem(const string& controller,const char* const* comb) {
    string name=controller+": "+comb[0];
    if (items.FindObject(name.c_str())) return false;
    try {
	items.Add(new RCTSelectionItemCombination(this,name.c_str(),controller,comb,list));
    } catch (RCTSelectionItemCreationFailed) {
	new TGMsgBox(gClient->GetRoot(),gClient->GetWindowByName("opmon_analyser"),"Information",(string("Item '")+controller+":"+comb[0]+"' not available from these files").c_str(),kMBIconExclamation);
	return false;
    }
    graphlist->requestRefresh();
    return true;
}

void RCTSelection::removeItem(RCTSelectionItem* item) {
    if (items.Remove(item)) {
	delete item; item=NULL;
	graphlist->requestRefresh();
    }
}

void RCTSelection::remove() {
    parent->removeSelection(this);
}

void RCTSelection::activate() {
    parent->activateSelection(this);
}

void RCTSelection::saveToFile(const char* fileName) {
    TFile* f= new TFile(fileName,"UPDATE");
    if (f->IsZombie() || (!(f->IsOpen())) ) {
	new TGMsgBox(gClient->GetRoot(),gClient->GetWindowByName("opmon_analyser"),"Information",(string("File '")+fileName+"' is not a valid file with selections. The file will be recreated.").c_str(),kMBIconExclamation);
	// actually one should delete f at this point, but it seems that ROOT does it by itself
	// in any case delete f; would give a segmentation fault
	f=new TFile(fileName,"RECREATE");
    }
    TKey* key=f->FindKey(GetName());
    TDirectory* dir;
    if (key) {
	new TGMsgBox(gClient->GetRoot(),gClient->GetWindowByName("opmon_analyser"),"Information",(string("Selection with name '")+GetName()+"' already exists in this file, it will be overwritten.").c_str(),kMBIconExclamation);
	dir=static_cast<TDirectory*>(key->ReadObj());
	if (!dir) {
	    new TGMsgBox(gClient->GetRoot(),gClient->GetWindowByName("opmon_analyser"),"Information",(string("File '")+fileName+"' is not a valid file with selections. The file will be recreated.").c_str(),kMBIconExclamation);
	    f->Close();
	    delete f;
	    f=new TFile(fileName,"RECREATE");
	    dir= f->mkdir(GetName());
	} else {
	    dir->Delete("*;*");
	}
    } else
	dir= f->mkdir(GetName());

    dir->WriteObject(filesInfo,filesInfo->GetName());
    TIter next(&items);
    while ( TObject* o=next() )
	dir->WriteObject(o,o->GetName());

    f->Close();
    delete f;

    cout << "Successfully saved selection '" << GetName() << "' in '" << fileName << "'." << endl;
}

void RCTSelection::refresh() {
    TIter next(&items);
    while (RCTSelectionItem* item=static_cast<RCTSelectionItem*>(next()) )
	item->refresh();

    delete graphlist;
    filesInfo->refresh(list);
    graphlist= new RCTSelectionGraphList(GetName(),&items,filesInfo);
    notUpdated=false;
}

void RCTSelection::saveToCSV(const char* fileName) {
    ofstream fout(fileName,std::ios::trunc);
    if (fout.bad()) {
	cerr << "ERROR: Could not open '"<<fileName<<"' for writing!" << endl;
	return;
    }
    fout<<"Selection:,"<<GetName()<<endl;
    fout<<"Files the selection was created from:"<< endl;
    const vector<string>* fNames=filesInfo->getFileNames();
    for (unsigned i=0;i<fNames->size();i++)
	fout << (*fNames)[i] << ",";
    fout << endl << endl;
    fout <<"Controller,Transition,Number of Values,Average"<<endl;

    TIter next(&items);
    while (RCTSelectionItem* item=static_cast<RCTSelectionItem*>(next()) ) {
	fout<<item->getController()<<","<<item->getTransition()<<","
	    <<item->getEntries()<<","<<item->getAverage()<<endl;
    }

    cout<< "Successfully saved CSV data to '" << fileName << "'." << endl;

    fout.close();
}


ClassImp(RCTSelectionItem)

void RCTSelectionItem::refresh(bool fromConstructor,bool verbose) {
    distribution.clear();
    tainted=false;
    const string fullName=list->getFullName(controller);
    if (fullName=="N/A") {
	if (verbose) cout << "Selection Item: '" << GetName() << "' not available any more. Will be removed." << endl;
	if (fromConstructor) throw RCTSelectionItemCreationFailed();
	remove();
	return;
    }
    unsigned curSize=0;
    TIter next(list);
    while ( RCTFile* f=static_cast<RCTFile*>(next()) ) {
	const RCTransLog* rctl=f->getTransLogByFull(fullName);
	if (!rctl) {
	    tainted=true;
	    continue;
	}
	const vector<double>* trT=rctl->getTransTimes(transition);
	if (trT) {
	    values.resize(curSize+trT->size());
	    memcpy(&values[curSize],&(*trT)[0],sizeof(double)*trT->size());
	    curSize=values.size();
	    distribution.push_back(trT->size());
	} else {
	    distribution.push_back(0);
	    tainted=true;
	}
    }
    if (values.size()==0) {
	if (verbose) cout << "Selection Item: " << GetName() << " not available any more. Will be removed." << endl;
	if (fromConstructor) throw RCTSelectionItemCreationFailed();
	remove();
	return;
    }
    avg=average(values);
}

void RCTSelectionItem::remove() {
    selection->removeItem(this);
}


ClassImp(RCTSelectionItemCombination)

RCTSelectionItemCombination:: RCTSelectionItemCombination(RCTSelection* parent,const char* name,const string& ctrl,const char* const* comb,RCTFileList* _list,bool verbose) :
    RCTSelectionItem(parent,name,ctrl,comb[0],_list,verbose,false)
{
    SetNameTitle(name,"Combined Selection Item");
    unsigned i=0;
    while (comb[i]) i++;
    combination.resize(i);
    i=0;
    while (comb[i]) {
	combination[i]=comb[i];
	i++;
    }
    refresh(true,verbose);
}

void RCTSelectionItemCombination::refresh(bool fromConstructor,bool verbose) {
    distribution.clear();
    tainted=false;
    const string fullName=list->getFullName(controller);
    if (fullName=="N/A") {
	if (verbose) cout << "Selection Item: " << GetName() << " not available any more. Will be removed." << endl;
	if (fromConstructor) throw RCTSelectionItemCreationFailed();
	remove();
	return;
    }
    const char** comb= new const char*[combination.size()+1];
    for (unsigned i=0;i<combination.size();i++)
	comb[i]=combination[i].c_str();
    comb[combination.size()]=0;

    unsigned curSize=0;
    TIter next(list);
    while ( RCTFile* f=static_cast<RCTFile*>(next()) ) {
	const RCTransLog* rctl=f->getTransLogByFull(fullName);
	if (!rctl) {
	    tainted=true;
	    continue;
	}
	vector<double> trT;
	if (rctl->getCombinedTransTimes(comb,trT)) {
	    values.resize(curSize+trT.size());
	    memcpy(&values[curSize],&trT[0],sizeof(double)*trT.size());
	    curSize=values.size();
	    distribution.push_back(trT.size());
	} else {
	    distribution.push_back(0);
	    tainted=true;
	}
    }
    if (values.size()==0) {
	if (verbose) cout << "Selection Item: " << GetName() << " not available any more. Will be removed." << endl;
	if (fromConstructor) throw RCTSelectionItemCreationFailed();
	remove();
	return;
    }
    avg=average(values);
}


ClassImp(RCTFilesInformation)

RCTFilesInformation::RCTFilesInformation(RCTFileList* list) : TNamed("FilesInformation",""),fileNames(0) {
    TIter next(list);
    while ( TNamed* f=static_cast<TNamed*>(next()) ) {
	fileNames.push_back(f->GetName());
    }
}

void RCTFilesInformation::refresh(RCTFileList* list) {
    fileNames.clear();
    TIter next(list);
    while ( TNamed* f=static_cast<TNamed*>(next()) ) {
	fileNames.push_back(f->GetName());
    }
}

