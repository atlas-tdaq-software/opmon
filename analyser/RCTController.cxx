/** -----------------------------------------------------------
 *  RCTController.cxx - implementation of class RCTController
 *  author: Herbert Kaiser, Sep 2006
 *  -----------------------------------------------------------
 */

#include <list>
#include <fstream>
#include <iostream>
#include <iomanip>
#include "TGMsgBox.h"
#include "TBrowser.h"
#include "opmon/RCStringTree.h"
#include "opmon/RCTransLog.h"
#include "opmon/RCTimeSequence.h"
#include "opmon/Transitions.h"
#include "RCTFile.h"
#include "RCTFileList.h"
#include "RCTGraphs.h"
#include "RCTSelection.h"
#include "RCTController.h"

using std::ofstream;
using std::endl;
using std::cerr;
using std::cout;
using std::pair;


bool RCTController::oldtdaq=false;

ClassImp(RCTController)

RCTController::RCTController(const char* name,RCTFileList* fList,bool _notInAllFiles) : 
    TNamed(name,"" ),list(fList),refresh(true),notInAllFiles(_notInAllFiles)
{
    children.SetOwner(true);
    
    if (list->isSegment(name))
	SetTitle("Segment Controller");
    else 
	SetTitle("Application");
}


RCTController::~RCTController() {
    children.Delete();
}

void RCTController::addAllTransitionsToSelection(TGWindow* msgParent,RCTSelection* sel) {
    if (refresh) {
	CreateChildren();
	refresh=false;
    }
    RCTScatterGraph* g;
    const char* const* transOrder;
    transOrder=transitionsOrder;
    
    unsigned i=0;
    while (transOrder[i])
	if ( (g=static_cast<RCTScatterGraph*>(children.FindObject(transOrder[i++])))  )
	    sel->addItem(GetName(),g->getTransition());

    // add the rest by just trying to add all
    TIter next(&children);
    while (TObject* child=next() ) 
	if (child->IsA()==RCTScatterGraph::Class())
	    sel->addItem(GetName(),static_cast<RCTScatterGraph*>(child)->getTransition());
}

void RCTController::addAllCombinedToSelection(TGWindow* msgParent,RCTSelection* sel) {
    if (refresh) {
	CreateChildren();
	refresh=false;
    }
    RCTCombinedScatterGraph* g;
    BUILD_COMBINED_TRANS_VECTOR(comb)
      for (unsigned i=0;i<comb.size();i++)
	if ( (g=static_cast<RCTCombinedScatterGraph*>(children.FindObject(comb[i][0])))  )
	  sel->addItem(GetName(),g->getCombination());
    
}

void RCTController::addToSelection(TGWindow* msgParent,RCTSelection* sel,const char* transName) {
    if (refresh) {
	CreateChildren();
	refresh=false;
    }
    RCTScatterGraph* g;
    if ( (g=static_cast<RCTScatterGraph*>(children.FindObject(transName)))  ) {
	if (g->IsA()==RCTCombinedScatterGraph::Class())
	    sel->addItem(GetName(),static_cast<RCTCombinedScatterGraph*>(g)->getCombination());
	else
	    sel->addItem(GetName(),g->getTransition());
    } else {
	new TGMsgBox( gClient->GetRoot(),msgParent,
		"Error!",
		(string("Transition '")+transName+"' does not exist for controller '"+
		 GetName()+"! This item is not added to the selection.").c_str(),kMBIconStop);
    }
}

void RCTController::addSubToSelection(TGWindow* msgParent,RCTSelection* sel,const char* transName) {
    if (refresh) {
	CreateChildren();
	refresh=false;
    }
    TIter next(&children);
    while (TObject* child=next() ) 
	if (child->IsA()==RCTController::Class())
	    static_cast<RCTController*>(child)->addToSelection(msgParent,sel,transName);
}

void RCTController::addSubSubToSelection(TGWindow* msgParent,RCTSelection* sel,const char* transName) {
    if (refresh) {
	CreateChildren();
	refresh=false;
    }
    TIter next(&children);
    while (TObject* child=next() ) 
	if (child->IsA()==RCTController::Class())
	    static_cast<RCTController*>(child)->addSubToSelection(msgParent,sel,transName);
}

void RCTController::CreateChildren(TBrowser* b) {
    children.Delete();
    string name=GetName();
    TNamed* child;
    
    vector<string> childrenNames;
    list->getTrees()[0]->getChildren(name,childrenNames);
    childNumber=0;
    unsigned i;
    for (i=0;i<childrenNames.size();i++) {
	if (list->getEntries()==1 || list->controllerExistsInAll(childrenNames[i],true))
	    child = new RCTController(childrenNames[i].c_str(),list);
	else
	    child = new RCTController(childrenNames[i].c_str(),list,true);
	children.Add(child);
	childNumber++;
	if (b) b->Add(child, child->GetName() );
    }
    // add additional children not from the first file
    for (i=1;i<list->getEntries();i++) {
	childrenNames.clear();
	list->getTrees()[i]->getChildren(name,childrenNames);
	for (unsigned j=0;j<childrenNames.size();j++) {
	    if (children.FindObject(childrenNames[j].c_str()) )
		continue;
	    child=new RCTController(childrenNames[j].c_str(),list,true);
	    children.Add(child);
	    childNumber++;
	    if (b) b->Add(child, child->GetName() );
	}
    }

    vector< pair<string,pair<unsigned,double> > > transNames;
    list->getAllTransitions(name,transNames);
    
    if (transNames.size()==0) return;

    for (i=0;i<transNames.size();i++) {
	child = new RCTScatterGraph( transNames[i].first.c_str(), (name+": "+transNames[i].first),
		name,transNames[i].first,list,transNames[i].second.first,transNames[i].second.second);
	children.Add(child);
	if (b) b->Add(child, child->GetName() );
    }

    // try to add the classic combined transitions

	BUILD_COMBINED_TRANS_VECTOR(combs)
	addOrdered(transNames,combs,transitionsOrder,b);

}

void RCTController::addOrdered(const vector< pair<string,pair<unsigned,double> > >& transNames,const vector<const char* const*>& combs,const char* const* transOrder, TBrowser* b) {
    string name=GetName();
    TNamed* child;
    for (unsigned i=0;i<combs.size();i++) {

	unsigned k=1;
	int entries=-1;
	double average=0.0;
	bool found=false;
	while (combs[i][k]) {
	    for (unsigned j=0;j<transNames.size();j++) {
		if (strcmp(transNames[j].first.c_str(),combs[i][k])==0) {
		   found=true;
		   if (entries==-1 || entries>((int)transNames[j].second.first) )
		       entries=(int)transNames[j].second.first;
		   average+=transNames[j].second.second;
		   break;
		}
	    }
	    if (!found || entries==0) {
		entries=0;
		average=0.0;
		break;
	    } else
		found=false;
	    k++;
	}
	child= new RCTCombinedScatterGraph(combs[i][0],(name+": "+combs[i][0]),name,combs[i],list,(unsigned)entries,average);
	children.Add(child);
	if (b) b->Add(child, child->GetName() );
    }
    // add average diagrams for this controller
    child= new RCTAveragesDiagram("Averages",name+": Averages",name,transOrder,list);
    children.Add(child);
    if (b) b->Add(child, child->GetName() );
    child= new RCTCombinedAveragesDiagram("Combined Averages",name+": Combined Averages",name,combs,list);
    children.Add(child);
    if (b) b->Add(child, child->GetName() );

    if (strcmp(GetName(),ROOT_CTRL)==0) {
	TIter itList(list);
	const RCTimeSequence* rcts;
	while ( RCTFile* rctf=static_cast<RCTFile*>(itList()) ) {
	    rcts=rctf->getRootTimes();
	    if (rcts) {
		cout << "In file " << rctf->GetName() << endl;
		cout << "Internal transition sequence for RootController:" << endl;
		pair< const std::list<string>* ,const std::list<string>* > seqs=rcts->getSequence();
		std::list<string>::const_iterator nameIt,timeIt;
		timeIt=seqs.second->begin();
		for (nameIt=seqs.first->begin();nameIt!=seqs.first->end();nameIt++) {
		    std::cout << std::setw(25) << (*nameIt) << " at " << (*timeIt) << endl;
		    timeIt++;
		}

		std::list<string> combNames;
		std::list<string> combTimes;
		rcts->getCombinedSequence(combs,combNames,combTimes);

		timeIt=combTimes.begin();
		cout << "Transition sequence for RootController:" << endl;
		for (nameIt=combNames.begin();nameIt!=combNames.end();nameIt++) {
		    std::cout << std::setw(13) << (*nameIt) << " at " << (*timeIt) << endl;
		    timeIt++;
		}
	    }
	    
	}

    }
}

void RCTController::Browse(TBrowser *b)
{
    if (refresh) {
	CreateChildren(b);
	refresh=false;
    } else {
	TIter next(&children);
	while ( TObject* child=next() ) {
	    if (child->IsA()!=RCTController::Class()) {
		b->RecursiveRemove(child);
		b->Add(child, child->GetName() );
	    }
	}
    }
}

void RCTController::saveChildren(ofstream& fout,int level,int maxlevel) {
    if (refresh) {
	CreateChildren();
	refresh=false;
    }
    TIter next(&children);
    TObject* child;
    while ( (child=next()) && (child->IsA()!=RCTScatterGraph::Class()) );
    if (!child) {
	cerr << "ERROR: Unforeseen error occured during data collection, no graphs for this Ctrl!!" << endl;
	return;
    }
    fout<<endl<<endl;
    fout<<"Transitions for Controller:,"<<GetName()<<endl;
    fout<<"Transition,Average time,Values"<<endl;
    do {
	if (child->IsA()==RCTScatterGraph::Class()) {
	    const string& transition=static_cast<RCTScatterGraph*>(child)->getTransition();
	    RCTSelectionItem* item;
	    try {
		item= new RCTSelectionItem(NULL,"CVSItem",GetName(),transition,list,false);
	    } catch(RCTSelectionItemCreationFailed) {
		continue;
	    }
	    fout <<transition<<","<<item->getAverage();
	    const vector<double>* values=item->getValues();
	    for (unsigned i=0;i<values->size();i++)
		fout <<","<<(*values)[i];
	    fout << endl;
	    delete item;
	} else if (child->IsA()==RCTCombinedScatterGraph::Class() ) {
	    const char* const* comb= static_cast<RCTCombinedScatterGraph*>(child)->getCombination();
	    RCTSelectionItemCombination * item;
	    try {
		item=new RCTSelectionItemCombination(NULL,"CVSItem",GetName(),comb,list,false);
	    } catch(RCTSelectionItemCreationFailed) {
		continue;
	    }
	    fout <<comb[0]<<","<<item->getAverage();
	    const vector<double>* values=item->getValues();
	    for (unsigned i=0;i<values->size();i++)
		fout <<","<<(*values)[i];
	    fout << endl;
	    delete item;
	}
    } while ( (child=next()) );

    if ( (maxlevel<0) || (level<maxlevel) ) {
	level++;
	// save subcontrollers
	TIter second(&children);
	while ( (child=second()) && (child->IsA()==RCTController::Class()) )
	    static_cast<RCTController*>(child)->saveChildren(fout,level,maxlevel);
    }
}

void RCTController::saveToCSV(const char* fileName,int upTo) {
    ofstream fout(fileName,std::ios::trunc);
    if (fout.bad()) {
	cerr << "ERROR: Could not open '"<<fileName<<"' for writing!" << endl;
	return;
    }
    fout<<GetName()<<endl;
    fout<<"Values for "<< GetName() << " and subcontrollers up to sublevel number:,"<<upTo<<endl;
    fout<<"Files taken for the values:,"<< endl;
    TIter itList(list);
    while ( RCTFile* rctf=static_cast<RCTFile*>(itList()) ) {
	fout << rctf->GetName() << "," << rctf->GetPartition() << "," << rctf->GetTime() << endl;
    }

    int level=0;
    saveChildren(fout,level,upTo);

    cout<< "Successfully saved CSV data to '" << fileName << "'." << endl;

    fout.close();
}

