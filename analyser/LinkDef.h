/** ------------------------------------------------------------
 *  LinkDef.h - file for the production of the ROOT dictionary
 *
 *  used to generate the ROOT dictionary for opmon_analyser,
 *  that makes the classes in a way readable for the 
 *  ROOT object browser etc. Classes marked with a '+' at
 *  the end are storable in ROOT files
 *
 *  author: Herbert Kaiser, Sep 2006
 *  ------------------------------------------------------------
 */

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class RCTFile;
#pragma link C++ class RCTController;
#pragma link C++ class RCTDisplayGuiFactory;
#pragma link C++ class RCTFileList;
#pragma link C++ class RCTBrowser;
#pragma link C++ class RCTStartFolder;
#pragma link C++ class RCTScatterGraph;
#pragma link C++ class RCTCombinedScatterGraph;
#pragma link C++ class RCTAveragesDiagram;
#pragma link C++ class RCTCombinedAveragesDiagram;
#pragma link C++ class RCTSelectionList;
#pragma link C++ class RCTSelection;
#pragma link C++ class RCTSelectionItem+;
#pragma link C++ class RCTSelectionItemCombination+;
#pragma link C++ class RCTFilesInformation+;
#pragma link C++ class RCTSelectionGraphList;
#pragma link C++ class RCTSelectionHistogram;
#pragma link C++ class RCTSelectionDiagram;
#pragma link C++ class RCTSelectionGraph;
#pragma link C++ class RCTSelectionComparison;

#endif
