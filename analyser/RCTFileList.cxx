/** -----------------------------------------------------
 *  RCTFileList.cxx - implementation of class RCTFileList
 *  author: Herbert Kaiser, Sep 2006
 *  -----------------------------------------------------
 */

#include <owl/time.h>
#include "TBrowser.h"
#include "TClass.h"
#include "TKey.h"
#include "TFile.h"
#include "THashList.h"
#include "opmon/RCStringTree.h"
#include "opmon/RCTransLog.h"
#include "opmon/RCTimeSequence.h"
#include "RCTFile.h"
#include "RCTFileList.h"

#include <iostream>
using std::cout; using std::cerr; using std::endl;

ClassImp(RCTFileList)
  

RCTFileList::RCTFileList(const vector<string>& fileList) :
    TObjArray(fileList.size())
{
    SetOwner(true);
    RCTFile* f;
    entries=0;
    for (unsigned i=0;i<fileList.size();i++) {
	try {
	    f=new RCTFile(fileList[i].c_str());
	    Add(f);
	    trees.push_back(f->getTree());
	    entries++;
	} catch(RCTFileException e) {
	    cerr << e.msg << endl;
	}
    }
    if (entries==0) {
	cerr << "No file added to list! Need at least one file!" << endl;
	throw RCTFileException("Empty file list!");
    }
}

RCTFileList::~RCTFileList() {
    Delete();
}


void RCTFileList::Browse(TBrowser *b)
{
    TIter next(this);
    while (RCTFile* f=static_cast<RCTFile*>(next()) ) {
	b->RecursiveRemove(f);
	b->Add(f,f->GetName());
    }
}

bool RCTFileList::controllerExistsInAll(const string& ctrl,bool startSecond) {
    unsigned i=0;
    if (startSecond) i=1;
    while (i<trees.size())
	if ( !(trees[i++]->exists(ctrl)) ) return false;
    return true;
}

string RCTFileList::getFullName(const string& ctrl) {
    string fullName;
    for (unsigned i=0;i<entries;i++) {
	fullName=trees[i]->getFullName(ctrl);
	if (fullName!="N/A")
	    break;
    }
    return fullName;
}

void RCTFileList::getAllTransitions(const string& controller, vector< pair<string,pair<unsigned,double> > >& transList) {
    transList.clear();
    vector<string> intList;
    const string fullName=getFullName(controller).c_str();
    TIter next(this);
    RCTFile* f=static_cast<RCTFile*>(next());
    const RCTransLog* rctl;
    while ( !(rctl=f->getTransLogByFull(fullName))) {
	f=static_cast<RCTFile*>(next());
	if (!f) {
	    return;
	}
    }
    rctl->getTransList(intList);

    const vector<double>* values;

    for (unsigned i=0;i<intList.size();i++) {
	values=rctl->getTransTimes(intList[i]);
	if (values)
	    transList.push_back(pair<string,pair<unsigned,double> >( intList[i],pair<unsigned,double>(values->size(),rctl->getAverageTime(intList[i])) ) );
	else
	    transList.push_back(pair<string,pair<unsigned,double> >( intList[i],pair<unsigned,double>(0,0.0) ) );
    }
    while ( (f=static_cast<RCTFile*>(next())) ) {
	rctl=f->getTransLogByFull(fullName);
	if (!rctl) continue;
	rctl->getTransList(intList);
	unsigned oldListSize=transList.size();
	for (unsigned i=0;i<intList.size();i++) {
	    bool addMe=true;
	    for (unsigned j=0;j<oldListSize;j++)
		if (intList[i]==transList[j].first) {
		    unsigned oldsize=transList[j].second.first;
		    double newAverage=transList[j].second.second*oldsize;
		    values=rctl->getTransTimes(intList[i]);
		    if (values) {
			transList[j].second.first+=values->size();
			transList[j].second.second=(newAverage+(rctl->getAverageTime(intList[i])*(transList[j].second.first-oldsize)) )/transList[j].second.first;
		    }
		    addMe=false;
		    break;
		}
	    if (addMe) {
		values=rctl->getTransTimes(intList[i]);
		if (values)
		    transList.push_back(pair<string,pair<unsigned,double> >( intList[i],pair<unsigned,double>(values->size(),rctl->getAverageTime(intList[i])) ) );
		else
		    transList.push_back(pair<string,pair<unsigned,double> >( intList[i],pair<unsigned,double>(0,0.0) ) );
	    }
	}
    }
}

bool RCTFileList::isSegment(const string& controller) {
    for (unsigned i=0;i<entries;i++)
	if (trees[i]->isSegment(controller)) return true;
    return false;
}

bool strcmp(const char* a,const char* b,unsigned len) {
    unsigned i=0;
    while( i<len && a[i]!=0 && b[i]!=0 ) {
        unsigned t = i;
	if ( a[t]!=b[i++] )
	    return false;
    }
    if (i<len) return false;
    return true;
}

class HasTransLog : public TNamed{
public:
    RCTransLog* rctl;
    HasTransLog(RCTransLog* _rctl,const char* fullName) : TNamed(fullName,""),rctl(_rctl) {}
};

void RCTFileList::mergeFiles(const char* fileName, bool force) {
    TIter next(this);
    RCTFile* f=static_cast<RCTFile*>(next());
    TString partition(f->GetPartition());
    while ( (f=static_cast<RCTFile*>(next())) ) {
	if (strcmp(partition.Data(),f->GetPartition())!=0) {
	    cout << "WARNING: merging files from different partitions" << endl;
	    if (!force) {
		cout << "Canceled merging." << endl;
		return;
	    }
	    break;
	}
    }

    const char* createOp=force ? "RECREATE" : "CREATE";
    TFile newFile(fileName,createOp);
    if (!newFile.IsOpen()) {
	cerr << "ERROR: Could not create the file " << fileName << " it seems, it already exists. Canceled merging." << endl;
	return;
    }
    newFile.WriteObject(&partition,"partition");
    TString time(OWLTime().c_str());
    newFile.WriteObject(&time,"timestamp");

    THashList hashList;
    next.Reset();
    // fill in the first file first
    f=static_cast<RCTFile*>(next());
    TKey *key;
    RCTimeSequence* timeSeq=NULL;
    if (f->getRootTimes()) {
	timeSeq=new RCTimeSequence(*(f->getRootTimes()));
    }
    TIter firstrun(f->mFile->GetListOfKeys());firstrun();firstrun();
    while ((key = (TKey *) firstrun())) {

	if ( strcmp(key->GetName(),"RootTiming")==0 ) {
	    key= (TKey*) firstrun();
	}

	RCTransLog* rctl = static_cast<RCTransLog*>(key->ReadObjectAny(TClass::GetClass("RCTransLog")));
	hashList.Add(new HasTransLog(rctl,key->GetName()));
    }
    int count=1;
    while ( (f=static_cast<RCTFile*>(next())) ) {

	if (f->getRootTimes()) {
	    if (timeSeq) {
		pair< const std::list<string>* ,const std::list<string>* > seqs=f->getRootTimes()->getSequence();
		std::list<string>::const_iterator nameIt,timeIt;
		timeIt=seqs.second->begin();
		for (nameIt=seqs.first->begin();nameIt!=seqs.first->end();nameIt++) {
		    timeSeq->recordValue( (*nameIt),(*timeIt) );
		    timeIt++;
		}

	    } else
		timeSeq=new RCTimeSequence(*(f->getRootTimes()));
	}
	
	TIter keynext(f->mFile->GetListOfKeys());keynext();keynext();
	while ((key = (TKey *) keynext())) {
	    
	    if ( strcmp(key->GetName(),"RootTiming")==0 ) {
		key= (TKey*) keynext();
	    }

	    RCTransLog* rctl = static_cast<RCTransLog*>(key->ReadObjectAny(TClass::GetClass("RCTransLog")));
	    TObject* ht=hashList.FindObject(key->GetName());
	    if (ht) {
		static_cast<HasTransLog*>(ht)->rctl->appendValues(rctl);
	    } else {
		const char* parent=f->getTree()->getFullName(f->getTree()->getParent(rctl->getName())).c_str();
		unsigned len=strlen(parent);
		if (!(ht=hashList.FindObject(parent))) {
		    cerr << "ERROR: could not find a parent, bad tree structure in one of the files. Canceled merging." << endl;
		    return;
		}
		while ( (ht=hashList.After(ht)) && strcmp(ht->GetName(),parent,len) );
		if (!ht)
		    hashList.Add(new HasTransLog(rctl,key->GetName()));
		else
		    hashList.AddBefore(ht,new HasTransLog(rctl,key->GetName()));
	    }
	}
	count++;
    }

    if (timeSeq)
	newFile.WriteObject(timeSeq,"RootTiming");

    TIter hashIter(&hashList);
    while ( HasTransLog* ht=static_cast<HasTransLog*>(hashIter()) )
	newFile.WriteObject(ht->rctl,ht->GetName());

    newFile.Close();

    cout << "Successfully merged "<< count << " files to '" << fileName << "'." << endl;

    if (timeSeq) delete timeSeq;
}


bool RCTFileList::removeFileAt(int curFile) {
    RCTFile* f=static_cast<RCTFile*>(RemoveAt(curFile));
    if (!f) {
	cerr << "ERROR: Could not find the file to close!" << endl;
	return false;
    }
    delete f;
    f=0;
    return true;
}

void RCTFileList::addFileAt(int curFile,RCTFile* newFile) {
    AddAt(newFile,curFile);
    trees[curFile]=newFile->getTree();
}

TFile* RCTFileList::getFileForUpdate(int curFile) {
    RCTFile* f=static_cast<RCTFile*>(At(curFile));
    return f->mFile;
}

