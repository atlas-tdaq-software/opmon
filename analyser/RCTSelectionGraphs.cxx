/** ---------------------------------------------------
 *  RCTSelectionGraphs.cxx - implementation of classes
 *  - RCTSelectionGraphList
 *  - RCTSelectionHistogram
 *  - RCTSelectionDiagram
 *  - RCTSelectionComparison
 *  author: Herbert Kaiser, Sep 2006
 *  ---------------------------------------------------
 */

#include "TH1.h"
#include "TGraph.h"
#include "TVirtualPad.h"
#include "TLegend.h"
#include "THashList.h"
#include "TBrowser.h"
#include "RCTSelectionGraphs.h"
#include "RCTSelection.h"
#include "RCTGraphs.h"
		    
#include <iostream>
using std::cout; using std::endl;

ClassImp(RCTSelectionGraphList)

RCTSelectionGraphList::RCTSelectionGraphList(const string& selName, THashList* itemList,RCTFilesInformation* fInfo) :
    TNamed("Diagrams","Selection Diagrams"),name(selName),items(itemList),filesInfo(fInfo),refresh(true)
{
    histogram=new RCTSelectionHistogram("Histogram", "Histogram filled with the values from each recorded transition in this selection",name,items,false);
    avg_histogram=new RCTSelectionHistogram("Average Histogram","Histogram filled with average values of the selected items",name,items,true);
    ctrl_diagram=new RCTSelectionDiagram("Controller Diagram","Diagram with the average values of the selection, labeled with controllers",name,items,true);
    trans_diagram=new RCTSelectionDiagram("Transition Diagram","Diagram with the average values of the selection, labeled with transitions",name,items,false);
    graph=new RCTSelectionGraph("Selected Scatter Plots","All items together in one diagram, each item has one graph line per file",name,items,fInfo);
    merged_graph=new RCTSelectionGraph("Merged Scatter Plots","Same as Selected Scatter Plots, but not a seperate line for each file",name,items,NULL);

    compareList.SetOwner(true);
}

RCTSelectionGraphList::~RCTSelectionGraphList() {
    delete histogram;
    delete avg_histogram;
    delete ctrl_diagram;
    delete trans_diagram;
    delete graph;
    delete merged_graph;
    compareList.Delete();
}

void RCTSelectionGraphList::Browse(TBrowser *b) {
    if (refresh) {
	compareList.Delete();
	vector<string> labels;
	TIter firstrun(items);
	while ( RCTSelectionItem* item=static_cast<RCTSelectionItem*>(firstrun()) ) {
	    bool addMe=true;
	    const string& flabel=item->getController();
	    unsigned labelpos=0;
	    while (labelpos<labels.size()) {
		if (labels[labelpos]==flabel) {
		    addMe=false;
		    break;
		}
		labelpos++;
	    }
	    if (addMe) {
		labels.push_back(flabel);
		compareList.Add(new RCTSelectionComparison(flabel.c_str(),"Compare the values from the different files of this controller's transitions",name+" - "+flabel,flabel,items,filesInfo));
	    }
	}
	refresh=false;
    }
    b->RecursiveRemove(histogram);
    b->RecursiveRemove(avg_histogram);
    b->RecursiveRemove(ctrl_diagram);
    b->RecursiveRemove(trans_diagram);
    b->RecursiveRemove(graph);
    b->RecursiveRemove(merged_graph);
    b->Add(histogram,histogram->GetName());
    b->Add(avg_histogram,avg_histogram->GetName());
    b->Add(ctrl_diagram,ctrl_diagram->GetName());
    b->Add(trans_diagram,trans_diagram->GetName());
    b->Add(graph,graph->GetName());
    b->Add(merged_graph,merged_graph->GetName());
    TIter next(&compareList);
    while ( TObject* child = next() ) {
	b->RecursiveRemove(child);
	b->Add(child,child->GetName());
    }
}

void RCTSelectionGraphList::requestRefresh() {
    refresh=true;
    histogram->requestRefresh();
    avg_histogram->requestRefresh();
    ctrl_diagram->requestRefresh();
    trans_diagram->requestRefresh();
    graph->requestRefresh();
    merged_graph->requestRefresh();
    TIter next(&compareList);
    while ( RCTSelectionComparison* child = static_cast<RCTSelectionComparison*>(next()) ) {
	child->requestRefresh();
    }
}


ClassImp(RCTSelectionHistogram)

RCTSelectionHistogram::~RCTSelectionHistogram() {
    delete histo;
}

void RCTSelectionHistogram::Browse(TBrowser *b) {
    if (histo || refresh) {
	delete histo;
	histo=NULL;
    }
    if (!histo) {
	histo=new TH1D((histoTitle+": "+GetName()).c_str(),histoTitle.c_str(),HISTOGRAM_BIN_NUMBER,0,0);
	TIter next(items);
	while ( RCTSelectionItem* item=static_cast<RCTSelectionItem*>(next()) ) {
	    if (avg)
		histo->Fill(item->getAverage());
	    else {
		const vector<double>* values=item->getValues();
		for (unsigned i=0;i<values->size();i++)
		    histo->Fill( (*values)[i] );
	    }
	}
	histo->GetXaxis()->SetTitle("time [millisec]");
	refresh=false;
    }

    if (gPad) gPad->Clear();
    histo->Draw();
    gPad->Update();
}


ClassImp(RCTSelectionDiagram)

RCTSelectionDiagram::~RCTSelectionDiagram() {
    for (unsigned i=0;i<histos.size();i++) {
	delete histos[i];
    }
}

void RCTSelectionDiagram::Browse(TBrowser *b) {
    if (histos.size()>0 || refresh) {
	for (unsigned i=0;i<histos.size();i++) {
	    delete histos[i]; histos[i]=NULL;
	}
	histos.clear();
    }
    if (histos.size()==0) {
	maxValue=0;
	minValue=-1;
	unsigned color=1;
	if (legend) delete legend;
	legend= new TLegend(0.8,0.7,0.95,0.95);

	vector<string> labels;
	vector<string> legendNames;
	vector< vector<double>* > matrix;
	TIter firstrun(items);
	while ( RCTSelectionItem* item=static_cast<RCTSelectionItem*>(firstrun()) ) {
	    bool addMe=true;
	    const string& flabel=useCtrl ? item->getController() : item->getTransition();
	    unsigned labelpos=0;
	    while (labelpos<labels.size()) {
		if (labels[labelpos]==flabel) {
		    addMe=false;
		    break;
		}
		labelpos++;
	    }
	    if (addMe)
		labels.push_back(flabel);
	    addMe=true;
	    const string& flegend=useCtrl ? item->getTransition() : item->getController();
	    for (unsigned i=0;i<legendNames.size();i++) {
		if (legendNames[i]==flegend) {
		    if (matrix[i]->size()<labelpos) matrix[i]->resize(labelpos+100,0.0);
		    (*matrix[i])[labelpos]=item->getAverage();
		    addMe=false;
		    break;
		}
	    }
	    if (addMe) {
		legendNames.push_back(flegend);
		vector<double>* newLabelLine=new vector<double>(labelpos+100,0.0);
		(*newLabelLine)[labelpos]=item->getAverage();
		matrix.push_back(newLabelLine);
	    }
	}

	double barWidth=((double)1/matrix.size());
	char numid[5];

	for (unsigned i=0;i<matrix.size();i++) {
	    sprintf(numid," %u",i);
	    TH1D* histo=new TH1D((diaTitle+": "+GetName()+numid).c_str(),diaTitle.c_str(),labels.size(),0,0);
	    for (unsigned j=0;j<labels.size();j++) {
		histo->Fill(labels[j].c_str(),(*matrix[i])[j]);
	    }
	    histo->SetBarWidth(barWidth);
	    histo->SetBarOffset(i*barWidth);
	    if (color==8) color+=3;
	    if (color==12) color++;
	    if (color==18) color+=2;
	    if (color==50) color=1;
	    histo->SetFillColor(color);
	    histo->SetStats(kFALSE);
	    histo->GetYaxis()->SetTitle("time [millisec]");
	    legend->AddEntry(histo,legendNames[i].c_str(),"F");
	    histos.push_back(histo);
	    delete matrix[i];
	    if (histo->GetMaximum()>maxValue)
		maxValue=histo->GetMaximum();
	    if (histo->GetMinimum()<minValue || minValue==-1)
		minValue=histo->GetMinimum();
	    color++;
	}
	refresh=false;
    }

    if (gPad) gPad->Clear();

    if (histos.size()==0) return;
    if (gPad) gPad->Clear();
    double tenthDiff=(maxValue-minValue)*0.1;
    histos[0]->SetMaximum(maxValue+tenthDiff);
    histos[0]->SetMinimum( ((minValue-tenthDiff)>0) ? (minValue-tenthDiff) : 0 );
    histos[0]->Draw("barhist");
    for (unsigned i=1;i<histos.size();i++) {
	histos[i]->Draw("barhist,same");
    }
    legend->Draw();
    gPad->Update();
}


ClassImp(RCTSelectionComparison)

RCTSelectionComparison::~RCTSelectionComparison() {
    for (unsigned i=0;i<histos.size();i++) {
	delete histos[i];
    }
}

double buildAverage(const vector<double>* values,unsigned pos,unsigned len) {
    if (len==0) return 0.0;
    double avg=0.0;
    for (unsigned i=0;i<len;i++) {
	avg+=(*values)[pos+i];
    }
    avg=avg/((double)len);
    return avg;
}

void RCTSelectionComparison::Browse(TBrowser *b) {
    if (histos.size()>0 || refresh) {
	for (unsigned i=0;i<histos.size();i++) {
	    delete histos[i]; histos[i]=NULL;
	}
	histos.clear();
    }
    if (histos.size()==0) {
	maxValue=0;
	minValue=-1;
	unsigned color=1;
	if (legend) delete legend;
	legend= new TLegend(0.8,0.7,0.95,0.95);

	vector<string> legendNames;
	vector<RCTSelectionItem* > itemsByLegends;
	TIter next(items);
	while ( RCTSelectionItem* item=static_cast<RCTSelectionItem*>(next()) ) {
	    if (item->getController()!=controller)
		continue;
	    legendNames.push_back(item->getTransition());
	    itemsByLegends.push_back(item);
	}

	double barWidth=((double)1/itemsByLegends.size());
	char numid[5];

	for (unsigned i=0;i<itemsByLegends.size();i++) {
	    const vector<int>* distri=itemsByLegends[i]->getDistribution();
	    const vector<double>* values=itemsByLegends[i]->getValues();
	    sprintf(numid," %u",i);
	    TH1D* histo=new TH1D((diaTitle+": "+GetName()+numid).c_str(),diaTitle.c_str(),distri->size(),0,0);
	    unsigned pos=0;
	    for (unsigned j=0;j<distri->size();j++) {
		histo->Fill((*filesInfo->getFileNames())[j].c_str(),buildAverage(values,pos,(*distri)[j]));
		pos+=(*distri)[j];
	    }
	    histo->SetBarWidth(barWidth);
	    histo->SetBarOffset(i*barWidth);
	    if (color==8) color+=3;
	    if (color==12) color++;
	    if (color==18) color+=2;
	    if (color==50) color=1;
	    histo->SetFillColor(i+color);
	    histo->SetStats(kFALSE);
	    histo->GetYaxis()->SetTitle("time [millisec]");
	    legend->AddEntry(histo,legendNames[i].c_str(),"F");
	    histos.push_back(histo);
	    if (histo->GetMaximum()>maxValue)
		maxValue=histo->GetMaximum();
	    if (histo->GetMinimum()<minValue || minValue==-1)
		minValue=histo->GetMinimum();
	    color++;
	}
	refresh=false;
    }

    if (gPad) gPad->Clear();

    if (histos.size()==0) return;
    if (gPad) gPad->Clear();
    double tenthDiff=(maxValue-minValue)*0.1;
    histos[0]->SetMaximum(maxValue+tenthDiff);
    histos[0]->SetMinimum( ((minValue-tenthDiff)>0) ? (minValue-tenthDiff) : 0 );
    histos[0]->Draw("barhist");
    for (unsigned i=1;i<histos.size();i++) {
	histos[i]->Draw("barhist,same");
    }
    legend->Draw();
    gPad->Update();
}


ClassImp(RCTSelectionGraph)

RCTSelectionGraph::~RCTSelectionGraph() {
    for (unsigned i=0;i<graphs.size();i++) {
	delete graphs[i];
    }
}

void RCTSelectionGraph::Browse(TBrowser *b) {
    if (graphs.size()>0 || refresh) {
	for (unsigned i=0;i<graphs.size();i++) {
	    delete graphs[i]; graphs[i]=NULL;
	}
	graphs.clear();
    }
    if (graphs.size()==0) {
	unsigned maxNumber=0;
	maxValue=0;
	minValue=-1;
	unsigned color=1;
	unsigned marker=20;
	if (legend) delete legend;
	legend= new TLegend(0.8,0.7,0.95,0.95);
	unsigned i=0;

	TIter next(items);
	RCTSelectionItem* item=static_cast<RCTSelectionItem*>(next());
	while ( item ) {
	    const vector<double>* vals=item->getValues();
	    double* values;
	    unsigned len;
	    const vector<int>* distri=item->getDistribution();
	    unsigned j=0;
	    unsigned pos=0;
	    string addToName;
	    while ( j < distri->size() ) {

		if (filesInfo) {
		    addToName=" - ";
		    addToName.append( (*filesInfo->getFileNames())[j] );
		    len=(*distri)[j];
		    values=new double[len+1];
		    for (unsigned k=0;k<len;k++)
			values[k]=(*vals)[k+pos];
		    pos+=len;
		    j++;
		} else {
		    len=vals->size();
		    values=new double[len+1];
		    for (unsigned k=0;k<len;k++)
			values[k]=(*vals)[k];
		    values[len]=0;

		    j=distri->size();
		}
		if (len==0) {
		    delete[] values;
		    cout << "No values for '"<< item->getController() <<": "<<item->getTransition()<<addToName << "' available, will be skipped." << endl;
		    continue;
		}
	    
		double* xvals=RCTScatterGraph::countUp(len);

		TGraph* g = new TGraph(len,xvals,values);
		delete[] xvals;
		delete[] values;

		g->SetNameTitle((item->getController()+": "+item->getTransition()+addToName).c_str(),diaTitle.c_str());
		if (marker==31) marker=20;
		g->SetMarkerStyle(marker);
		if (color==8) color+=3;
		if (color==12) color++;
		if (color==18) color+=2;
		if (color==50) color=1;
		g->SetMarkerColor(color);
		g->SetLineColor(color);
		g->GetYaxis()->SetTitle("time [millisec]");
		g->GetXaxis()->SetTitle("execution number");
		legend->AddEntry(g,g->GetName(),"P");
		graphs.push_back(g);

		if (g->GetYaxis()->GetXmax()>maxValue)
		    maxValue=g->GetYaxis()->GetXmax();
		if (g->GetYaxis()->GetXmin()<minValue || minValue==-1)
		    minValue=g->GetYaxis()->GetXmin();

		if (len > maxNumber) { 
		    maxNumber=len;
		    biggestGraph=g;
		}
		color++;
		marker++;
		i++;
	    }
	    item=static_cast<RCTSelectionItem*>(next());
	}
	refresh=false;
    }

    if (gPad) gPad->Clear();

    biggestGraph->SetMaximum(maxValue);
    biggestGraph->SetMinimum(minValue);
    biggestGraph->Draw("AP");
    for (unsigned i=0;i<graphs.size();i++) {
	graphs[i]->Draw("LP");
    }
    legend->Draw();
    gPad->Update();

}
