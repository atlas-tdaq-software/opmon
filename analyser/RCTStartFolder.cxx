/** -------------------------------------------------------------
 *  RCTStartFolder.cxx - implementation of class RCTStartFolder
 *  author: Herbert Kaiser, Sep 2006
 *  -------------------------------------------------------------
 */

#include "TBrowser.h"
#include "RCTStartFolder.h"
#include "RCTFile.h"
#include "RCTFileList.h"
#include "RCTController.h"
#include "RCTSelectionList.h"

ClassImp(RCTStartFolder)
  

RCTStartFolder::RCTStartFolder(const vector<string>& fileList) :
    TNamed("RCT","RCT start folder")
{
    list=new RCTFileList(fileList);
    rootCtrl=new RCTController(ROOT_CTRL,list);
    selection=new RCTSelectionList(list);
}

RCTStartFolder::~RCTStartFolder() {
    delete rootCtrl;
    delete selection;
    delete list;
}


void RCTStartFolder::Browse(TBrowser *b)
{
    b->RecursiveRemove(rootCtrl);
    b->RecursiveRemove(selection);
    b->RecursiveRemove(list);
    b->Add( rootCtrl, ROOT_CTRL );
    b->Add( selection, "Selections");
    b->Add( list, "Files" );
}

