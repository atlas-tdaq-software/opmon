/** -----------------------------------------------------------------------
 *  RCTDisplayGuiFactory.cxx - implementation of class RCTDisplayGuiFactory
 *  author: Herbert Kaiser, Sep 2006 based on OHDisplayGuiFactory from OH
 *  -----------------------------------------------------------------------
 */

#include "RCTBrowser.h"
#include "RCTSelectionList.h"
#include "RCTDisplayGuiFactory.h"


ClassImp(RCTDisplayGuiFactory)

RCTDisplayGuiFactory::RCTDisplayGuiFactory( RCTSelectionList* sel )
  : TRootGuiFactory( "DisplayFactory", "Display Factory" ), selList(sel)
{
    old_factory = gGuiFactory;
    gGuiFactory = this;
}

RCTDisplayGuiFactory::~RCTDisplayGuiFactory( )
{
    gGuiFactory = old_factory;
}

TBrowserImp *RCTDisplayGuiFactory::CreateBrowserImp(TBrowser *b, const char *title, UInt_t width, UInt_t height, const Option_t * )
{
    return new RCTBrowser( b, title, width, height, selList );
}

TBrowserImp *RCTDisplayGuiFactory::CreateBrowserImp(TBrowser *b, const char *title,Int_t x, Int_t y, UInt_t width, UInt_t height, const Option_t*)
{
    return new RCTBrowser( b, title, x, y, width, height, selList );
}

