/** -----------------------------------------------------
 *  RCTGraphs.cxx - implementation of classes
 *  - RCTScatterGraph
 *  - RCTCombinedScatterGraph
 *  - RCTAveragesDiagram
 *  - RCTCombinedAveragesDiagram
 *  author: Herbert Kaiser, Sep 2006
 *  -----------------------------------------------------
 */

#include "opmon/RCTransLog.h"
#include "TCollection.h"
#include "RCTFile.h"
#include "TBrowser.h"
#include "TVirtualPad.h"
#include "TGClient.h"
#include "TGMsgBox.h"
#include "RCTFileList.h"
#include "RCTGraphs.h"


ClassImp(RCTScatterGraph)

RCTScatterGraph::~RCTScatterGraph() {
    for (unsigned i=0;i<graphs.size();i++) {
	delete graphs[i];
    }
}

void RCTScatterGraph::Browse(TBrowser *b) {
    if (graphs.size()==0) {
	unsigned maxNumber=0;
	maxValue=0;
	minValue=-1;
	unsigned color=1;
	unsigned marker=20;
	if (legend) delete legend;
	legend= new TLegend(0.8,0.7,0.95,0.95);
	unsigned i=0;
	const string fullName=list->getFullName(controller).c_str();
	TIter next(list);
	while ( RCTFile* f=static_cast<RCTFile*>(next()) ) {
	    const RCTransLog* rctl=f->getTransLogByFull(fullName);
	    if (!rctl) continue;
	    const vector<double>* values=rctl->getTransTimes(transition);
	    if (!values) continue;

	    double* xvals=countUp(values->size());
	    TGraph* g = new TGraph(values->size(),xvals,static_cast<const double*>(&((*values)[0])));
	    delete[] xvals;
	    g->SetNameTitle(GetName(),title.c_str());
	    if (marker==31) marker=20;
	    g->SetMarkerStyle(marker);
	    if (color==8) color+=3;
	    if (color==12) color++;
	    if (color==18) color+=2;
	    if (color==50) color=1;
	    g->SetMarkerColor(color);
	    g->SetLineColor(color);
	    g->GetYaxis()->SetTitle("time [millisec]");
	    g->GetXaxis()->SetTitle("execution number");
	    legend->AddEntry(g,f->GetName(),"P");
	    graphs.push_back(g);

	    if (g->GetYaxis()->GetXmax()>maxValue)
		maxValue=g->GetYaxis()->GetXmax();
	    if (g->GetYaxis()->GetXmin()<minValue || minValue==-1)
		minValue=g->GetYaxis()->GetXmin();

	    if (values->size() > maxNumber) { 
		maxNumber=values->size();
		biggestGraph=g;
	    }

	    i++;
	    color++;
	    marker++;
	}
    }

    if (gPad) gPad->Clear();

    biggestGraph->SetMaximum(maxValue);
    biggestGraph->SetMinimum(minValue);
    biggestGraph->Draw("AP");
    for (unsigned i=0;i<graphs.size();i++) {
	graphs[i]->Draw("LP");
    }
    legend->Draw();
    gPad->Update();
    
}

double* RCTScatterGraph::countUp(unsigned max) {
    double* values= new double[max];
    for (unsigned i=0;i<max;i++)
	values[i]=(double)(i+1);
    return values;
}



ClassImp(RCTCombinedScatterGraph)

void RCTCombinedScatterGraph::Browse(TBrowser *b) {
    if (graphs.size()==0) {
	unsigned maxNumber=0;
	maxValue=0;
	minValue=-1;
	unsigned color=1;
	unsigned marker=20;
	if (legend) delete legend;
	legend= new TLegend(0.8,0.7,0.95,0.95);
	unsigned i=0;
	const string fullName=list->getFullName(controller).c_str();
	TIter next(list);
	while ( RCTFile* f=static_cast<RCTFile*>(next()) ) {
	    const RCTransLog* rctl=f->getTransLogByFull(fullName);
	    if (!rctl) continue;
	    vector<double> values;
	    if (!rctl->getCombinedTransTimes(combination,values)) continue;

	    double* xvals=countUp(values.size());
	    TGraph* g = new TGraph(values.size(),xvals,static_cast<const double*>(&(values[0])));
	    delete[] xvals;
	    g->SetNameTitle(GetName(),title.c_str());
	    if (marker==31) marker=20;
	    g->SetMarkerStyle(marker);
	    if (color==8) color+=3;
	    if (color==12) color++;
	    if (color==18) color+=2;
	    if (color==50) color=1;
	    g->SetMarkerColor(color);
	    g->SetLineColor(color);
	    g->GetYaxis()->SetTitle("time [millisec]");
	    g->GetXaxis()->SetTitle("execution number");
	    legend->AddEntry(g,f->GetName(),"P");
	    graphs.push_back(g);

	    if (g->GetYaxis()->GetXmax()>maxValue)
		maxValue=g->GetYaxis()->GetXmax();
	    if (g->GetYaxis()->GetXmin()<minValue || minValue==-1)
		minValue=g->GetYaxis()->GetXmin();

	    if (values.size() > maxNumber) { 
		maxNumber=values.size();
		biggestGraph=g;
	    }

	    color++;
	    marker++;
	    i++;
	}
    }

    if (!biggestGraph) {
	new TGMsgBox(gClient->GetRoot(),gClient->GetWindowByName("opmon_analyser"),"Information",(string("Graph '")+controller+":"+transition+"' not available from these files").c_str(),kMBIconExclamation);
	return;
    }

    if (gPad) gPad->Clear();

    biggestGraph->SetMaximum(maxValue);
    biggestGraph->SetMinimum(minValue);
    biggestGraph->Draw("AP");
    for (unsigned i=0;i<graphs.size();i++) {
	graphs[i]->Draw("LP");
    }
    legend->Draw();
    gPad->Update();
}



ClassImp(RCTAveragesDiagram)

RCTAveragesDiagram::~RCTAveragesDiagram() {
    for (unsigned i=0;i<hists.size();i++) {
	delete hists[i];
    }
}

void RCTAveragesDiagram::Browse(TBrowser *b) {
    if (hists.size()==0) {
	maxValue=0;
	minValue=-1;
	unsigned color=1;
	if (legend) delete legend;
	legend= new TLegend(0.8,0.7,0.95,0.95);

	unsigned count=0;
	while (transitions[count])
	    count++;
	double barWidth=((double)1)/list->GetEntries();
	const string fullName=list->getFullName(controller).c_str();
	unsigned fi=0;
	char numid[200];
	TIter next(list);
	while ( RCTFile* f=static_cast<RCTFile*>(next()) ) {
	    const RCTransLog* rctl=f->getTransLogByFull(fullName);
	    if (!rctl) continue;
	    sprintf(numid,"%s %u",GetName(),fi);
	    TH1D* histo=new TH1D(numid,title.c_str(),count,0,0);
	    for (unsigned i=0;i<count;i++) {
		const vector<double>* trT=rctl->getTransTimes(transitions[i]);
		if (trT)
		    histo->Fill(transitions[i],average(*trT));
		else
		    histo->Fill(transitions[i],0.0);
	    }
	    histo->SetBarWidth(barWidth);
	    histo->SetBarOffset(fi*barWidth);
	    if (color==8) color+=3;
	    if (color==12) color++;
	    if (color==18) color+=2;
	    if (color==50) color=1;
	    histo->SetFillColor(color);
	    histo->SetStats(kFALSE);
	    histo->GetYaxis()->SetTitle("time [millisec]");
	    legend->AddEntry(histo,f->GetName(),"F");
	    hists.push_back(histo);
	    if (histo->GetMaximum()>maxValue)
		maxValue=histo->GetMaximum();
	    if (histo->GetMinimum()<minValue || minValue==-1)
		minValue=histo->GetMinimum();
	    color++;
	    fi++;
	}
    }

    if (gPad) gPad->Clear();
    if (hists.size()==0) return;
    double tenthDiff=(maxValue-minValue)*0.1;
    hists[0]->SetMaximum(maxValue+tenthDiff);
    hists[0]->SetMinimum( ((minValue-tenthDiff)>0) ? (minValue-tenthDiff) : 0 );
    hists[0]->Draw("barhist");
    for (unsigned i=1;i<hists.size();i++) {
	hists[i]->Draw("barhist,same");
    }
    legend->Draw();
    gPad->Update();
}



ClassImp(RCTCombinedAveragesDiagram)

void RCTCombinedAveragesDiagram::Browse(TBrowser *b) {
    if (hists.size()==0) {
	maxValue=0;
	minValue=-1;
	unsigned color=1;
	if (legend) delete legend;
	legend= new TLegend(0.8,0.7,0.95,0.95);

	unsigned count=combs.size();
	double barWidth=((double)1)/list->GetEntries();
	const string fullName=list->getFullName(controller).c_str();
	unsigned fi=0;
	char numid[200];
	TIter next(list);
	while ( RCTFile* f=static_cast<RCTFile*>(next()) ) {
	    const RCTransLog* rctl=f->getTransLogByFull(fullName);
	    if (!rctl) continue;
	    sprintf(numid,"%s %u",GetName(),fi);
	    TH1D* histo=new TH1D(numid,title.c_str(),count,0,0);
	    for (unsigned i=0;i<count;i++) {
		vector<double> values;
		if (rctl->getCombinedTransTimes(combs[i],values) )
		    histo->Fill(combs[i][0],average(values));
		else
		    histo->Fill(combs[i][0],0.0);
	    }
	    histo->SetBarWidth(barWidth);
	    histo->SetBarOffset(fi*barWidth);
	    if (color==8) color+=3;
	    if (color==12) color++;
	    if (color==18) color+=2;
	    if (color==50) color=1;
	    histo->SetFillColor(color);
	    histo->SetStats(kFALSE);
	    histo->GetYaxis()->SetTitle("time [millisec]");
	    legend->AddEntry(histo,f->GetName(),"F");
	    hists.push_back(histo);
	    if (histo->GetMaximum()>maxValue)
		maxValue=histo->GetMaximum();
	    if (histo->GetMinimum()<minValue || minValue==-1)
		minValue=histo->GetMinimum();
	    color++;
	    fi++;
	}
    }

    if (hists.size()==0) return;

    if (gPad) gPad->Clear();

    double tenthDiff=(maxValue-minValue)*0.1;
    hists[0]->SetMaximum(maxValue+tenthDiff);
    hists[0]->SetMinimum( ((minValue-tenthDiff)>0) ? (minValue-tenthDiff) : 0 );
    hists[0]->Draw("barhist");
    for (unsigned i=1;i<hists.size();i++) {
	hists[i]->Draw("barhist,same");
    }
    legend->Draw();
    gPad->Update();
}


