/** -----------------------------------------------
 *  RCTFile.cxx - implementation of class RCTFile
 *  author: Herbert Kaiser, Sep 2006
 *  -----------------------------------------------
 */

#include "TKey.h"
#include "TFile.h"
#include "RCTFile.h"
#include "opmon/RCStringTree.h"
#include "opmon/RCTransLog.h"
#include "opmon/RCTimeSequence.h"

#include <iostream>
#include <list>

using std::cerr;
using std::endl;
using std::string;
using std::pair;


ClassImp(RCTFile)

// needed for efficient tree reading
TIter* gIterFullNameList;
const char* getNextInFullNameList() {
    TKey *key=(TKey*) (*gIterFullNameList)();
    if (key) {
	return key->GetName();
    } else return NULL;
}

RCTFile::RCTFile(const char * fName) :
    TNamed(fName,"Transition Times")
{
    mFile = new TFile(fName,"READ");
    rcst=NULL;
    rootTimes=NULL;
    reloadAll();
}

RCTFile::RCTFile (const char * fName,TFile* rootFile) :
    TNamed(fName,"Transition Times")
{
    mFile=rootFile;
    rcst=NULL;
    rootTimes=NULL;
    reloadAll();
}


RCTFile::~RCTFile() {
    mFile->Close();
    delete mFile;
    delete rcst;
    if (rootTimes) delete rootTimes;
}

void RCTFile::reloadAll() {
    if (mFile->IsZombie())
	throw RCTFileException(string("File ")+GetName()+" doesn't exist or is not a valid ROOT file.");
    TString* tstr;
    mFile->GetObject("partition",tstr);
    if (!tstr)
	throw RCTFileException(string("File ")+GetName()+ " is not a valid ROOT file from opmon_monitor.");
    partition=*tstr;
    mFile->GetObject("timestamp",tstr);
    if (!tstr)
	throw RCTFileException(string("File ")+GetName()+" is not a valid ROOT file from opmon_monitor.");
    updatetime=*tstr;

    //first find out the seperator between names in the tree structure
    TIter sepFindIt(mFile->GetListOfKeys());
    sepFindIt();
    sepFindIt();
    TKey *key=(TKey*) sepFindIt();
    string keyName;
    char nameSeperator='|';
    bool hasRootTiming=false;
    if (key) {
	keyName=key->GetName();
	if (keyName=="RootTiming") {
	    hasRootTiming=true;
	    rootTimes=static_cast<RCTimeSequence*>(mFile->GetObjectChecked("RootTiming",RCTimeSequence::Class()) );
	    key=(TKey*)sepFindIt();
	    if (!key) {
		cerr << "File "<< GetName() << " contains no controller!" << endl;
		throw RCTFileException(string("File ")+GetName()+" is not a valid ROOT file from opmon_monitor.");
	    }
	    keyName=key->GetName();
	}
    } else {
	cerr << "File "<< GetName() << " contains no controller!" << endl;
	throw RCTFileException(string("File ")+GetName()+" is not a valid ROOT file from opmon_monitor.");
	return;
    }
    if (keyName!=ROOT_CTRL) {
	cerr << "File "<< GetName() << " contains no RootController!" << endl;
	throw RCTFileException(string("File ")+GetName()+" is not a valid ROOT file from opmon_monitor.");
    }
    key=(TKey*) sepFindIt();
    if (key) {
	keyName=key->GetName();
	if (strncmp(keyName.c_str(),ROOT_CTRL,strlen(ROOT_CTRL))!=0) {
	    cerr << "File "<< GetName() << " has a bad tree structure, problems may occur!" << endl;
	} else {
	    nameSeperator=(*(keyName.c_str()+strlen(ROOT_CTRL)));
	}
    }

    gIterFullNameList=new TIter(mFile->GetListOfKeys());
    (*gIterFullNameList)();
    (*gIterFullNameList)();
    if (hasRootTiming) (*gIterFullNameList)();
    if (rcst) delete rcst;
    rcst=new RCStringTree(nameSeperator,false);
    if (!rcst->refreshTree(getNextInFullNameList)) {
	cerr << "File " << GetName() << " has has a wrong tree structure or is badly ordered. opmon_analyser could have problems analyzing this file!" << endl;
    }
}

const RCTransLog* RCTFile::getTransLogByFull(const string& fullName) {
    return static_cast<RCTransLog*>( mFile->GetObjectChecked(fullName.c_str(),RCTransLog::Class()) );
}


