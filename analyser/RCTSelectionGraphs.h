/** -----------------------------------------------------------------
 *  RCTSelectionGraphs.cxx - definition of classes:
 *
 *  - RCTSelectionGraphList
 *  the list of graphs produced from a selection
 *
 *  - RCTSelectionHistogram
 *  used to draw the histogram with values from the selection items
 *  (takes values from all files merged together)
 *
 *  - RCTSelectionDiagram
 *  bar diagram to compare controllers or transitions in between
 *  a partition (takes values from all files merged together)
 *
 *  - RCTSelectionComparison
 *  bar diagram for to compare the values from the files
 *  showing the selected transitions for one controller
 *
 *  - RCTSelectionGraph
 *  draws all the selected items as graph lines
 *
 *  author: Herbert Kaiser, Sep 2006
 *  -----------------------------------------------------------------
 */

#ifndef RCT_SELECTION_GRAPHS_H
#define RCT_SELECTION_GRAPHS_H

#define HISTOGRAM_BIN_NUMBER 500

#include <vector>
#include <string>
#include "TNamed.h"

using std::vector;
using std::string;

class TH1D;
class TGraph;
class TLegend;
class THashList;
class TBrowser;
class RCTSelectionItem;
class RCTFileList;
class RCTSelectionHistogram;
class RCTSelectionDiagram;
class RCTSelectionGraph;
class RCTFilesInformation;

class RCTSelectionGraphList: public TNamed {
protected:
    string name;
    THashList* items;
    RCTFilesInformation* filesInfo;
    bool refresh;
    RCTSelectionHistogram* histogram;
    RCTSelectionHistogram* avg_histogram;
    RCTSelectionDiagram* ctrl_diagram;
    RCTSelectionDiagram* trans_diagram;
    RCTSelectionGraph* graph;
    RCTSelectionGraph* merged_graph;
    TList compareList;

public:
    RCTSelectionGraphList(const string& name,THashList* itemList,RCTFilesInformation* fInfo);
    ~RCTSelectionGraphList();
    virtual Bool_t IsFolder() const { return kTRUE; }
    virtual void Browse(TBrowser *b);
    void requestRefresh();

    ClassDef(RCTSelectionGraphList,1) // folder with diagrams of a selection
};


class RCTSelectionHistogram: public TNamed {
protected:
    string histoTitle;
    THashList* items;
    bool avg;
    TH1D* histo;
    bool refresh;
public:
    RCTSelectionHistogram(const char* name, const char* title,const string& htitle,THashList* selItems,bool useAvg) :
	TNamed(name,title),histoTitle(htitle),items(selItems),avg(useAvg),histo(NULL),refresh(true) {}
    virtual ~RCTSelectionHistogram();

    Bool_t IsFolder() const { return kFALSE; }
    void requestRefresh() { refresh=true; }
    virtual void Browse(TBrowser *b);

    ClassDef(RCTSelectionHistogram,1) // histogram made of a selection
};


class RCTSelectionDiagram: public TNamed {
protected:
    string diaTitle;
    THashList* items;
    bool useCtrl;
    vector<TH1D*> histos;
    bool refresh;
    TLegend *legend;
    double minValue;
    double maxValue;

public:
    RCTSelectionDiagram(const char* name, const char* title,const string& dtitle,THashList* selItems,bool ctrl) :
	TNamed(name,title),diaTitle(dtitle),items(selItems),useCtrl(ctrl),histos(0),refresh(true),legend(NULL) {}
    virtual ~RCTSelectionDiagram();

    Bool_t IsFolder() const { return kFALSE; }
    void requestRefresh() { refresh=true; }
    virtual void Browse(TBrowser *b);

    ClassDef(RCTSelectionDiagram,1) // diagram made of a selection
};


class RCTSelectionComparison: public TNamed {
protected:
    string diaTitle;
    string controller;
    THashList* items;
    RCTFilesInformation* filesInfo;
    vector<TH1D*> histos;
    bool refresh;
    TLegend *legend;
    double minValue;
    double maxValue;

public:
    RCTSelectionComparison(const char* name, const char* title,const string& dtitle,const string& ctrl,THashList* selItems,RCTFilesInformation* fInfo) :
	TNamed(name,title),diaTitle(dtitle),controller(ctrl),items(selItems),filesInfo(fInfo),histos(0),refresh(true),legend(NULL) {}
    virtual ~RCTSelectionComparison();

    Bool_t IsFolder() const { return kFALSE; }
    void requestRefresh() { refresh=true; }
    virtual void Browse(TBrowser *b);

    ClassDef(RCTSelectionComparison,1) // diagram for comparison between the files
};

class RCTSelectionGraph: public TNamed {
protected:
    string diaTitle;
    THashList* items;
    vector<TGraph*> graphs;
    bool refresh;
    TLegend *legend;
    RCTFilesInformation* filesInfo;
    TGraph* biggestGraph; //!
    double minValue;
    double maxValue;

public:
    RCTSelectionGraph(const char* name, const char* title,const string& dtitle,THashList* selItems,RCTFilesInformation* fInfo=NULL) :
	TNamed(name,title),diaTitle(dtitle),items(selItems),graphs(0),refresh(true),legend(NULL),filesInfo(fInfo),biggestGraph(NULL) {}
    virtual ~RCTSelectionGraph();

    Bool_t IsFolder() const { return kFALSE; }
    void requestRefresh() { refresh=true; }
    virtual void Browse(TBrowser *b);

    ClassDef(RCTSelectionGraph,1) // graphs made of a selection
};

#endif //  RCT_SELECTION_GRAPHS_H

