/** -----------------------------------------------------
 *  RCTFileList.h - class RCTFileList
 *
 *  contains interface to and list of RCTFile objects
 *
 *  Constructor throws RCTFileException if there are not
 *  enough valid files (at least one is needed) are given
 *  in the fileList argument.
 *  See mergeFiles() for how the file merging is done.
 *
 *  author: Herbert Kaiser, Sep 2006
 *  -----------------------------------------------------
 */

#ifndef _RCT_FILE_LIST_H_
#define _RCT_FILE_LIST_H_

#include <string>
#include <vector>
#include "TObjArray.h"

using std::vector;
using std::string;
using std::pair;

class TBrowser;
class RCStringTree;
class RCTransLog;
class RCTFile;
class TFile;
typedef vector<const RCStringTree*> RCStringTreeVector;

class RCTFileList: public TObjArray {
    private:
	unsigned entries;  // number of entries
	RCStringTreeVector trees; //! the trees
    public:
    RCTFileList(const vector<string>& fileList);
    ~RCTFileList();
    virtual Bool_t IsFolder() const { return kTRUE; }
    virtual void Browse(TBrowser *b);
    unsigned getEntries() const {return entries; }
    const RCStringTreeVector& getTrees() {return trees; }
    bool controllerExistsInAll(const string& ctrl,bool startSecond=false);
    string getFullName(const string& ctrl);
    void getAllTransitions(const string& controller, vector< pair<string,pair<unsigned,double> > >& transList);
    bool isSegment(const string& controller);
    void mergeFiles(const char* fileName,bool force=false);
    bool removeFileAt(int curFile);
    void addFileAt(int curFile,RCTFile* newFile);
    TFile* getFileForUpdate(int curFile);

    ClassDef(RCTFileList,1) // Start Folder for the Browser
};

#endif // _RCT_FILE_LIST_H_

