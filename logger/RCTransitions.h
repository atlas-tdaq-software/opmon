/*
 * RCTransitions.h
 *
 *  Created on: Dec 1", 1}, 2020
 *      Author: kolos
 */

#ifndef LOGGER_RCTRANSITIONS_H_
#define LOGGER_RCTRANSITIONS_H_

#include <map>
#include <string>
#include <vector>

extern const std::map<std::string, int> RCTransitionNames;

struct RCTransitionCompare {
    bool operator()(const std::string & a, const std::string & b) const {
        auto ia = RCTransitionNames.find(a);
        auto ib = RCTransitionNames.find(b);
        return (ia != RCTransitionNames.end() && ib != RCTransitionNames.end())
                ? ia->second < ib->second
                : a < b;
    }
};

typedef std::map<std::string, std::vector<double>, RCTransitionCompare> RCTransitions;

#endif /* LOGGER_RCTRANSITIONS_H_ */
