/*
 * Logger.h
 *
 *  Created on: Nov 9, 2020
 *      Author: kolos
 */

#ifndef OPMON_LOGGER_H_
#define OPMON_LOGGER_H_

#include <memory>
#include <mutex>
#include <string>
#include <unordered_map>

#include <dal/Segment.h>

#include <is/inforeceiver.h>

#include "RCObject.h"

namespace opmon {
    class Logger {
    public:
        Logger(const std::string & partition, const std::string & log_path);

        ~Logger();

        std::string save();

    private:
        typedef std::unordered_map<std::string, std::shared_ptr<RCObject>> Applications;

        void init();

        void parseSegment(const daq::core::Segment * seg,
                std::shared_ptr<RCObject> & app, Applications & apps);

        void callback(const ISCallbackInfo * isc);

        static void configCallback(const std::vector<ConfigurationChange*>&, void*);

    private:
        std::shared_ptr<RCObject> m_root_controller;
        Applications m_applications;
        ISInfoReceiver m_receiver;
        std::string m_log_path;
        Configuration m_config;
        Configuration::CallbackId m_subscription;
    };
}

#endif /* OPMON_LOGGER_H_ */
