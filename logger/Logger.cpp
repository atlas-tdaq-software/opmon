/*
 * Logger.cpp
 *
 *  Created on: Nov 9, 2020
 *      Author: kolos
 */
#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <sstream>

#include <TObjArray.h>
#include <TFile.h>

#include <config/Configuration.h>
#include <config/Errors.h>
#include <dal/OnlineSegment.h>
#include <dal/Partition.h>
#include <dal/BaseApplication.h>
#include <dal/ResourceBase.h>
#include <dal/RunControlApplicationBase.h>
#include <dal/RunControlTemplateApplication.h>
#include <dal/Segment.h>
#include <dal/util.h>
#include <ipc/partition.h>
#include <ipc/signal.h>
#include <rc/RCStateInfo.h>

#include "Logger.h"

using namespace opmon;

namespace {
    std::string from_time(const std::string & path, const std::string & partition) {
        auto time = std::time(nullptr);
        std::stringstream ss;
        ss << std::put_time(std::localtime(&time), "%Y_%b_%d_%H_%M_%S");
        auto file = ss.str();

        std::string slash = path.empty() || path.back() == '/' ? "" : "/";
        return path + slash + partition + "_" + file + ".root";
    }
}

Logger::Logger(const std::string &partition, const std::string &log_path) :
    m_receiver(IPCPartition(partition)),
    m_log_path(log_path),
    m_config("rdbconfig:" + partition + "::RDB")
{
    {
        std::string n = from_time(log_path, partition) + ".tmp";
        TFile file(n.c_str(), "recreate");

        if (file.IsZombie()) {
            throw daq::ipc::Exception(ERS_HERE,
                    "TFile::Open() fails to open a new file in '" + log_path + "' directory");
        }
        file.Close();
        std::remove(n.c_str());
    }

    const daq::core::Partition *p =
            daq::core::get_partition(m_config, partition);
    if (p) {
        m_config.register_converter(new daq::core::SubstituteVariables(*p));

        const daq::core::Segment * setup = p->get_segment(
                p->get_OnlineInfrastructure()->UID());

        parseSegment(setup, m_root_controller, m_applications);
    } else {
        std::string m("Partition '" + partition + "' not found in the database");
        throw daq::config::Generic(ERS_HERE, m.c_str());
    }

    m_subscription = m_config.subscribe(ConfigurationSubscriptionCriteria(),
                Logger::configCallback, this);

    m_receiver.subscribe("RunCtrl", RCStateInfo::type(), &Logger::callback, this);
}

Logger::~Logger() {
}

std::string Logger::save() {
    try {
        m_receiver.unsubscribe("RunCtrl", RCStateInfo::type());
    } catch(daq::is::Exception & ex) {
        ers::log(ex);
    }

    std::string file_name = from_time(m_log_path, m_receiver.partition().name());

    RCObject::ChildrenTransitions  dummy;

    ERS_LOG("Analysing statistics");
    TObject * obj = m_root_controller->finalize(dummy);

    if (obj && obj->IsA() == TList::Class() && ((TList*)obj)->GetSize() > 1) {
        TFile file(file_name.c_str(), "recreate");
        ERS_LOG("Saving data to '" << file_name << "' file");
        obj->Write("", TObject::kSingleKey);
        file.Close();
    } else {
        std::cout << "No RC transitions were recorded, "
                << "no file will be produced" << std::endl;
        return "";
    }

    ERS_LOG("ROOT file '" << file_name << "' has been saved");

    delete obj;

    return file_name;
}

void Logger::configCallback(const std::vector<ConfigurationChange*>&, void* ptr) {
    ERS_LOG("Database modification notification has been received");

    Logger * logger = (Logger*)ptr;

    try {
        const daq::core::Partition *p = daq::core::get_partition(logger->m_config,
                        logger->m_receiver.partition().name());
        if (p) {
            const daq::core::Segment * setup = p->get_segment(
                    p->get_OnlineInfrastructure()->UID());

            std::shared_ptr<RCObject> root_controller;
            Applications apps;
            logger->parseSegment(setup, root_controller, apps);

            auto keys_cmp = [] (auto a, auto b)
                               { return a.first == b.first; };

            if (logger->m_applications.size() == apps.size()) {
                if (std::equal(
                        logger->m_applications.begin(),
                        logger->m_applications.end(),
                        apps.begin(), keys_cmp)) {
                    ERS_LOG("Run Control tree is not affected by this change, "
                            "database will not be reloaded");
                    return;
                }
            }
        }
    }
    catch (ers::Issue & ex) {
        ers::error(ex);
        return;
    }

    ERS_LOG("Run Control tree has been modified, database will be reloaded");
    daq::ipc::signal::raise(SIGUSR1);
}

void Logger::parseSegment(const daq::core::Segment * segment,
        std::shared_ptr<RCObject> & parent, Applications & apps) {
    const daq::core::BaseApplication * rc = segment->get_controller();
    std::string id = rc->UID();

    parent.reset(new RCObject(id));
    apps.insert(std::make_pair(id, parent));

    for (const auto &a : segment->get_applications()) {
        if (a->cast<daq::core::RunControlApplicationBase>()) {
            std::shared_ptr<RCObject> app(new RCObject(a->UID()));
            parent->addChild(app);
            apps.insert(std::make_pair(a->UID(), app));
        }
    }

    for (const auto &s : segment->get_nested_segments()) {
        if (s->is_disabled()) {
            continue;
        }
        std::shared_ptr<RCObject> app;
        parseSegment(s, app, apps);
        parent->addChild(app);
    }
}

void Logger::callback(const ISCallbackInfo * isc) {

    std::string name = isc->name();
    name.erase(0, 8); // delete 'RunCtrl.' at the beginning

    Applications::const_iterator it = m_applications.find(name);
    if (it == m_applications.end()) {
        return;
    }

    RCStateInfo info;
    isc->value(info);

    it->second->update(info);
}


