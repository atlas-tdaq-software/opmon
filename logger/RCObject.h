/*
 * RCObject.h
 *
 *  Created on: Nov 9, 2020
 *      Author: kolos
 */

#ifndef OPMON_RCOBJECT_H_
#define OPMON_RCOBJECT_H_

#include <logger/RCTransitions.h>
#include <list>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <tuple>
#include <vector>


class RCStateInfo;
class TObject;

class RCObject {
public:
    struct Application {
        Application(const std::string & name, double av, double min, double max, double err) :
            m_name(name), m_average(av), m_min(min), m_max(max), m_error(err)
        {}

        const std::string m_name;
        double m_average, m_min, m_max, m_error;
    };

    typedef std::map<std::string, Application> Applications;

    struct Transition {
        Transition(const std::string & name) : m_name(name) {}

        void addApplication(const Application & a) {
            m_applications.insert(std::make_pair(a.m_name, a));
        }

        typedef std::map<std::string, Application> Applications;

        const std::string m_name;
        Applications m_applications;
    };

    struct ChildrenTransitions {
        void addApplication(const std::string & transition, const Application & a) {
            auto it = std::find_if(m_transitions.begin(), m_transitions.end(),
                    [transition](auto i) { return i.m_name == transition; });

            if (it == m_transitions.end()) {
                m_transitions.push_back(Transition(transition));
                it = std::prev(m_transitions.end());
            }
            it->addApplication(a);

            m_app_index[a.m_name] = 0;
        }

        void updateAppIndex() {
            int x = 1;
            for (auto it = m_app_index.begin(); it != m_app_index.end(); ++it) {
                it->second = x++;
            }
        }

        std::map<std::string, int> m_app_index;
        std::list<Transition> m_transitions;
    };

    RCObject(const std::string & name) : m_name(name) {
    }

    void addChild(std::shared_ptr<RCObject> app) {
        m_children_data[app->getName()] = app;
    }

    TObject * finalize(ChildrenTransitions & tr);

    const std::string & getName() const {
        return m_name;
    }

    void update(const RCStateInfo & info);

private:
    typedef std::map<std::string, std::shared_ptr<RCObject>> ChildrenData;

    std::mutex m_mutex;
    std::string m_name;
    std::string m_last_transition;
    RCTransitions m_transitions;
    ChildrenData m_children_data;
};

#endif /* OPMON_RCOBJECT_H_ */
