/*
 * RCObject.cxx
 *
 *  Created on: Nov 9, 2020
 *      Author: kolos
 */

#include <iostream>
#include <map>
#include <numeric>

#include <TAxis.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TH1.h>
#include <TH2.h>
#include <TObjString.h>
#include <TList.h>
#include <TSortedList.h>
#include <TObjArray.h>
#include <TString.h>

#include <rc/RCStateInfo.h>

#include "RCObject.h"

namespace {
    TGraphErrors * createGraphErr(size_t size) {
        TGraphErrors * graph = new TGraphErrors(size);
        std::string n("Error");
        graph->SetNameTitle(n.c_str(), n.c_str());
        graph->SetLineWidth(2);
        graph->SetLineColor(46);
        graph->SetMarkerStyle(21);
        graph->SetMarkerColor(46);
        return graph;
    }

    TGraph * createGraph(size_t size, int color, int style, const std::string & name) {
        TGraph * graph = new TGraph(size);
        graph->SetNameTitle(name.c_str(), name.c_str());
        graph->SetLineWidth(0);
        graph->SetFillColor(color);
        graph->SetFillStyle(style);
        return graph;
    }

    TGraph * createGraphAvg(size_t size) {
        return createGraph(size, 9, 1001, "Average");
    }

    TGraph * createGraphMin(size_t size) {
        return createGraph(size, 1, 3017, "Min");
    }

    TGraph * createGraphMax(size_t size) {
        return createGraph(size, 18, 3001, "Max");
    }

    TH2 * createSummary(const std::string & name, int x_size, int y_size) {
        std::string h_name = "children summary";
        std::string h_title = name + " children summary";

        if (not x_size) ++x_size;
        if (not y_size) ++y_size;

        TH2F * sum = new TH2F(h_name.c_str(), h_title.c_str(),
                x_size, 0, x_size,
                y_size, 0, y_size);
        sum->SetStats(kFALSE);
        sum->SetDirectory(0);
        sum->GetXaxis()->SetLabelOffset(0.01);
        sum->GetXaxis()->SetLabelSize(0.03);
        sum->GetYaxis()->SetLabelOffset(0.01);
        sum->GetYaxis()->SetLabelSize(0.03);
        sum->GetZaxis()->SetLabelSize(0.03);
        sum->GetZaxis()->SetMaxDigits(3);
        sum->GetZaxis()->SetTitleOffset(1.3);
        sum->GetZaxis()->SetTitle("Time (seconds)");
        return sum;
    }

    template <class T, class F>
    void setLabels(TAxis & axis, T begin, T end, F op) {
        axis.Set(std::distance(begin, end), 0, std::distance(begin, end));

        int index = 1;
        for (T it = begin; it != end; ++it) {
            std::string l = op(it);
            axis.SetBinLabel(index++, l.c_str());
        }
    }

    void updateYAxis(TMultiGraph & g) {
        g.GetHistogram()->SetMinimum(0);
        g.GetYaxis()->SetTitle("Time (seconds)");
    }
}

void RCObject::update(const RCStateInfo & info) {
    std::unique_lock lock(m_mutex);
    if (not info.lastTransitionName.empty()
            && info.lastTransitionName != m_last_transition
            && RCTransitionNames.find(info.lastTransitionName) != RCTransitionNames.end()) {

        ERS_DEBUG(1, " recording " << info.transitionTime << " milliseconds for "
                << m_last_transition << " of " << m_name);

        m_transitions[info.lastTransitionName].push_back(info.transitionTime/1000.);
        m_last_transition = info.lastTransitionName;
    }
}

TObject * RCObject::finalize(ChildrenTransitions & tr) {
    if (m_transitions.empty()) {
        return nullptr;
    }

    int index = 0;
    TGraphErrors * g_err = createGraphErr(m_transitions.size());
    TGraph * g_avg = createGraphAvg(m_transitions.size());
    TGraph * g_min = createGraphMin(m_transitions.size());
    TGraph * g_max = createGraphMax(m_transitions.size());
    for (auto it = m_transitions.begin(); it != m_transitions.end(); ++it) {
        double average = 0.;
        double error = 0.;
        double min = 1000000.;
        double max = 0.;

        if (not it->second.empty()) {
            for (auto v : it->second) {
                average += v;
                min = std::min(min, v);
                max = std::max(max, v);
            }
            average = average / it->second.size();

            error = std::accumulate(it->second.begin(), it->second.end(), 0.,
                    [average](auto i, auto v) {
                        return i + (v - average)*(v - average);
                    });
            error = sqrt(error/it->second.size());
        }
        g_avg->SetPoint(index, index + 0.5, average);
        g_min->SetPoint(index, index + 0.5, min);
        g_max->SetPoint(index, index + 0.5, max);
        g_err->SetPoint(index, index + 0.5, average);
        g_err->SetPointError(index, 0., error);
        ++index;

        tr.addApplication(it->first, Application(m_name, average, min, max, error));
    }

    TMultiGraph * mg = new TMultiGraph("my transitions", m_name.c_str());
    mg->Add(g_max, "B");
    mg->Add(g_avg, "B");
    mg->Add(g_min, "B");
    mg->Add(g_err, "P");

    updateYAxis(*mg);
    setLabels(*mg->GetXaxis(), m_transitions.begin(), m_transitions.end(),
            [](auto it){ return it->first; });

    if (m_children_data.empty()) {
        mg->SetName(m_name.c_str());
        return mg;
    }

    TList * list = new TList();
    list->SetName(m_name.c_str());
    list->Add(mg);

    if (m_children_data.size() == 1) {
        ChildrenTransitions dummy;
        TObject * child = m_children_data.begin()->second->finalize(dummy);
        if (child) {
            list->Add(child);
        }
        return list;
    }

    ChildrenTransitions tr_map;
    TList * children = new TList();
    children->SetName("children");

    for (auto it = m_children_data.begin(); it != m_children_data.end(); ++it) {
        TObject * child = it->second->finalize(tr_map);

        if (not child) {
            continue;
        }

        children->Add(child);
    }

    if (children->IsEmpty()) {
        delete children;
        return list;
    }

    list->Add(children);

    // Some applications might have missed some transitions
    // tr_map.m_app_index is a complete applications map for
    // all transitions sorted alphabetically
    tr_map.updateAppIndex();

    TH2 * sum = createSummary(m_name, tr_map.m_app_index.size(), tr_map.m_transitions.size());
    TObjArray * transitions = new TObjArray(tr_map.m_transitions.size());
    transitions->SetOwner(kTRUE);
    transitions->SetName("children transitions");

    int y = 1;
    for (auto it = tr_map.m_transitions.begin(); it != tr_map.m_transitions.end(); ++it) {
        std::string t = it->m_name + " of " + m_name + " children";
        TMultiGraph * t_graph = new TMultiGraph();
        t_graph->SetNameTitle(it->m_name.c_str(), t.c_str());

        TGraphErrors * g_err = createGraphErr(it->m_applications.size());
        TGraph * g_avg = createGraphAvg(it->m_applications.size());
        TGraph * g_min = createGraphMin(it->m_applications.size());
        TGraph * g_max = createGraphMax(it->m_applications.size());

        int x = 0;
        for (auto ait = it->m_applications.begin(); ait != it->m_applications.end(); ++ait) {
            int X = tr_map.m_app_index[ait->first]; // must be present in the map
            sum->SetBinContent(X, y, ait->second.m_average);
            sum->SetBinError(X, y, ait->second.m_error);
            g_avg->SetPoint(x, x + 0.5, ait->second.m_average);
            g_min->SetPoint(x, x + 0.5, ait->second.m_min);
            g_max->SetPoint(x, x + 0.5, ait->second.m_max);
            g_err->SetPoint(x, x + 0.5, ait->second.m_average);
            g_err->SetPointError(x, 0., ait->second.m_error);
            ++x;
        }

        t_graph->Add(g_max, "B");
        t_graph->Add(g_avg, "B");
        t_graph->Add(g_min, "B");
        t_graph->Add(g_err, "P");

        updateYAxis(*t_graph);
        setLabels(*t_graph->GetXaxis(), it->m_applications.begin(), it->m_applications.end(),
                [](auto it) { return it->first; });
        t_graph->GetYaxis()->SetTitle("Time (seconds)");

        transitions->Add(t_graph);
        ++y;
    }

    setLabels(*sum->GetXaxis(), tr_map.m_app_index.begin(), tr_map.m_app_index.end(),
            [](auto it) { return it->first; });
    setLabels(*sum->GetYaxis(), tr_map.m_transitions.begin(), tr_map.m_transitions.end(),
            [](auto it) { return it->m_name; });

    if (transitions->GetEntries()) {
        list->AddAt(sum, 1);
        list->AddAt(transitions, 2);
    } else {
        delete sum;
        delete transitions;
    }

    return list;
}
