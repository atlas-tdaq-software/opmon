/*
 * main.cpp
 *
 *  Created on: Nov 9, 2020
 *      Author: kolos
 */

#include <boost/program_options.hpp>

#include <TSystem.h>

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/signal.h>
#include <coca/coca.h>

#include "Logger.h"

using namespace opmon;
using namespace daq::coca;

namespace po = boost::program_options;

int main(int argc, char *argv[]) {
    // prepare command line parser

    try {
        IPCCore::init(argc, argv);
    }
    catch(daq::ipc::Exception & ex) {
        ers::fatal(ex);
        return 1;
    }

    po::options_description desc(
            "This program subscribes to RunCtrl IS server "
            "and logs timing of all RC transitions to a new ROOT file "
            "in the given directory");

    std::string directory;
    std::string partition;
    std::string coca_dataset;
    std::string coca_server;
    std::string coca_partition;
    desc.add_options()("help,h", "Print help message")
            ("coca-dataset,c", po::value<std::string>(&coca_dataset),
                    "folder name in CoCa service to register the ROOT file")
            ("coca-server,s", po::value<std::string>(&coca_server)->default_value("coca"),
                    "CoCa server name")
            ("coca-partition,P", po::value<std::string>(&coca_partition)->default_value("initial"),
                    "CoCa server partition name")
            ("directory-name,d", po::value<std::string>(&directory)->required(),
             "directory to store the ROOT file")
            ("partition-name,p", po::value<std::string>(&partition)->required(),
             "partition name");

    po::variables_map vm;
    try {
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    } catch (std::exception &ex) {
        std::cerr << ex.what() << std::endl;
        desc.print(std::cout);
        return EXIT_FAILURE;
    }

    if (vm.count("help")) {
        desc.print(std::cout);
        return 1;
    }

    // Disable ROOT signal handlers as otherwise no core file will be produced
    if (gSystem) {
        gSystem->ResetSignals();
    }

    try {
        while (true) {
            Logger logger(partition, directory);

            ERS_LOG("Run Control logger has been (re)started");

            int sig = daq::ipc::signal::wait_for({SIGINT, SIGTERM, SIGUSR1});

            std::string file_name = logger.save();

            if (not file_name.empty() && not coca_dataset.empty()) {
                Register coca(coca_server, coca_partition);
                std::string archive = coca.registerFile(coca_dataset, file_name, 10, 86400, "");
                ERS_LOG(file_name << "' file has been registered with '"
                        << coca_dataset << "' coca dataset as '" << archive << " archive");
            }

            if (sig != SIGUSR1) {
                break;
            }
        }
    } catch (ers::Issue &ex) {
        std::cerr << "ERROR: " << ex << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
