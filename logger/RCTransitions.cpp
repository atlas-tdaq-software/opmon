/*
 * RCTransitions.cpp
 *
 *  Created on: Dec 1, 2020
 *      Author: kolos
 */


#include <logger/RCTransitions.h>

const std::map<std::string, int> RCTransitionNames = {
    {"INIT_FSM", 1},
    {"INITIALIZE", 2},
    {"CONFIGURE", 3},
    {"CONFIG", 4},
    {"CONNECT", 5},
    {"START", 6},
    {"STOP", 7},
    {"STOPROIB", 8},
    {"STOPDC", 9},
    {"STOPHLT", 10},
    {"STOPRECORDING", 11},
    {"STOPGATHERING", 12},
    {"STOPARCHIVING", 13},
    {"DISCONNECT", 14},
    {"UNCONFIG", 15},
    {"UNCONFIGURE", 16},
    {"SHUTDOWN", 17},
    {"STOP_FSM", 18},
    {"USERBROADCAST", 19},
    {"RELOAD_DB", 20},
    {"RESYNCH", 21},
    {"NEXT_TRANSITION", 22},
    {"SUB_TRANSITION", 23}
};
